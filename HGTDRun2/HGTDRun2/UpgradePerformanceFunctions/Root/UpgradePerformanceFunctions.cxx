#ifndef UPGRADEPERFORMANCEFUNCTIONS_CXX
#define UPGRADEPERFORMANCEFUNCTIONS_CXX

#include "UpgradePerformanceFunctions/UpgradePerformanceFunctions.h"

UpgradePerformanceFunctions::UpgradePerformanceFunctions():
  m_muonRes(nullptr),
  m_muonEff(nullptr){
  // This is the default constructor, so we use default settings
  m_layout = gold;
  m_avgMu = 200.;
  m_photonNoiseScaling = -1; //use electron resolution by default
  m_useMuonHighEta = true;  // use high-eta muon tagger by default
  // Initialize the missing ET performance histograms
  // For now, this is done inside the MissingETPerformanceFunctions

  // Initialize the tracking performance histograms
  // For now, this is done inside the TrackingPerformanceFunctions

  // Initialize JetEtMiss settings
  // Set random seeds
  m_jetRandom.SetSeed(1234567);
  m_pileupRandom.SetSeed(12345678);

  // JER setting initial values already set in header file (possible in C++11)

  // Inital values for track confirmation and PU jet overlay
  m_fPileupJetThresholdMeV = 30e3; // 30 GeV, default
  m_pueff = PU;            // PU-scheme is only scheme supported (so maybe drop this option)
  m_bUseTrackConf = true;  // Don't understand why this even is an option...
  m_fEff = 0.02;           // Set JVT PU fake rate to 2% within tracking acceptance (default)
  m_puPath = "/afs/cern.ch/atlas/www/GROUPS/DATABASE/GroupData/UpgradePerformanceFunctions/";
}

UpgradePerformanceFunctions::UpgradePerformanceFunctions(UpgradeLayout layout, double avgMu) {
  m_layout = layout;
  //m_avgMu = avgMu;
  if (layout!=UpgradePerformanceFunctions::UpgradeLayout::run2 && avgMu != 80. && avgMu != 140. && avgMu != 200.) {
    std::cout << "ERROR: mu value " << avgMu 
	      << " not allowed. Allowed values are 80, 140, 200 except for Run-2 layout.  Falling back to mu=" 
	      << m_avgMu << std::endl;
  } else {
    m_avgMu = avgMu;
  }
  m_muonRes = 0;
}

void UpgradePerformanceFunctions::fatal(TString msg) {
  printf("\nUpgradePerformanceFunctions FATAL\n\n  %s\n\n  Aborting.\n",msg.Data());
  abort();
}

void UpgradePerformanceFunctions::setLayout(UpgradeLayout layout) {
  m_layout = layout;
}

void UpgradePerformanceFunctions::setAvgMu(double avgMu) {
  if (avgMu != 60. && avgMu != 80. && avgMu != 140. && avgMu != 200.) {
    std::cout << "ERROR: mu value " << avgMu 
	      << " not allowed. Allowed values are 60, 80, 140, 200.  Falling back to mu=" 
	      << m_avgMu << std::endl;
  } else {
    m_avgMu = avgMu;
  }

}

UpgradePerformanceFunctions::UpgradeLayout UpgradePerformanceFunctions::getLayout() {
  return m_layout;
}

double UpgradePerformanceFunctions::getAvgMu() {
  return m_avgMu;
}

// Private helper function
TGraph *UpgradePerformanceFunctions::makeGraph(std::vector<double> x, std::vector<double> y) {
  if (x.size()!=y.size()) fatal("Bad input to UpgradePerformanceFunctions::makeGraph");
  TGraph *g = new TGraph();
  for (size_t i=0;i<x.size();++i)
    g->SetPoint(i,x[i],y[i]);
  return g;
}

#endif
