#include "parameters.h"
#include "functions.h"

void macroTable(string analysis = "WeightAnalysis", string fileType = "weight_JetsReso_TopoEM_TOTAL", TString channels_used = "0011011101111", int cutsType = 2,float luminosity = 36.1, int normalize = 1, int showRows = 1,int stn = 0, int entries = 0, int error = 1)
{
    vector<TFile*> files;
    vector<double> factors;
    for (int i=0 ; i < channels.size() ; i++)
    {
        if ((int)(channels_used[i] - '0') == 0) {
            files.push_back(NULL);
            continue;
        }
        cout << "Opening file " << channels[i] << fileType << ".root" << endl;
        files.push_back(new TFile((channels[i] + fileType + ".root").c_str()));
    }

    cout << "\\begin{table}[htbp]" << endl;
    cout << "\t" << "\\centering" << endl;
    cout << "\t" << "\\caption{Cutflow simulation data at $" << luminosity << " \\text{ fb}^{-1}$}" << endl;

    cout << "\t" << "\\begin{tabular}{";
    if (showRows == 1){
        if (cutsType == 1 || cutsType == 2) cout << "c|";
    } else {
        cout << "||l";
    }
    cout << "l";
    for (int i=0 ; i < channels.size() ; i++)
    {
        if ((int)(channels_used[i] - '0') == 0) continue;
        cout << "l";
    }
    if (stn == 1) cout << "ll";
    cout << "}" << endl;
    cout << "\t\t \\label{tab::cutflow}" << endl;
    cout << "\t\t";
    if (cutsType == 1 || cutsType == 2) cout << "&";
    for (int i=0 ; i < channels_titles.size() ; i++)
    {
        if ((int)(channels_used[i] - '0') == 0) continue;
        cout << " & " << channels_titles[i];
    }
    if (stn == 1) cout << " & $S/B$ & $S/\\sqrt{B}$";
    cout << " \\\\" << endl;
    cout << "\t\t \\midrule" << endl;
    if (cutsType == 1 || cutsType == 2) cout << "&";
    if (showRows == 1) cout << "\t\t Generated events ";
    for (int i=0 ; i < channels.size() ; i++)
    {
        if ((int)(channels_used[i] - '0') == 0) {
            factors.push_back(1.);
            continue;
        }
        // TH1D* h_full = (TH1D *)files[i]->Get((analysis + "__" + "n_taus" + "_0").c_str());
        TH1D* h_full = (TH1D *)files[i]->Get((analysis + "__" + "meffi").c_str());
        // TH1D* h_full = (TH1D *)files[i]->Get((analysis + "__" + "dphi1jet").c_str());
        int N = GetTotalBinContent(h_full);
        // int N = h_full->GetEntries();
        if  (entries == 1) N = h_full->GetEntries();
        factors.push_back(luminosity*cross_sections[i]/N);
        cout << " & " << N;
    }
    if (stn == 1) cout << " & &";
    cout << "\\\\" << endl;
    auto rows = table_rows;
    if (cutsType == 1) rows = table_rows_run2;
    if (cutsType == 2) rows = table_rows_run2_inclusiveJets;
    auto errorRows = error_rows;
    if (cutsType == 2) errorRows = error_rows_inclusive;

    for (int i=0 ; i < rows.size() ; i++)
    {
        if (cutsType == 1) {
            if (i==0){
                if (showRows == 1) cout << "\\multirow{9}{*}{\\shortstack[c]{QCD \\\\ fit}} ";
                cout << "& ";
            }
            else if (i==9) {
                cout << "\\midrule \n";
                if (showRows == 1) cout << " Preselection ";
                cout << "& ";
            }
            else if (i==10) {
                cout << "\\midrule \n";
                if (showRows == 1) cout << " \\multirow{7}{*}{\\shortstack[c]{CBA \\\\ VBF}} ";
                cout << "& ";
            }
            else cout << "&";
        } else if (cutsType == 2) {
            if (i==0){
                if (showRows == 1) cout << "\\multirow{9}{*}{\\shortstack[c]{QCD \\\\ fit}} ";
                cout << "& ";
            }
            else if (i==8) {
                cout << "\\midrule \n ";
                if (showRows == 1) cout << " Preselection ";
                cout << "& ";
            }
            else if (i==9) {
                cout << "\\midrule \n ";
                if (showRows == 1) cout << " \\multirow{7}{*}{\\shortstack[c]{CBA \\\\ VBF}} ";
                cout << "& ";
            }
            else cout << "&";
        }
        if (showRows == 1) cout << "\t\t " << rows[i];
        
        double S;
        double e_S;
        double B = 0.;
        double e_B = 0.;
        for (int j=0 ; j < channels.size() ; j++)
        {
            if ((int)(channels_used[j] - '0') == 0) continue;
            TH1D* h = (TH1D *)files[j]->Get((analysis + "__" + "n_taus" + "_" + to_string(i+1)).c_str());
            double q = GetTotalBinContent(h);
            if (entries == 1) q = h->GetEntries();
            cout << " & ";
            double curr_error = sqrt(GetTotalBinContent(h))*factors[j];
            if (contains(errorRows,i)){
                if (error == 1){
                    TH1D* errorHist = (TH1D *)files[j]->Get((analysis + "__" + "weight_squared_sum").c_str());
                    if (errorHist != NULL){
                        double sumSquaredWeight = errorHist->GetBinContent(i+1);
                        // curr_error = factors[j] * (q/h->GetEntries()) * sqrt(h->GetEntries());
                        curr_error = factors[j] * (sqrt(sumSquaredWeight)) ;
                    }
                    // cout << " $\\pm$ " << curr_error;
                    if (normalize == 1) {
                        printWithError(factors[j]*q, curr_error);
                    } else {
                        printWithError(q, curr_error);
                    }
                } else {
                    if (normalize == 1) {
                    cout << factors[j]*q;
                } else {
                    cout << q;
                }
                }
            } else {
                if (normalize == 1) {
                    cout << factors[j]*q;
                } else {
                    cout << q;
                }
            }
            if (j == 0) {
                S = factors[j]*q;
                e_S = curr_error;
            }
            else {
                B += factors[j]*q;
                e_B += curr_error*curr_error;
            }
        }
        if (stn == 1) {
            if (error == 1 && i == rows.size() - 1){
                e_B = sqrt(e_B);
                double e_SOB = 0.5 *(abs(((S + e_S) /(B - e_B)) - S/B) +
                                    abs(((S - e_S) /(B + e_B)) - S/B));
                double e_SOSB = 0.5 *(abs(((S + e_S) / sqrt(B - e_B)) - S/sqrt(B)) +
                                    abs(((S - e_S) / sqrt(B + e_B)) - S/sqrt(B)));
                cout << " & ";
                printWithError(S/B,e_SOB);
                cout << " & ";
                printWithError(S/sqrt(B),e_SOSB);
            } else {
                cout << " & " << S/B << " & " << S/sqrt(B);
            }
        }
        cout << " \\\\" << endl;
    }
    
    cout << "\t" << "\\end{tabular}" << endl;
    cout << "\\end{table}" << endl;

    for (int i=0 ; i < channels.size() ; i++)
    {
        if ((int)(channels_used[i] - '0') == 0) continue;
        files[i]->Close();
    }
}
