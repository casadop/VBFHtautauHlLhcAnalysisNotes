void macroPlotCompare(string analysis = "CBM", string file = "outSignalxxx", string histogram = "etaTau", 
                    string analysis2 = "CBM", string file2 = "outSignalxxx", string histogram2 = "etaTau", 
                    string format = ".jpeg")
{

    gROOT->SetBatch(kTRUE); //Don't display histograms
    gStyle->SetOptStat(111111);
    gStyle->SetPalette(1);

    TCanvas *c;
    TFile *f = new TFile((file + ".root").c_str());
    TFile *f2 = new TFile((file2 + ".root").c_str());

    TH1D * h = (TH1D *)f->Get((analysis + "__" + histogram).c_str());
    TH1D * h2 = (TH1D *)f2->Get((analysis2 + "__" + histogram2).c_str());

    c = new TCanvas;
    // c->SetRightMargin(0.15);

    // auto legend = new TLegend(0.85,0.1,0.995,0.9);
    // legend->SetNColumns(1);
    // legend->AddEntry(h,(file.substr(3,file.find("_")-3) + " " + histogram).c_str(),"f");
    // legend->AddEntry(h2,(file2.substr(3,file2.find("_")-3) + " " + histogram2).c_str(),"f");

    THStack *hs = new THStack("hs","");
    h->Scale(1./h->Integral("width"));
    h2->Scale(1./h2->Integral("width"));

    h->SetTitle((analysis + " " + file.substr(3,file.find("_")-3) + " " + histogram).c_str());
    h->SetLineColor(4);
    h->SetLineWidth(4);
    h->SetMarkerStyle(21);

    h2->SetTitle((analysis2 + " " + file2.substr(3,file2.find("_")-3) + " " + histogram2).c_str());
    h2->SetLineColor(2);
    h2->SetLineWidth(4);
    h2->SetMarkerStyle(21);
    h2->SetLineStyle(2);

    hs->Add(h); 
    hs->Add(h2); 

    hs->Draw("hist nostack");

    // legend->SetTextSize(0.02);
    // legend->Draw();
    c->BuildLegend();


    c->Update();
    c->Print(("compare_" + file + "_" + file2 + "__" + histogram + "__" + histogram2 + format).c_str());

    delete c;

    f->Close();
    f2->Close();

}