#!/bin/bash
bash checkLAL.sh ${1} Signal
echo "------------------------------------"
bash checkLAL.sh ${1} tZtt
echo "------------------------------------"
bash checkLAL.sh ${1} ELWZtt 
echo "------------------------------------"
bash checkLAL.sh ${1} ttbar
echo "------------------------------------"
bash checkLAL.sh ${1} inclusiveTop
echo "------------------------------------"
bash checkLAL.sh ${1} inclusiveAntiTop
echo "------------------------------------"
bash checkLAL.sh ${1} J1
echo "------------------------------------"
bash checkLAL.sh ${1} J2
echo "------------------------------------"
bash checkLAL.sh ${1} J3
echo "------------------------------------"
bash checkLAL.sh ${1} J4
echo "------------------------------------"
