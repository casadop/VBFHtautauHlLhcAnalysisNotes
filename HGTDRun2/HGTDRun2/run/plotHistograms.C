#include "parameters.h"
#include "functions.h"

#include "AtlasStyle.C"
#include "AtlasUtils.C"

void plotHistograms(string NAME = "", string type = "", string histName = "", string xLabel = "",  int normed = 1, int channelNorm = -1, string format = ".eps",
                    string suffix1 = "_17", string suffix2 = "_16"){

    SetAtlasStyle();

    gROOT->SetBatch(kTRUE); //Don't display histograms
    gStyle->SetStatH(0.4);
    gStyle->SetOptStat(1110);
    gStyle->SetPalette(1);
    // gPad->SetRightMargin(0.15);

    // std::vector<std::string> fileNames{"outSignalhh_" + type + "_TOTAL",
                                    // "outZtt_" + type + "_TOTAL",
                                    // "outELWZtt_" + type + "_TOTAL",
                                    // "outttbar_" + type + "_TOTAL",
                                    // // "outSingleTop_inclusiveTop_" + type + "_TOTAL",
                                    // // "outSingleTop_inclusiveAntiTop_" + type + "_TOTAL",
                                    // "outJ1_" + type + "_TOTAL",
                                    // "outJ2_" + type + "_TOTAL",
                                    // "outJ3_" + type + "_TOTAL",
                                    // "outJ4_" + type + "_TOTAL"
                                    // };
    // std::vector<std::string> analysis{"WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res","WeightAnalysis_Res"};
    // std::vector<std::string> prefix{"","","","","","","","","",""};
    // std::vector<std::string> suffix{"","","","","","","","","",""};

    std::vector<std::string> fileNames{"outSignalhh_weight_JetsReso_TopoEM_noSmearJets_Presentation_TOTAL",
                                        "outSignalhh_weight_withOptCuts_Presentation_TOTAL"
                                        };
    std::vector<std::string> analysis{"WeightAnalysis_Res","WeightAnalysis"};
    std::vector<std::string> prefix{"",""};
    std::vector<std::string> suffix{"",""};

    // std::vector<std::string> fileNames{"outSignalhh_weight_JetsReso_TopoEM_TOTAL",
    //                                     "outSignalhh_weight_JetsReso_TopoEM_newTest_TOTAL"};
    // std::vector<std::string> analysis{"WeightAnalysis_Res","WeightAnalysis_Res"};
    // std::vector<std::string> prefix{"",""};
    // std::vector<std::string> suffix{"",""};

    // std::vector<std::string> fileNames{"outJ1_cuts_noMcWeight_notrigger_TOTAL"};
    // std::vector<std::string> analysis{"CBM","CBM"};
    // std::vector<std::string> prefix{"",""};
    // std::vector<std::string> suffix{"",""};

    // std::vector<std::string> fileNames{"outZtt_cuts_TOTAL",
    //                                     "outZtt_weight_TOTAL"};
    // std::vector<std::string> analysis{"CBM","WeightAnalysis"};
    // std::vector<std::string> prefix{"",""};
    // std::vector<std::string> suffix{"_17","_16"};
    // suffix[0] = suffix1;
    // suffix[1] = suffix2;

    string labelAnalysis = "weight";
    if (analysis[0].find("CBM") == 0) labelAnalysis = "cuts";

    std::vector<TFile*> files;
    std::vector<std::string> legends;
    for (std::string name : fileNames){
        files.push_back(new TFile((name + ".root").c_str()));
        if (name.substr(3,7).find("Sing") == 0) {
            legends.push_back(name.substr(22,name.find(labelAnalysis)-23));
        } else if (name.substr(3,7).find("ELWZ") == 0){
            legends.push_back("ELW Ztt");
        } else {
            legends.push_back(name.substr(3,name.find(labelAnalysis)-4));
        }
    }

    legends.clear();
    legends.push_back("No smearing");
    legends.push_back("Smearing");
    

    // vector<string> histograms_to_plot{"weight_leptons_0","weight_leptons_1","weight_tau_0","weight_tau_1","weight_jet_0","weight_jet_1"};

    // vector<string> histograms_to_plot{"ptJet1", "ptTau1", "ptTau2", "n_leptons", "met", "met_HPTO", "DRtau", "RapiTaus", "n_jets",
    //                                 "n_taus", "ptJet2", "etaJet1", "etaJet2",
    //                                 "RapiJets", "invMassJets", "x1", "x2"
    //                                 };

    // vector <string> histograms_to_plot{histName};
    // vector <string> xLabels{xLabel};

    // vector <string> histograms_to_plot{"collmass_5"};
    // vector <string> xLabels{"Collinear Mass [GeV]"};
    vector <string> histograms_to_plot{"resJet1_Tight"};
    vector <string> xLabels{"Jet resolution"};

    // vector <string> histograms_to_plot{"etaJet2_0","etaTau","n_jets_0","n_taus_1","ptTau1_2","met_HPTO_4","DRtau_5","n_leptons_6",
    //                                     "x1_7","x2_7","RapiTaus_8","ptJet1_9","etaJet1_9","ptJet2_10","invMassJets_11","RapiJets_12","MMC"};
    // vector <string> xLabels{"Subleading jet's eta","Leading tau's eta","Number of jets","Number of taus","Leading tau's p_{T} [GeV]",
    //                         "MET [GeV]","#Delta R_{#tau#tau}","Number of leptons","x1","x2","#Delta#eta_{#tau#tau}","Leading jet's p_{T} [GeV]",
    //                          "Leading jet's eta", "Subleading jet's p_{T}","#Delta m_{jj}","#Delta#eta_{jj}","MMC"};

    vector<int> colors{};

    colors.push_back(kBlue);
    colors.push_back(kRed);

    TCanvas *c;
    for (uint k = 0 ; k < histograms_to_plot.size() ; k++){
        string histogram_name = histograms_to_plot[k];
        c = new TCanvas;
        // c->SetRightMargin(0.15);

        TLegend *legend=new TLegend(0.65,0.6,0.9,0.85);
        legend->SetBorderSize(0.00);
        legend->SetFillColor(10);

        THStack *hs = new THStack("hs","");

        for (uint i = 0 ; i < files.size() ; i++) {
            TH1D *h = (TH1D *)files[i]->Get((analysis[i] + "__" + prefix[i] + histogram_name + suffix[i]).c_str());
            if (h == NULL) {
                std::cout << "Histogram: " << analysis[i] << "__" << prefix[i] << histogram_name << suffix[i] << " doesn't exist." << std::endl;
                continue;
            }
            legend->AddEntry(h,legends[i].c_str(),"f");

            h->Scale(1./h->GetBinContent(h->GetMaximumBin()));
            // if (normed == 1) h->Scale(1./h->Integral("width"));
            if (channelNorm >= 0){
                double factor = 36.1*cross_sections[channelNorm]/GetTotalBinContent((TH1D *)files[i]->Get((analysis[i] + "__meffi").c_str()));
                h->Scale(factor);
            }

            int color = (i + 1 == 10) ? i + 31 : i + 1;
            h->SetTitle(fileNames[i].c_str());
            if (colors.size() == 0){
                h->SetLineColor(color);
            } else {
                h->SetLineColor(colors[i]);
            }
            h->SetLineWidth(4);
            h->SetMarkerStyle(21);
            h->SetLineStyle(i+1);
            
            // h->Rebin(5);

            hs->Add(h);
            // h->Draw("hist");
            // h->GetXaxis()->SetTitle(xLabels[k].c_str());
        }

        hs->Draw("hist nostack");
        hs->GetXaxis()->SetTitle(xLabels[k].c_str());
        hs->SetTitle(histogram_name.c_str());

        legend->SetTextSize(0.05);
        legend->Draw("same");
        // if (fileName2.compare("") != 0) c->BuildLegend();
        // if (fileName2.compare("") != 0) legend->Draw("same");

        c->Update();
        c->Print((NAME + fileNames[0].substr(fileNames[0].find("_")+1) + "_" + histogram_name  + suffix[0] + format).c_str());
    
        delete c;
    }

    for (TFile * file : files){
        file->Close();
    }

}