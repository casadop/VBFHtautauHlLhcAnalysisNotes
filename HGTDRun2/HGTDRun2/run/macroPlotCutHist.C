#include "parameters.h"
#include "functions.h"

void macroPlotCutHist(string analysis = "SergioAnalysis", string fileName = "outSignalhh", string format = ".eps", int normed = 0, int cutsType = 0, int n_cuts = 17, string analysis2 = "", string fileName2 = "")
{   
    TCanvas *c;
    TFile *f = new TFile((fileName + ".root").c_str());
    TFile *f2;
    if (fileName2.compare("") != 0) f2 = new TFile((fileName2 + ".root").c_str());

    auto current_legend_cuts = legend_cuts;
    if (cutsType == 1) current_legend_cuts = legend_stn_cuts;
    else if (cutsType == 2) current_legend_cuts = legend_run2_cuts;
    else if (cutsType == 3) current_legend_cuts = legend_run2_cuts_v2;

    if (n_cuts > current_legend_cuts.size()-1) n_cuts = current_legend_cuts.size()-1;

    gStyle->SetOptStat(000000000); // No statistics box
    gROOT->SetBatch(kTRUE); //Don't display histograms
    // gStyle->SetOptTitle(0); //Don't print title

    int highestIndex = getLastColorIndex();
    defineColors(highestIndex, n_cuts);

    for (int i = 0; i < histograms.size(); i++)
    {
        c = new TCanvas;
        c->SetTicks(0,0);
        c->SetRightMargin(0.25);

        THStack *hs = new THStack("hs",histograms[i].c_str());

        auto legend = new TLegend(0.75,0.1,0.995,0.9);
        legend->SetNColumns(1);
        for (int j = 0; j < n_cuts + 1; j++)
        {
            TH1D *h = (TH1D *)f->Get((analysis + "__" + histograms[i] + "_" + to_string(j)).c_str());
            TH1D *h2;
        
            if (fileName2.compare("") != 0) h2 = (TH1D *)f2->Get((analysis2 + "__" + histograms[i] + "_" + to_string(j)).c_str());
            if (normed == 1)
            {
                h->Scale(1./h->Integral());
                if (fileName2.compare("") != 0) h2->Scale(1./h2->Integral());
            }
            h->SetTitle(histograms[i].c_str());
            h->SetLineColor(j + highestIndex + 1);
            h->SetLineWidth(2);
            h->SetMarkerStyle(21);
            hs->Add(h);
            if (fileName2.compare("") != 0)
            {
                h2->SetTitle(histograms[i].c_str());
                h2->SetLineColor(j + highestIndex + 1);
                h2->SetLineWidth(2);
                h2->SetMarkerStyle(21);
                h2->SetLineStyle(2);
                hs->Add(h2);
            }
            legend->AddEntry(h,current_legend_cuts[j].c_str(),"f");    
        }
        hs->Draw("hist nostack");
        legend->SetTextSize(0.02);
        legend->Draw();
        c->Update();
        if (fileName2.compare("") != 0)
        {
            if (normed == 1){
                c->Print((fileName + "_" + fileName2 + "__" + histograms[i] + "_Normalized" + format).c_str());
            } else {
                c->Print((fileName + "_" + fileName2 + "__" + histograms[i] + format).c_str());
            }
        } else {
            if (normed == 1){
                c->Print((fileName + "__" + histograms[i] + "_Normalized" + format).c_str());
            } else {
                c->Print((fileName + "__" + histograms[i] + format).c_str());
            }
        }
        delete c;
    }

    f->Close();
    if (fileName2.compare("") != 0) f2->Close();
}