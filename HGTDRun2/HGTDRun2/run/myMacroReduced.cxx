#include "TFile.h"

int myMacroReduced(void){
  TFile *_fileSignal = TFile::Open("outSignalMu200NoHGTD.root");
  TFile *_fileBkg = TFile::Open("outBkgllMu200NoHGTD.root");
  TFile *_fileEWZtt = TFile::Open("outBkgllELWZttMu200NoHGTD.root");


  char const* histname="VBFHtautau__mc_weight";
  TH1F *histSignal=(TH1F*) _fileSignal->Get(histname);
  TH1F *histBkg=(TH1F*) _fileBkg->Get(histname);
  TH1F *histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  double entriesSignal=histSignal->GetEntries();
  double entriesBkg=histBkg->GetEntries();
  double entriesEWZtt=histEWZtt->GetEntries();

  double factorSignal= 3000.*(32.634247/1870000.);
  double factorBkg=3000.*(2112739.9/1.0238e+07);
  double factorEWZtt=3000.*(2849.0/246100.);


  std::cout << "Generated evts & " << entriesSignal << " & " << entriesBkg << " & " << entriesEWZtt <<  "\\\\ " << std::endl; 
  std::cout << "\\hline"<< std::endl;  
  histname="VBFHtautau__ptMu";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"One muon & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__m_ll";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"0 $\\tau_h$, 2 $\\mu$'s oppposite charge & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__met";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"m_ll,collmass cuts & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__mcoll_ttF";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"MET cut & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  



  histname="VBFHtautau__n_bjets";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"deltaRlep, deltaEtalep, x1, x2 & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__met1TeV_HPTO";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  

  std::cout <<"0 bjets & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  


  histname="VBFHtautau__n_jetsCheck";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  
  std::cout <<"MET_HPTO & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  


  histname="VBFHtautau__ptJet";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  
  std::cout <<"$>=$2 jets & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__DiffRapi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
    
  std::cout <<"Lead or SubLead jet $>$ 40 GeV & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__invMassJets";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
    
  std::cout <<"Rapidity cut, $\\eta_1$*$\\eta_2$ $<0$, central veto cut & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " <<  factorEWZtt*entriesEWZtt << " \\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__meffi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
    
  std::cout <<"invMassJet $>$ 500 GeV& " 
  << factorSignal*entriesSignal << " +/- " << factorSignal*sqrt(entriesSignal) << " & "   
  << factorBkg*entriesBkg << " +/- " << factorBkg*sqrt(entriesBkg) << " & "   
  << factorEWZtt*entriesEWZtt << " +/- " << factorEWZtt*sqrt(entriesEWZtt) << " \\\\ " << std::endl;   
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__meffi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
    
  std::cout <<"Final number of events & " 
  << "(" << entriesSignal<<")" << " & "   
  << "(" << entriesBkg << ")"<< " & "   
  << "(" << entriesEWZtt <<")"<< " \\\\ " << std::endl;   
  std::cout << "\\hline"<< std::endl;  

  return 0;
}
