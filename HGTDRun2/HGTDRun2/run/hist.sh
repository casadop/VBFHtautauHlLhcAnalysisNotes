echo TFile *_fileSignal = TFile::Open\(\"outSignalMu200NoHGTD.root\"\)
echo TFile *_fileBkg = TFile::Open\(\"outBkgllMu200NoHGTD.root\"\)
echo TFile *_fileZtt = TFile::Open\(\"outBkgllZttMu200NoHGTD.root\"\)
echo TFile *_filettbar = TFile::Open\(\"outBkgllttbarMu200NoHGTD.root\"\)
echo gStyle-\>SetOptStat\(1111110\);
echo c1 = new TCanvas\(\"c1\",\"Trigger Studies\",50,50,800,600\);
echo _fileZtt-\>cd\(\);
echo VBFHtautau__$1-\>Scale\(1./VBFHtautau__$1-\>Integral\(\)\);
echo VBFHtautau__$1-\>SetLineColor\(kRed\);
echo VBFHtautau__$1-\>Draw\(\"hist\"\);
echo _fileBkg-\>cd\(\);
echo VBFHtautau__$1-\>Scale\(1./VBFHtautau__$1-\>Integral\(\)\);
echo VBFHtautau__$1-\>Draw\(\"hist same\"\);
echo _filettbar-\>cd\(\);
echo VBFHtautau__$1-\>Scale\(1./VBFHtautau__$1-\>Integral\(\)\);
echo VBFHtautau__$1-\>SetLineColor\(kBlack\);
echo VBFHtautau__$1-\>Draw\(\"hist same\"\);
echo _fileSignal-\>cd\(\);
echo VBFHtautau__$1-\>Scale\(1./VBFHtautau__$1-\>Integral\(\)\);
echo VBFHtautau__$1-\>Draw\(\"same e\"\);
echo c1-\>Print\(\"$1.eps\"\);
echo .q
