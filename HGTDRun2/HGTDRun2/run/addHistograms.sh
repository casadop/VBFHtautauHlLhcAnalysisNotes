#!/bin/bash
i=0
if [ -d ${1} ]
then
	rm -f temp.txt
	for log in $(ls ${1}/*log)
	do
		if [ "$i" -lt "${2}" ]
		then
		t=$( tail -n 1 ${log} )
		if [ "${t:0:6}" == "xAOD::" ] 
		then
        		echo "${log%.*}.root " >> temp.txt
		#	echo "USING: ${log}"
			i=$((i+1))
		fi
		fi
	done

	echo "${i} FINISHED in ${1}"

	if [ "$i" -gt "0" ]
	then
		hadd -v 0 -f ${1}/${1}_TOTAL.root $(cat temp.txt)
		mv  ${1}/${1}_TOTAL.root ${1}_TOTAL.root
		echo "root -l ${1}_TOTAL.root"
	fi
fi

