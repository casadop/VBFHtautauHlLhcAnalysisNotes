std::cout << "Entries = " << VBFHtautau__mc_weight->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__ptMu->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__m_ll->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__met->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__met1TeV_HPTO->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__deltaRlep->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__n_bjets->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__n_jetsCheck->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__ptJet->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__DiffRapi->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__invMassJets->GetEntries() << std::endl;
std::cout << "Entries = " << 3000.*(32.634247/1870000.)*VBFHtautau__meffi->GetEntries() << " +/- " << 3000.*(32.634247/1870000.)*sqrt(VBFHtautau__meffi->GetEntries() ) << std::endl;
std::cout << " " << std::endl;
float sum=0.;
for(int i=1;i++;i<VBFHtautau__mc_weight->GetNbinsX()){sum=sum+VBFHtautau__mc_weight->GetBinCenter(i)*VBFHtautau__mc_weight->GetBinContent(i);}
std::cout << "mc_weight factor = " << sum << std::endl; 
