#include <string>
#include <algorithm>
#include <iomanip>
#include "TFile.h"
#include "TLegend.h"
#include "TCanvas.h"
#include "THStack.h"

#include "AtlasStyle.C"
#include "AtlasUtils.C"

  void doStackPlot(char const*histname){
//void doStackPlot(){  

  TFile *_fileZmmFinal = TFile::Open("outTemporary1.root");
  TFile *_fileZttFinal = TFile::Open("outTemporary2.root");
  TFile *_fileTopFinal = TFile::Open("outTemporary3.root");
  
  TFile *_fileSignal = TFile::Open("outSignalMu200NoHGTD.root");
  TFile *_fileSignalggF = TFile::Open("outSignalggFMu200NoHGTD.root");
  TFile *_fileBkg = TFile::Open("outBkgllMu200NoHGTD.root");
  TFile *_fileZtt = TFile::Open("outBkgllZttMu200NoHGTD.root");
  TFile *_fileEWZtt = TFile::Open("outBkgllELWZttMu200NoHGTD.root");
  TFile *_filettbar = TFile::Open("outBkgllttbarMu200NoHGTD.root");
  TFile *_fileEWZmumu13TeV = TFile::Open("outEWZmm13TeVMu200NoHGTD.root");
  TFile *_fileSingleTop1 = TFile::Open("outBkgllSingleTop1Mu200NoHGTD.root");
  TFile *_fileSingleTop2 = TFile::Open("outBkgllSingleTop2Mu200NoHGTD.root");
  TFile *_fileSingleTop3 = TFile::Open("outBkgllSingleTop3Mu200NoHGTD.root");
  TFile *_fileSingleTop4 = TFile::Open("outBkgllSingleTop4Mu200NoHGTD.root");

  TH1F *histZmmFinal=(TH1F*) _fileZmmFinal->Get(histname);
  TH1F *histZttFinal=(TH1F*) _fileZttFinal->Get(histname);
  TH1F *histTopFinal=(TH1F*) _fileTopFinal->Get(histname);
  TH1F *histSignal=(TH1F*) _fileSignal->Get(histname);
  TH1F *histSignalggF=(TH1F*) _fileSignalggF->Get(histname);
  TH1F *histBkg=(TH1F*) _fileBkg->Get(histname);
  TH1F *histZtt=(TH1F*) _fileZtt->Get(histname);
  TH1F *histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  TH1F *histttbar=(TH1F*) _filettbar->Get(histname);
  TH1F *histEWZmumu13TeV= (TH1F*) _fileEWZmumu13TeV->Get(histname);
  TH1F *histSingleTop3= (TH1F*) _fileSingleTop3->Get(histname);
  TH1F *histSingleTop4= (TH1F*) _fileSingleTop4->Get(histname);

  double entriesSignal=histSignal->GetEntries();
  double entriesSignalggF=histSignalggF->GetEntries();
  double entriesBkg=histBkg->GetEntries();
  double entriesZtt=histZtt->GetEntries();
  double entriesEWZtt=histEWZtt->GetEntries();
  double entriesttbar=histttbar->GetEntries();
  double entriesEWZmumu13TeV=histEWZmumu13TeV->GetEntries();
  double entriesSingleTop3=histSingleTop3->GetEntries();
  double entriesSingleTop4=histSingleTop4->GetEntries();

  double factorSignal= 3000.*(32.634247/1870000.);
  double factorSignalggF= 3000.*(32.634247/1450000.)*3.4108e-02/4.341e-03;
  double factorBkg=3000.*(2112739.9/1.0238e+07);
  double factorZtt=3000.*(2109661.6/4.1e+07);
  double factorEWZtt=3000.*(2849.0/246100.);
  double factorttbar=3000.*(534072./4.0005e+07);
  double factorEWZmumu13TeV= 3000.*((2538.2+2538.2*0.1)/entriesEWZmumu13TeV) ;
  double factorSingleTop3=3000.*40279.*1.054/entriesSingleTop3;
  double factorSingleTop4= 3000.* 40256.*1.054/entriesSingleTop4;


  TCanvas *c1 = new TCanvas("c1", "HGTD studies", 50,50,800,600);
 
  histZmmFinal->Reset();
  histZmmFinal->Add(histBkg, factorBkg);
  histZmmFinal->Add(histEWZmumu13TeV, factorEWZmumu13TeV);
  
  histZttFinal->Reset();
  histZttFinal->Add(histZtt, factorZtt);
  histZttFinal->Add(histEWZtt, factorEWZtt);
  
  histTopFinal->Reset();
  histTopFinal->Add(histttbar, factorttbar);
  histTopFinal->Add(histSingleTop3, factorSingleTop3);
  histTopFinal->Add(histSingleTop4, factorSingleTop4);
  
  //TH1F *h_mcTTbar= (TH1F*) h_mctt->Clone(("mcTTbar"+histname).c_str()); 
   
  histZmmFinal->SetLineColor (kOrange+8);
  histZmmFinal->SetFillColor (kOrange+8);
  histZttFinal->SetLineColor(kGreen+2); 
  histZttFinal->SetFillColor(kGreen+2);
  histTopFinal->SetLineColor(kBlue+2);
  histTopFinal->SetFillColor(kBlue+2);
  histSignal->SetMarkerStyle(20);
   
  histSignal->Scale(1000.);
  //histZmmFinal->Scale(1./histZmmFinal->Integral());
  //histZttFinal->Scale(1./histZttFinal->Integral());
  //histTopFinal->Scale(1./histTopFinal->Integral());
  //histSignalggF->Scale(1./histSignalggF->Integral());
  histSignalggF->Scale(factorSignalggF);


  //double max= std::max(histSignal->GetMaximum(), histZmmFinal->GetMaximum());
  //histSignal->SetMaximum(max);
  //histSignal->GetXaxis()->SetTitle(histname);

  THStack *hstack = new THStack("hstack","");
  hstack->Add(histSignal);
  hstack->Add(histZmmFinal);
  hstack->Add(histZttFinal);
  hstack->Add(histTopFinal);
  hstack->Add(histSignalggF);

  //histSignal->Draw("E");
  //histSignalggF->Draw("hist, same");
  //histZmmFinal->Draw("hist, same");
  //histZttFinal->Draw("hist, same");
  //histTopFinal->Draw("hist, same");
  //hstack->Draw("hist, same");

  hstack->GetXaxis()->SetTitle(histname);
  hstack->Draw("hist");
  //histSignal->Draw("E,same");

  Double_t legend_height=0.2;
  TLegend* legend = new TLegend(0.65,0.89-legend_height,0.87,0.89);
  legend->SetBorderSize(0.00);
  legend->SetFillColor(10);
  legend->AddEntry(histSignal,"Signal","l");
  legend->AddEntry(histSignalggF, "ggF", "l");
  legend->AddEntry(histZmmFinal,"Zmm","l");
  legend->AddEntry(histZttFinal, "Ztt","l");
  legend->AddEntry(histTopFinal, "Tops","l");
  legend->Draw("same");
  
  std::string myString(histname);
//  std::string fileOutput=myString+"Lin.eps";

  c1->Print("StackPlot.eps");
  _fileSignal->Close();
  _fileZmmFinal->Close();
  _fileZttFinal->Close();
  _fileTopFinal->Close();
  _fileSignalggF->Close();
  _fileBkg->Close();
  _fileZtt->Close();
  _fileEWZtt->Close();
  _filettbar->Close();
  _fileEWZmumu13TeV->Close();
  _fileSingleTop1->Close();
  _fileSingleTop2->Close();
  _fileSingleTop3->Close();
  _fileSingleTop4->Close();

  return; 
  }

int StackPlot(void){
  SetAtlasStyle(); 
  char const* histname="StackPlot";
  doStackPlot(histname);

  return 0;
}
