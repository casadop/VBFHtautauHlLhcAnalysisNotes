#!/bin/bash
step=${7}
start=${8}
MAX=${6}
end=${9}

STARTTIME=`date "+%d-%m   %H:%M"`
echo "${STARTTIME}  START for ${1} using ${MAX} events and step of ${step}"
LISTENING=true
LAUNCHING=true
while [ ${LISTENING} = true ]
do
	if [ $((end*step)) -gt ${MAX} ]	
	then
		TIME=`date "+%d-%m   %H:%M"`
		LAUNCHING=false
		echo "${TIME}  ALL LAUNCHED"
	fi
	t=$( tail -n 1 ${1}/${1}_${start}.log )
	while [ "${t:0:23}" != "xAOD::TFileAccessTracer" ]
	do
		t=$( tail -n 1 ${1}/${1}_${start}.log )
		sleep 10
	done
	TIME=`date "+%d-%m   %H:%M"`
	if [ ${LAUNCHING} = true ]
	then
		echo "${TIME}  FINISHED: ${1}_${start} ---------> LAUNCHING: ${1}_${end} at $(((end-1)*step))"
		nohup simpleAnalysis -a ${2} $(cat ${3}) -s ${4} -t ${5} -c $(((end-1)*step)) -e ${step}  -o ${1}/${1}_${end} >& ${1}/${1}_${end}.log &
		end=$((end+1))
	else
		echo "${TIME}  FINISHED: ${1}_${start}"
	fi
	start=$((start+1))
	if [ ${start} -gt $((end-1)) ]
	then
		LISTENING=false
	fi
done

TIME=`date "+%d-%m   %H:%M"`
echo "${TIME}  ALL ANALYSIS FINISHED (STARTED AT ${STARTTIME}"
echo "bash addHistograms.sh ${1}"
echo "rm LAL_${1}.log"
