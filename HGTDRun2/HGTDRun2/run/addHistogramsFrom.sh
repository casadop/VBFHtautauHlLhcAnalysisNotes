#!/bin/bash
i=0
if [ -d ${1} ]
then
	rm -f temp.txt
	for log in $(ls ${1}/*log)
	do
		if [ "$i" -lt "${3}" ]
		then
			t=$( tail -n 1 ${log} )
			if [ "${t:0:6}" == "xAOD::" ] 
			then
        			if [ "${i}" -ge "${2}" ]
				then
					echo "${log%.*}.root " >> temp.txt
					echo "USING: ${log}"
				fi
				i=$((i+1))
			fi
		fi
	done

	echo "${i} FINISHED in ${1}"

	if [ "$i" -gt "0" ]
	then
		hadd -v 0 -f ${1}/${1}_TOTAL.root $(cat temp.txt)
		if [ "${4}" -eq "0" ]
		then
			fileName="${1}_MMC_PSEUDO_TOTAL.root"
		else
			fileName="${1}_MMC_TOTAL.root"
		fi
		mv  ${1}/${1}_TOTAL.root ${fileName}
		echo "root -l ${fileName}"
	fi
fi

