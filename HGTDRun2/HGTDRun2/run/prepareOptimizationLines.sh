#!/bin/bash
echo "bash addAll.sh weight_${1}"
echo "bash addAll.sh weight_${2}"
echo "bash addAll.sh weight_${3}"
echo "bash addAll.sh weight_${4}"

echo "bash addBkg.sh weight_${1}"
echo "bash addBkg.sh weight_${2}"
echo "bash addBkg.sh weight_${3}"
echo "bash addBkg.sh weight_${4}"

echo

echo "root -l"
echo ".x macroStack.C (\"${1}\",\"${5}\",\"${6}\",${7})"
echo ".q"
echo "root -l"
echo ".x macroStack.C (\"${2}\",\"${5}\",\"${6}\",${7})"
echo ".q"
echo "root -l"
echo ".x macroStack.C (\"${3}\",\"${5}\",\"${6}\",${7})"
echo ".q"
echo "root -l"
echo ".x macroStack.C (\"${4}\",\"${5}\",\"${6}\",${7})"
echo ".q"

echo "root -l"
echo ".x plotHistograms.C (\"\",\"${1}\",\"${5}\",\"${6}\")"
echo ".q"
echo "root -l"
echo ".x plotHistograms.C (\"\",\"${2}\",\"${5}\",\"${6}\")"
echo ".q"
echo "root -l"
echo ".x plotHistograms.C (\"\",\"${3}\",\"${5}\",\"${6}\")"
echo ".q"
echo "root -l"
echo ".x plotHistograms.C (\"\",\"${4}\",\"${5}\",\"${6}\")"
echo ".q"

echo

echo "string histogramToUse = \"${5}\";"
errorBin=$(echo ${5} | cut -d"_" -f2)
echo "int errorBin = ${errorBin};"
echo "string xLabel = \"${6}\";"
echo "std::vector<std::string> fileTypes{\"weight_${1}_TOTAL\",\"weight_${2}_TOTAL\",\"weight_${3}_TOTAL\",\"weight_${4}_TOTAL\"};"
echo "std::vector<std::string> rows{\"${8} = ${9}\",\"${8} = ${10}\",\"${8} = ${11}\",\"${8} = ${12}\"};"
echo "std::vector<double> cutValues{${9},${10},${11},${12}};"

echo

echo "\begin{figure}[H]"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/SignalFraction_${5}_36}{Signal fraction for different values of ${14}}{}{0.33}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/SOB_${5}_36}{\$S/B\$ for different values of the cut of ${14}}{}{0.33}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/SOSB_${5}_36}{\$S/\sqrt{B}\$ for different values of the cut of ${14}}{}{0.33}{1}"
echo "	\bigskip"
echo "\end{figure}"

echo

echo "\begin{figure}[H]"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/weight_${1}_TOTAL_${5}}{${15} ${9}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/weight_${2}_TOTAL_${5}}{${15} ${10}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/weight_${3}_TOTAL_${5}}{${15} ${11}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/weight_${4}_TOTAL_${5}}{${15} ${12}}{}{0.24}{1}"
echo "	\bigskip"
echo	
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/STACKED_${1}}{${15} ${9}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/STACKED_${2}}{${15} ${10}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/STACKED_${3}}{${15} ${11}}{}{0.24}{1}"
echo "	\addSubimage{img/misc/OptimizationMCWeight/${13}/STACKED_${4}}{${15} ${12}}{}{0.24}{1}"
echo "\end{figure}"

