#!/bin/bash
if [ -d ${1} ] && [ ${11} != 1 ]
then
	echo "${1} already exists!"
	echo "rm -rf ${1}"
else
	mkdir -p ${1}
	step=${8}
	start=${6}
	max=${7}
	index=${9}
	maxIndex=$((index+${10}))
	for (( i=${index}; i<${maxIndex}; i++ ))
	do
		nohup simpleAnalysis -a ${2} $(cat ${3}) -s ${4} -t ${5} -c ${start} -e $((step-1))  -o ${1}/${1}_${i} >& ${1}/${1}_${i}.log &
		start=$((start+step))
	done
	nohup bash listenAndLaunch.sh ${1} ${2} ${3} ${4} ${5} ${7} ${8} ${9} ${maxIndex} >& LAL_${1}.log &
fi
