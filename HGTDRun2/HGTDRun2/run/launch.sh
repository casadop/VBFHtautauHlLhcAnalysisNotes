#!/bin/bash
mkdir -p ${1}
step=${8}
start=${6}
max=${7}
index=${9}
for (( c=${start}; c<=${max}; c+=${step} ))
do
	nohup simpleAnalysis -a ${2} $(cat ${3}) -s ${4} -t ${5} -c ${c} -e ${step}  -o ${1}/${1}_${index} >& ${1}/${1}_${index}.log &
	index=$((index+1))
done

