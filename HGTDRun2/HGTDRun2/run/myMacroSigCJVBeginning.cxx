#include "TFile.h"

int myMacroSigCJVBeginning(void){
  TFile *_fileSignal = TFile::Open("outSignalMu200NoHGTD.root");
  TFile *_fileBkg = TFile::Open("outBkgllMu200NoHGTD.root");
  TFile *_fileZtt = TFile::Open("outBkgllZttMu200NoHGTD.root");
  TFile *_fileEWZtt = TFile::Open("outBkgllELWZttMu200NoHGTD.root");
  TFile *_filettbar = TFile::Open("outBkgllttbarMu200NoHGTD.root");


  char const* histname="VBFHtautau__mc_weight";
  TH1F *histSignal=(TH1F*) _fileSignal->Get(histname);
  TH1F *histBkg=(TH1F*) _fileBkg->Get(histname);
  TH1F *histZtt=(TH1F*) _fileZtt->Get(histname);
  TH1F *histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  TH1F *histttbar=(TH1F*) _filettbar->Get(histname);
  double entriesSignal=histSignal->GetEntries();
  double entriesBkg=histBkg->GetEntries();
  double entriesZtt=histZtt->GetEntries();
  double entriesEWZtt=histEWZtt->GetEntries();
  double entriesttbar=histttbar->GetEntries();

  double factorSignal= 3000.*(32.634247/1870000.);
  double factorBkg=3000.*(2112739.9/1.0238e+07);
  double factorZtt=3000.*(2109661.6/4.1e+07);
  double factorEWZtt=3000.*(2849.0/246100.);
  double factorttbar=3000.*(534072./4.0005e+07);


  std::cout << "Generated evts & " << entriesSignal << " & " << entriesBkg << " & " << entriesZtt << " & " << entriesEWZtt << " & " << entriesttbar << " & & \\\\ " << std::endl; 
  std::cout << "\\hline"<< std::endl;  
  histname="VBFHtautau__ptJet";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  double Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"$>=$2 jets & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__DiffRapi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();  
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"Lead or SubLead jet $>$ 40 GeV & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__etaLeadingJetF";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();  
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"Rapidity cut, $\\eta_1$*$\\eta_2$ $<0$ & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__invMassJets";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();  
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"Central jet veto cut & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__n_muons";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();  
  std::cout <<"invMassJet $>$ 500 GeV&  " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  


  histname="VBFHtautau__m_ll";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"0 $\\tau_h$, 2 $\\mu$'s oppposite charge & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__met";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"m_ll,collmass cuts & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__mcoll_ttF";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"MET cut & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__n_bjets";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"deltaRlep, deltaEtalep, x1, x2 & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__met1TeV_HPTO";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"0 bjets & " << factorSignal*entriesSignal << " & " << factorBkg*entriesBkg << " & " << factorZtt*entriesZtt << " & " << factorEWZtt*entriesEWZtt << " & " << factorttbar*entriesttbar << " & " <<factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) <<  "\\\\ " << std::endl;
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__meffi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();
  Tot_Bkg = factorBkg*entriesBkg + factorZtt*entriesZtt + factorEWZtt*entriesEWZtt +  factorttbar*entriesttbar;
  std::cout <<"MET_HPTO & " 
  << factorSignal*entriesSignal << " +/- " << factorSignal*sqrt(entriesSignal) << " & "   
  << factorBkg*entriesBkg << " +/- " << factorBkg*sqrt(entriesBkg) << " & "   
  << factorZtt*entriesZtt << " +/- " << factorZtt*sqrt(entriesZtt) << " & "   
  << factorEWZtt*entriesEWZtt << " +/- " << factorEWZtt*sqrt(entriesEWZtt) << " & "   
  << factorttbar*entriesttbar << " +/- " << factorttbar*sqrt(entriesttbar) << " & " << factorSignal*entriesSignal / Tot_Bkg << " & " << factorSignal*entriesSignal / sqrt(Tot_Bkg) << "\\\\" << std::endl;   
  std::cout << "\\hline"<< std::endl;  

  histname="VBFHtautau__meffi";
  histSignal=(TH1F*) _fileSignal->Get(histname);
  histBkg=(TH1F*) _fileBkg->Get(histname);
  histZtt=(TH1F*) _fileZtt->Get(histname);
  histEWZtt=(TH1F*) _fileEWZtt->Get(histname);
  histttbar=(TH1F*) _filettbar->Get(histname);
  entriesSignal=histSignal->GetEntries();
  entriesBkg=histBkg->GetEntries();
  entriesZtt=histZtt->GetEntries();
  entriesEWZtt=histEWZtt->GetEntries();
  entriesttbar=histttbar->GetEntries();  
  std::cout <<"Final number of events & " 
  << "(" << entriesSignal<<")" << " & "   
  << "(" << entriesBkg << ")"<< " & "   
  << "(" << entriesZtt << ")"<<" & "   
  << "(" << entriesEWZtt <<")"<< " & "   
  << "(" << entriesttbar << ")"<< " & & \\\\" << std::endl;   
  std::cout << "\\hline"<< std::endl;  


  return 0;
}
