#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dInfsdIat3mIprojectsdIHGTDdIHGTDTestRepodIVBFHtautauHlLhcAnalysisNotesdIV7dO0dIVBFHtautauHlLhcAnalysisNotesdIHGTDRun2dIRootCoreBindIobjdIx86_64mIslc6mIgcc49mIoptdISimpleAnalysisdIobjdISimpleAnalysisCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "SimpleAnalysis/OldSlimReader.h"
#include "SimpleAnalysis/SlimReader.h"
#include "SimpleAnalysis/D3PDReader.h"

// Header files passed via #pragma extra_include

namespace ROOT {
   static void delete_OldSlimReaderSelector(void *p);
   static void deleteArray_OldSlimReaderSelector(void *p);
   static void destruct_OldSlimReaderSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::OldSlimReaderSelector*)
   {
      ::OldSlimReaderSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::OldSlimReaderSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("OldSlimReaderSelector", ::OldSlimReaderSelector::Class_Version(), "SimpleAnalysis/OldSlimReader.h", 24,
                  typeid(::OldSlimReaderSelector), DefineBehavior(ptr, ptr),
                  &::OldSlimReaderSelector::Dictionary, isa_proxy, 4,
                  sizeof(::OldSlimReaderSelector) );
      instance.SetDelete(&delete_OldSlimReaderSelector);
      instance.SetDeleteArray(&deleteArray_OldSlimReaderSelector);
      instance.SetDestructor(&destruct_OldSlimReaderSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::OldSlimReaderSelector*)
   {
      return GenerateInitInstanceLocal((::OldSlimReaderSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::OldSlimReaderSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_SlimReaderSelector(void *p);
   static void deleteArray_SlimReaderSelector(void *p);
   static void destruct_SlimReaderSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::SlimReaderSelector*)
   {
      ::SlimReaderSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::SlimReaderSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("SlimReaderSelector", ::SlimReaderSelector::Class_Version(), "SimpleAnalysis/SlimReader.h", 19,
                  typeid(::SlimReaderSelector), DefineBehavior(ptr, ptr),
                  &::SlimReaderSelector::Dictionary, isa_proxy, 4,
                  sizeof(::SlimReaderSelector) );
      instance.SetDelete(&delete_SlimReaderSelector);
      instance.SetDeleteArray(&deleteArray_SlimReaderSelector);
      instance.SetDestructor(&destruct_SlimReaderSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::SlimReaderSelector*)
   {
      return GenerateInitInstanceLocal((::SlimReaderSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::SlimReaderSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_D3PDReaderSelector(void *p);
   static void deleteArray_D3PDReaderSelector(void *p);
   static void destruct_D3PDReaderSelector(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::D3PDReaderSelector*)
   {
      ::D3PDReaderSelector *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::D3PDReaderSelector >(0);
      static ::ROOT::TGenericClassInfo 
         instance("D3PDReaderSelector", ::D3PDReaderSelector::Class_Version(), "SimpleAnalysis/D3PDReader.h", 19,
                  typeid(::D3PDReaderSelector), DefineBehavior(ptr, ptr),
                  &::D3PDReaderSelector::Dictionary, isa_proxy, 4,
                  sizeof(::D3PDReaderSelector) );
      instance.SetDelete(&delete_D3PDReaderSelector);
      instance.SetDeleteArray(&deleteArray_D3PDReaderSelector);
      instance.SetDestructor(&destruct_D3PDReaderSelector);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::D3PDReaderSelector*)
   {
      return GenerateInitInstanceLocal((::D3PDReaderSelector*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_(Init) = GenerateInitInstanceLocal((const ::D3PDReaderSelector*)0x0); R__UseDummy(_R__UNIQUE_(Init));
} // end of namespace ROOT

//______________________________________________________________________________
atomic_TClass_ptr OldSlimReaderSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *OldSlimReaderSelector::Class_Name()
{
   return "OldSlimReaderSelector";
}

//______________________________________________________________________________
const char *OldSlimReaderSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OldSlimReaderSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int OldSlimReaderSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::OldSlimReaderSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *OldSlimReaderSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OldSlimReaderSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *OldSlimReaderSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::OldSlimReaderSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr SlimReaderSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *SlimReaderSelector::Class_Name()
{
   return "SlimReaderSelector";
}

//______________________________________________________________________________
const char *SlimReaderSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SlimReaderSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int SlimReaderSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::SlimReaderSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *SlimReaderSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SlimReaderSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *SlimReaderSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::SlimReaderSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr D3PDReaderSelector::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *D3PDReaderSelector::Class_Name()
{
   return "D3PDReaderSelector";
}

//______________________________________________________________________________
const char *D3PDReaderSelector::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::D3PDReaderSelector*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int D3PDReaderSelector::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::D3PDReaderSelector*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *D3PDReaderSelector::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::D3PDReaderSelector*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *D3PDReaderSelector::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD2(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::D3PDReaderSelector*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
void OldSlimReaderSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class OldSlimReaderSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(OldSlimReaderSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(OldSlimReaderSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_OldSlimReaderSelector(void *p) {
      delete ((::OldSlimReaderSelector*)p);
   }
   static void deleteArray_OldSlimReaderSelector(void *p) {
      delete [] ((::OldSlimReaderSelector*)p);
   }
   static void destruct_OldSlimReaderSelector(void *p) {
      typedef ::OldSlimReaderSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::OldSlimReaderSelector

//______________________________________________________________________________
void SlimReaderSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class SlimReaderSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(SlimReaderSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(SlimReaderSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_SlimReaderSelector(void *p) {
      delete ((::SlimReaderSelector*)p);
   }
   static void deleteArray_SlimReaderSelector(void *p) {
      delete [] ((::SlimReaderSelector*)p);
   }
   static void destruct_SlimReaderSelector(void *p) {
      typedef ::SlimReaderSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::SlimReaderSelector

//______________________________________________________________________________
void D3PDReaderSelector::Streamer(TBuffer &R__b)
{
   // Stream an object of class D3PDReaderSelector.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(D3PDReaderSelector::Class(),this);
   } else {
      R__b.WriteClassBuffer(D3PDReaderSelector::Class(),this);
   }
}

namespace ROOT {
   // Wrapper around operator delete
   static void delete_D3PDReaderSelector(void *p) {
      delete ((::D3PDReaderSelector*)p);
   }
   static void deleteArray_D3PDReaderSelector(void *p) {
      delete [] ((::D3PDReaderSelector*)p);
   }
   static void destruct_D3PDReaderSelector(void *p) {
      typedef ::D3PDReaderSelector current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::D3PDReaderSelector

namespace {
  void TriggerDictionaryInitialization_SimpleAnalysisCINT_Impl() {
    static const char* headers[] = {
"SimpleAnalysis/OldSlimReader.h",
"SimpleAnalysis/SlimReader.h",
"SimpleAnalysis/D3PDReader.h",
0
    };
    static const char* includePaths[] = {
"/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis/Root",
"/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/RootCoreBin/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase/x86_64/root/6.04.16-x86_64-slc6-gcc49-opt/include",
"/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis/cmt/",
0
    };
    static const char* fwdDeclCode = 
R"DICTFWDDCLS(
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis/Root/LinkDef.h")))  OldSlimReaderSelector;
class __attribute__((annotate("$clingAutoload$/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis/Root/LinkDef.h")))  SlimReaderSelector;
class __attribute__((annotate("$clingAutoload$/nfs/at3-projects/HGTD/HGTDTestRepo/VBFHtautauHlLhcAnalysisNotes/V7.0/VBFHtautauHlLhcAnalysisNotes/HGTDRun2/SimpleAnalysis/Root/LinkDef.h")))  D3PDReaderSelector;
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_RELEASE_SERIES
  #define ROOTCORE_RELEASE_SERIES 24
#endif
#ifndef ROOTCORE_TEST_FILE
  #define ROOTCORE_TEST_FILE "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ROOTCORE_TEST_DATA
  #define ROOTCORE_TEST_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725"
#endif
#ifndef ASG_TEST_FILE_DATA
  #define ASG_TEST_FILE_DATA "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7562/data15_13TeV.00284154.physics_Main.merge.AOD.r7562_p2521/AOD.07687819._000382.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MC
  #define ASG_TEST_FILE_MC "/afs/cern.ch/user/a/asgbase/patspace/xAODs/r7725/mc15_13TeV.410000.PowhegPythiaEvtGen_P2012_ttbar_hdamp172p5_nonallhad.merge.AOD.e3698_s2608_s2183_r7725_r7676/AOD.07915862._000100.pool.root.1"
#endif
#ifndef ASG_TEST_FILE_MCAFII
  #define ASG_TEST_FILE_MCAFII ""
#endif
#ifndef XAOD_STANDALONE
  #define XAOD_STANDALONE 1
#endif
#ifndef XAOD_ANALYSIS
  #define XAOD_ANALYSIS 1
#endif
#ifndef ASGTOOL_STANDALONE
  #define ASGTOOL_STANDALONE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "SimpleAnalysis"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "SimpleAnalysis/OldSlimReader.h"
#include "SimpleAnalysis/SlimReader.h"
#include "SimpleAnalysis/D3PDReader.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"D3PDReaderSelector", payloadCode, "@",
"OldSlimReaderSelector", payloadCode, "@",
"SlimReaderSelector", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("SimpleAnalysisCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_SimpleAnalysisCINT_Impl, {{"namespace DataVector_detail { template <typename B1, typename B2, typename B3> class VirtBases; }", 1},{"template <typename T> class DataVectorBase;", 1},{"template <typename T, typename BASE> class DataVector;", 1},{"namespace DataVector_detail { template <typename T> class DVLEltBaseInit; }", 1}}, classesHeaders);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_SimpleAnalysisCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_SimpleAnalysisCINT() {
  TriggerDictionaryInitialization_SimpleAnalysisCINT_Impl();
}
