#include "SimpleAnalysis/AnalysisClass.h"
#include <RootCore/Packages.h>
#include "TRandom3.h"
#include "SimpleAnalysis/TruthSmear.h"

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
static  BTaggingTruthTaggingTool *m_btt;
#endif

#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
#include <UpgradePerformanceFunctions/UpgradePerformanceFunctions.h>
#endif

class CBM_Jets : public AnalysisClass
{
    public:
        CBM_Jets(): AnalysisClass("CBM_Jets"){};
        void Init(std::vector<std::string>& analysisOptions);
        void ProcessEvent(AnalysisEvent *event);
        void getPairs(AnalysisObjects particles, std::vector<int> indexes, std::vector<AnalysisObjects> &pairs);
	void Final();

        bool MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, const double metetx, const double metety, double &mass, double &xp1, double &xp2);

    private:
        UpgradePerformanceFunctions *m_upgrade;
        TRandom3 m_random;
        bool useTrigger;
        bool useHPTOmet;
        bool useRun2kinCuts;

	double Event_weight;
	double sumSquaredWeights[18];

        double tauEta;
        double jetEta;

};

static const AnalysisClass *CBM_Jets_instance __attribute__((used)) = new CBM_Jets();

void CBM_Jets::getPairs(AnalysisObjects particles, std::vector<int> indexes, std::vector<AnalysisObjects> &pairs){
    uint startingIndex = 0;
    if (indexes.size() > 0) {
        startingIndex = indexes.back() + 1;
    }
    for (uint i = startingIndex ; i < particles.size() ; i++){
        if (i > startingIndex) indexes.pop_back();
        indexes.push_back(i);
        if (indexes.size() == 2){
            AnalysisObjects pair;
            for (uint k : indexes){
                pair.push_back(particles[k]);
            }
            pairs.push_back(pair);
        } else {
            std::vector<int> clonedIndexes(indexes);
            getPairs(particles, clonedIndexes, pairs);
        }
    }
}

bool CBM_Jets::MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, //particles
                                       const double metetx, const double metety,           //met
                                       double &mass, double &xp1, double &xp2)
{ //result
    TMatrixD K(2, 2);
    K(0, 0) = k1.Px();
    K(0, 1) = k2.Px();
    K(1, 0) = k1.Py();
    K(1, 1) = k2.Py();

    if (K.Determinant() == 0)
        return false;

    TMatrixD M(2, 1);
    M(0, 0) = metetx;
    M(1, 0) = metety;

    TMatrixD Kinv = K.Invert();

    TMatrixD X(2, 1);
    X = Kinv * M;

    double X1 = X(0, 0);
    double X2 = X(1, 0);
    double x1 = 1. / (1. + X1);
    double x2 = 1. / (1. + X2);

    TLorentzVector p1 = k1 * (1 / x1);
    TLorentzVector p2 = k2 * (1 / x2);

    double m = (p1 + p2).M();

    //return to caller
    mass = m;

    if (k1.Pt() > k2.Pt())
    {
        xp1 = x1;
        xp2 = x2;
    }
    else
    {
        xp1 = x2;
        xp2 = x1;
    }

    return true;
}
