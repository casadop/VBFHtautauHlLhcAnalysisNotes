
#include "SimpleAnalysis/AnalysisClass.h"
#include <RootCore/Packages.h>
#include "TRandom3.h"
#include "SimpleAnalysis/TruthSmear.h"
#include "SimpleAnalysis/combination.h"
#include "SimpleAnalysis/MissingMassCalculator.h"

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
// static  BTaggingTruthTaggingTool *m_btt;
#endif

#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
#include <UpgradePerformanceFunctions/UpgradePerformanceFunctions.h>
#endif

struct RecoGroup
{
    AnalysisObjects group;
    std::vector<int> indices;
    float weight;
};

struct RecoSource
{
    AnalysisObject source;
    int index;
};

int getMax(std::vector<int> v) {
    return *std::max_element(v.begin(), v.end());
}

bool contains(std::vector<int> v, int i){
    return std::find(v.begin(), v.end(), i) != v.end();
}

class WeightAnalysis_Res : public AnalysisClass
{
    public:
        WeightAnalysis_Res(): AnalysisClass("WeightAnalysis_Res"){};
        void Init(std::vector<std::string>& analysisOptions);
        void ProcessEvent(AnalysisEvent *event);
        void Final();

        void Combinations(int N, AnalysisObjects toCombine, std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, 
                        std::vector<int> recoElectronsIndices, AnalysisObjects signalTaus, AnalysisObjects signalJets, 
                        AnalysisObjects signalElectrons, AnalysisObjects signalMuons, int type, 
                        double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event);

        void NextCombination(std::vector<int> indices, std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, 
                            std::vector<int> recoElectronsIndices, AnalysisObjects signalTaus, AnalysisObjects signalJets, 
                            AnalysisObjects signalElectrons, AnalysisObjects signalMuons, int type, 
                            double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event);        


        void Analyze(std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, std::vector<int> recoElectronsIndices, 
                    std::vector<int> recoMuonsIndices, AnalysisObjects signalTaus, AnalysisObjects signalJets, 
                    AnalysisObjects signalElectrons, AnalysisObjects signalMuons, 
                   double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event);

        int comb(int m, int n);
        int getTotalNumberOfCombinations(int N);

        void getPairs(AnalysisObjects particles, std::vector<int> indexes, std::vector<AnalysisObjects> &pairs);

        void PlotWeightFunctions();

        bool MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, const double metetx, const double metety, double &mass, double &xp1, double &xp2);

        void calculate_MET_HPTO(AnalysisObjects mainParticles, AnalysisObjects jets, double &MET_HPTO, double &MET_HPTOx, double &MET_HPTOy);

        bool isRecoOverlap(AnalysisObject origin, AnalysisObject reco);

        int getRecoSource(AnalysisObject reco, AnalysisObjects recoSources);


    private:
        UpgradePerformanceFunctions *m_upgrade;
        TRandom3 m_random;
        bool useTrigger;
        bool useHPTOmet;
        bool useRun2kinCuts;
        bool considerLeptonCombinations;
        bool considerLostWeights;
        bool plotWeightFunctions;
        int maxJetCombinations;
        int maxTauCombinations;
        int maxMuonCombinations;
        int maxElectronCombinations;
        int excludedJetCombination;
        bool useMcWeight;
        int DEBUG;
        bool useMMC;
        double current_mcWeight;        
        bool skipCombinations;

        double sumSquaredWeights[18];

        double muEtaCut;
        double jetEtaCut;
        double muPtCut;
        double deltaRlepCut;
        double deltaEtalepCut;
        double metCut;
        double x1Cut_1;
        double x1Cut_2;
        double x2Cut_1;
        double x2Cut_2;
        double leadingJetPtCut;
        double invMassJetsCut;
        double RapiJetsCut;

        MissingMassCalculator fMMC;
};

static const AnalysisClass *WeightAnalysis_Res_instance __attribute__((used)) = new WeightAnalysis_Res();

void WeightAnalysis_Res::Combinations(int N, AnalysisObjects toCombine, std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, 
                                    std::vector<int> recoElectronsIndices, AnalysisObjects signalTaus, AnalysisObjects signalJets, 
                                    AnalysisObjects signalElectrons, AnalysisObjects signalMuons, int type, 
                                    double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event) {
    std::vector<int> allIndices;
    std::vector<int> indices;
    if (N > 0) {
        for (int i = 0 ; i < N ; i++) indices.push_back(i);
        for (int i = 0 ; i < toCombine.size() ; i++) allIndices.push_back(i);
        do {
            NextCombination(indices, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, type, met, pmissx, pmissy, lostWeight, event);
        } while (stdcomb::next_combination(allIndices.begin (),allIndices.end (), indices.begin (),indices.end()));
    } else {
        NextCombination(indices, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, type, met, pmissx, pmissy, lostWeight, event);
    }

}

void WeightAnalysis_Res::NextCombination(std::vector<int> indices, std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, 
                                    std::vector<int> recoElectronsIndices, AnalysisObjects signalTaus, AnalysisObjects signalJets, 
                                    AnalysisObjects signalElectrons, AnalysisObjects signalMuons, int type, 
                                    double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event) {
    if (type == 3) {
        Analyze(recoTausIndices, recoJetsIndices, recoElectronsIndices, indices, signalTaus, signalJets, signalElectrons, signalMuons, met, pmissx, pmissy, lostWeight, event);
    } else if (type == 0) {
        if (signalJets.size() > 0) {
            for (int z = 0 ; z <= signalJets.size() ; z++) {
                std::vector<int> indexes_jet;
                Combinations(z , signalJets, indices, recoJetsIndices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, 1, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else if (signalElectrons.size() > 0 && considerLeptonCombinations) {
            for (int z = 0 ; z <= signalElectrons.size() ; z++) {
                Combinations(z , signalElectrons, indices, recoJetsIndices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, 2, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else if (signalMuons.size() > 0 && considerLeptonCombinations) {
            for (int z = 0 ; z <= signalMuons.size() ; z++) {
                Combinations(z , signalMuons, indices, recoJetsIndices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, 3, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else {
            std::vector<int> recoMuonsIndices;
            Analyze(indices, recoJetsIndices, recoElectronsIndices, recoMuonsIndices, signalTaus, signalJets, signalElectrons, signalMuons, met, pmissx, pmissy, lostWeight, event);
        }
    } else if (type == 1 ) {
        if (signalElectrons.size() > 0 && considerLeptonCombinations) {
            for (int z = 0 ; z <= signalElectrons.size() ; z++) {
                Combinations(z , signalElectrons, recoTausIndices, indices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, 2, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else if (signalMuons.size() > 0 && considerLeptonCombinations) {
            for (int z = 0 ; z <= signalMuons.size() ; z++) {
                Combinations(z , signalMuons, recoTausIndices, indices, recoElectronsIndices, signalTaus, signalJets, signalElectrons, signalMuons, 3, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else {
            std::vector<int> recoMuonsIndices;
            Analyze(recoTausIndices, indices, recoElectronsIndices, recoMuonsIndices, signalTaus, signalJets, signalElectrons, signalMuons, met, pmissx, pmissy, lostWeight, event);
        }
    } else if (type == 2) {
        if (signalMuons.size() > 0 && considerLeptonCombinations) {
            for (int z = 0 ; z <= signalMuons.size() ; z++) {
                Combinations(z, signalMuons, recoTausIndices, recoJetsIndices, indices, signalTaus, signalJets, signalElectrons, signalMuons, 3, met, pmissx, pmissy, lostWeight, event);                                    
            }
        } else {
            std::vector<int> recoMuonsIndices;
            Analyze(recoTausIndices, recoJetsIndices, indices, recoMuonsIndices, signalTaus, signalJets, signalElectrons, signalMuons, met, pmissx, pmissy, lostWeight, event);
        }
    }
}

int WeightAnalysis_Res::comb(int m, int n) {

    int mat[m][n];
    int i, j;
    if (n > m) return 0;
    if( (n == 0) || (m == n) ) return 1;
    for(j = 0; j < n; j ++)
    {
        mat[0][j] = 1;
        if (j == 0)
        {
            for (i = 1; i<= m - n; i ++ ) mat[i][j] = i + 1;
        }
        else
        {
            for (i = 1; i<= m - n; i ++) mat[i][j] = mat[i - 1][j] + mat[i][j - 1];
        }
    }
    return (mat[m - n][n - 1]);
}

int WeightAnalysis_Res::getTotalNumberOfCombinations(int N){
    int c = 0;
    for (int i = 0 ; i <= N ; i++) c += comb(N,i);
    return c;
}

void WeightAnalysis_Res::getPairs(AnalysisObjects particles, std::vector<int> indexes, std::vector<AnalysisObjects> &pairs){
    uint startingIndex = 0;
    if (indexes.size() > 0) {
        startingIndex = indexes.back() + 1;
    }
    for (uint i = startingIndex ; i < particles.size() ; i++){
        if (i > startingIndex) indexes.pop_back();
        indexes.push_back(i);
        if (indexes.size() == 2){
            AnalysisObjects pair;
            for (uint k : indexes){
                pair.push_back(particles[k]);
            }
            pairs.push_back(pair);
        } else {
            std::vector<int> clonedIndexes(indexes);
            getPairs(particles, clonedIndexes, pairs);
        }
    }
}


void WeightAnalysis_Res::PlotWeightFunctions(){
    float pt_f = 0;
    float eta_f = 0;
    for(int j=0;j<1000;j++){ //Begin eta loop
        for(int i=0;i<1000;i++){ //Begin pt loop
            float e_weight = TruthSmear::getElectronWeight(pt_f*1000,eta_f,m_upgrade);
            float mu_weight = TruthSmear::getMuonWeight(pt_f*1000,eta_f,m_upgrade);
            float tau_weight_prong_1 = TruthSmear::getTauWeight(pt_f*1000,eta_f,1,2,m_upgrade); //tight taus
            float tau_weight_prong_3 = TruthSmear::getTauWeight(pt_f*1000,eta_f,3,2,m_upgrade); //tight taus
            float photon_weight = TruthSmear::getPhotonWeight(pt_f*1000,m_upgrade);
            float photon_fake_from_electron_weight = TruthSmear::getPhotonFakeFromElectronWeight(pt_f*1000,eta_f,m_upgrade);
            float e_fake_weight = TruthSmear::getElectronFakeWeight(pt_f*1000,eta_f,m_upgrade);
            float tau_fake_weight_prong_1 = TruthSmear::getTauFakeWeight(pt_f*1000,eta_f,1,2,m_upgrade);  //tight taus
            float tau_fake_weight_prong_3 = TruthSmear::getTauFakeWeight(pt_f*1000,eta_f,3,2,m_upgrade);  //tight taus
            float photon_fake_weight = TruthSmear::getPhotonFakeWeight(pt_f*1000,m_upgrade);
            float pu_weight_hs = TruthSmear::getJetWeight(pt_f*1000,eta_f,"HS",m_upgrade);
            float pu_weight_pu = TruthSmear::getJetWeight(pt_f*1000,eta_f,"PU",m_upgrade);
            float photon_pu_fake_weight = TruthSmear::getPileupFakePhotonWeight(pt_f*1000,m_upgrade);

            setBinContent("electronWeight_Function",i,j,e_weight);
            setBinContent("muonWeight_Function",i,j,mu_weight);
            setBinContent("tauWeight_prong_1_Function",i,j,tau_weight_prong_1);
            setBinContent("tauWeight_prong_3_Function",i,j,tau_weight_prong_3);
            setBinContent("photonWeight_Function",i,j,photon_weight);
            setBinContent("photonFakeWeightFromElectron_Function",i,j,photon_fake_from_electron_weight);
            setBinContent("electronFakeWeight_Function",i,j,e_fake_weight);
            setBinContent("tauFakeWeight_prong_1_Function",i,j,tau_fake_weight_prong_1);
            setBinContent("tauFakeWeight_prong_3_Function",i,j,tau_fake_weight_prong_3);
            setBinContent("photonFakeWeight_Function",i,j,photon_fake_weight);
            setBinContent("jetWeight_HS_Function",i,j,pu_weight_hs);
            setBinContent("jetWeight_PU_Function",i,j,pu_weight_pu);
            setBinContent("pileup_photonFakeWeight_Function",i,j,photon_pu_fake_weight);

            pt_f = pt_f + 300.0/1000.0; //iterate pt from 0 to 120 in .12(GeV) steps
        } // End pt loop
        eta_f = eta_f + 5.0/1000.0; //iterate eta from 0 to 5 in .005 steps
        pt_f = 0;
    }
}

bool WeightAnalysis_Res::MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, //particles
                                       const double metetx, const double metety,           //met
                                       double &mass, double &xp1, double &xp2)
{ //result
    TMatrixD K(2, 2);
    K(0, 0) = k1.Px();
    K(0, 1) = k2.Px();
    K(1, 0) = k1.Py();
    K(1, 1) = k2.Py();

    if (K.Determinant() == 0)
        return false;

    TMatrixD M(2, 1);
    M(0, 0) = metetx;
    M(1, 0) = metety;

    TMatrixD Kinv = K.Invert();

    TMatrixD X(2, 1);
    X = Kinv * M;

    double X1 = X(0, 0);
    double X2 = X(1, 0);
    double x1 = 1. / (1. + X1);
    double x2 = 1. / (1. + X2);

    TLorentzVector p1 = k1 * (1. / x1);
    TLorentzVector p2 = k2 * (1. / x2);

    double m = (p1 + p2).M();

    //return to caller
    mass = m;

    if (k1.Pt() > k2.Pt())
    {
        xp1 = x1;
        xp2 = x2;
    }
    else
    {
        xp1 = x2;
        xp2 = x1;
    }

    return true;
}

void WeightAnalysis_Res::calculate_MET_HPTO(AnalysisObjects mainParticles, AnalysisObjects jets, double &MET_HPTO, double &MET_HPTOx, double &MET_HPTOy){
    MET_HPTOx= mainParticles[0].Px() + mainParticles[1].Px();
    MET_HPTOy= mainParticles[0].Py() + mainParticles[1].Py();
    for (uint i = 0 ; i < jets.size() ; i++){
        MET_HPTOx += jets[i].Px();
        MET_HPTOy += jets[i].Py();
    }
    MET_HPTO = sqrt(MET_HPTOx*MET_HPTOx + MET_HPTOy*MET_HPTOy);
}

bool WeightAnalysis_Res::isRecoOverlap(AnalysisObject origin, AnalysisObject reco){
    return (reco.index() == -1) && (reco.getRecoIndex() == origin.getRecoIndex());
}

int WeightAnalysis_Res::getRecoSource(AnalysisObject reco, AnalysisObjects recoSources) {
    for (int i = 0 ; i < recoSources.size() ; i++) {
        auto source = recoSources[i];
        if (isRecoOverlap(source, reco)){
            return i;
        }
    }
    return -1;
}


