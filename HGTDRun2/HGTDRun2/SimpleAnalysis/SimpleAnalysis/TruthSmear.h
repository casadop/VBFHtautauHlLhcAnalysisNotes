#ifndef TRUTHSMEAR_H
#define TRUTHSMEAR_H

#include <vector>
#include <string>

#include "TRandom3.h"

#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
#include <UpgradePerformanceFunctions/UpgradePerformanceFunctions.h>
#endif

class TruthEvent;
class AnalysisEvent;
// class UpgradePerformanceFunctions;

class TruthSmear
{
 public:
  TruthSmear(std::vector<std::string>&);
  TruthEvent *smearEvent(AnalysisEvent*);
  TruthEvent *weighedSmearing(AnalysisEvent*);
  static float getElectronWeight(float ptMev, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getElectronEfficiency(ptMev,eta);}
  static float getMuonWeight(float ptMev, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getMuonEfficiency(ptMev,eta);}
  static float getTauWeight(float ptMev, float eta, short prong, short wp,  UpgradePerformanceFunctions *upgrade) {return upgrade->getTauEfficiency(ptMev,eta,prong,wp);}
  static float getPhotonWeight(float ptMev, UpgradePerformanceFunctions *upgrade) {return upgrade->getPhotonEfficiency(ptMev);}
  static float getPhotonFakeFromElectronWeight(float ptMev, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getElectronToPhotonFakeRate(ptMev,eta);}
  static float getElectronFakeWeight(float ptMev, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getElectronFakeRate(ptMev, eta);}
  static float getTauFakeWeight(float ptMev, float eta, short prong, short wp, UpgradePerformanceFunctions *upgrade) {return upgrade->getTauFakeRate(ptMev, eta, prong, wp);}
  static float getPhotonFakeWeight(float ptMev, UpgradePerformanceFunctions *upgrade) {return upgrade->getPhotonFakeRate(ptMev);}
  static float getJetWeight(float pt, float eta, TString jetType, UpgradePerformanceFunctions *upgrade) {return upgrade->getTrackJetConfirmEff(pt, eta, jetType);}
  static float getPileupFakePhotonWeight(float pt,UpgradePerformanceFunctions *upgrade) {return upgrade->getPhotonPileupFakeRate(pt);}
  
  static float getDiTauTriggerEfficiency(float et1MeV, float et2MeV, float eta1, float eta2, short prong1, short prong2, UpgradePerformanceFunctions *upgrade) {return upgrade->getDiTauTriggerEfficiency(et1MeV, et2MeV, eta1, eta2, prong1, prong2);}

  static float getSingleMuonTriggerEfficiency(float etMeV, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getSingleMuonTriggerEfficiency(etMeV, eta);}
  static float getSingleElectronTriggerEfficiency(float etMeV, float eta, UpgradePerformanceFunctions *upgrade) {return upgrade->getSingleElectronTriggerEfficiency(etMeV, eta);}
  static float getDiMuonTriggerEfficiency(float et1MeV, float et2MeV, float eta1, float eta2, UpgradePerformanceFunctions *upgrade) {return upgrade->getDiMuonTriggerEfficiency(et1MeV,et2MeV, eta1, eta2);}


  int getID() {
    _ID++;
    return _ID;
  }

 private:
  std::string smearType;
  bool smearElectrons;
  bool smearMuons;
  bool smearTaus;
  bool smearPhotons;
  bool smearJets;
  bool smearMET;
  bool addPileupJets;
  bool useHGTD0;
  bool useHGTD1;
  bool smearJetPt;
  UpgradePerformanceFunctions *m_upgrade;
  TRandom3 m_random;
  int _ID = 1;

};



#endif
