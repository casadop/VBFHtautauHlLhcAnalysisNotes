#ifndef OUTPUTHANDLER_H
#define OUTPUTHANDLER_H
#include <fstream>
#include <iostream>
#include <map>
#include <unordered_map>
#include <string>
#include <vector>
#include <TH1.h>
#include <TTree.h>

class OutputHandler {
 public:
 OutputHandler(TDirectory *oFile,bool doNtuple=true) : _eventWeight(0),_ntuple(0),_oFile(oFile),_doNtuple(doNtuple) { 
    addEntry("All");
  };
  int addEntry(const std::string &label);
  void addEntries(const std::vector<std::string> &labels);
  void addHistogram(const std::string &label,int bins,float min,float max);
  void addHistogram(const std::string &label,int bins,float *edges);
  void addHistogram(const std::string &label,std::vector<float> &edges);
  void addHistogram(const std::string &label,
		    int binsX,float minX,float maxX,
		    int binsY,float minY,float maxY);

  void AddHistograms(int n_cuts, std::string prefix);
  void AddHistogramshh(int n_cuts, std::string prefix);
  //----
  void FillHistogramshh(int cutNumber,int n_taus, double ptTau1, double ptTau2, int n_leptons,double met, double met_HPTO, double deltaRtau, double deltaPhitau, double deltaEtatau, double RapiTaus, int n_jets, double ptJet1,double ptJet2, double RapiJets, double invMassJets, bool resultMass, double x1, double x2, double etaJet1, double etaJet2, double collmass, double mmc_mass, std::string prefix) ;

  void FillHistograms(int cutNumber,int n_muons, double ptMuon1, double ptMuon2, double invMassMuons, int n_leptons, int n_electrons, int n_taus,
                                     double met, double met_HPTO, double deltaRlep, double deltaEtalep, double deltaPhilep, int n_jets, double ptJet1,
                                     double ptJet2, double RapiJets, double invMassJets, int n_bjets, bool resultMass, double x1, double x2,
                                     double etaJet1, double etaJet2, double collmass, double mmc_mass, double etaMu1, double etaMu2, std::string prefix);

  //the following should only be called once per event and before the pass method is called
  void setEventWeight(double weight);
  double getEventWeight();
  void pass(const std::string &name,double weight=1);
  void fillHistogram(const std::string &label,double x);
  void fillHistogramWithoutWeight(const std::string &label,double x);
  void fillHistogram(const std::string &label,double x,double y);
  void setBinContent(const std::string &label,int x,int y,float count);
  void setBinContent(const std::string &label,int x,float count);
  void ntupVar(const std::string &label,int value);
  void ntupVar(const std::string &label,float value);
  void ntupVar(const std::string &label,double value);
  //void ntupVar(const std::string &label,double value) { ntupVar(label,(float) value); };
  void ntupVar(const std::string &label,std::vector<int> &values);
  void ntupVar(const std::string &label,std::vector<float> &values);
  void ntupVar(const std::string &label,std::vector<double> &values);
  //void ntupFill() { if (_ntuple) _ntuple->Fill(); };
  void ntupFill() { _ntuple->Fill(); };
  void saveRegions(std::ostream& filehandle,bool header=false);
  void title(std::string title) { _title=title+"__"; };

 protected:
  void createNtuple();
  void pass(int num,double weight=1);
  
 private:
  double _eventWeight;
  std::map<std::string,int> label2idx;
  std::map<int,std::string> idx2label;
  std::vector<int> eventCounts;
  std::vector<double> weightedSums;
  std::vector<double> weightedSquaredSums;
  std::string _title;
  std::unordered_map<std::string,TH1*> histograms;
  std::unordered_map<std::string,int> ntupInt;
  std::unordered_map<std::string,float> ntupFloat;
  std::unordered_map<std::string,double> ntupDouble;
  std::unordered_map<std::string,std::vector<int>* > ntupVectorInt;
  std::unordered_map<std::string,std::vector<float>* > ntupVectorFloat;
  std::unordered_map<std::string,std::vector<double>* > ntupVectorDouble;
  std::vector<int *> ntupInts;
  std::vector<float *> ntupFloats;
  std::vector<double *> ntupDoubles;
  std::vector<std::vector<int> *> ntupVectorInts;
  std::vector<std::vector<float> *> ntupVectorFloats;
  std::vector<std::vector<double> *> ntupVectorDoubles;
  TTree *_ntuple;
  TDirectory *_oFile;
  bool _doNtuple;
};
#endif
