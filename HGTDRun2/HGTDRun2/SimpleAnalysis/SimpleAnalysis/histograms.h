std::string histograms[26] = {"n_muons", "ptElectron1", "ptMuon1", "invMassMuons", "n_leptons", "n_electrons", "n_taus", "met", "met_HPTO", "deltaRlep", "deltaEtalep","deltaPhilep",
                                    "n_jets", "ptJet1", "ptJet2", "RapiJets", "invMassJets", "n_bjets",
                                    "x1", "x2", "etaJet1", "etaJet2", "collmass","mmc_mass","etaMu1","etaMu2"};

