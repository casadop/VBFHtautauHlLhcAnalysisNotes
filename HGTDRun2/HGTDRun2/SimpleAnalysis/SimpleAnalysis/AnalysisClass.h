#ifndef ANALYSISCLASS_H
#define ANALYSISCLASS_H

#include "SimpleAnalysis/OutputHandler.h"
#include "TLorentzVector.h"
#include <vector>
#include <algorithm>
#include <functional>
#include <RootCore/Packages.h>

#include "SimpleAnalysis/BDT.h"

#ifdef ROOTCORE_PACKAGE_Ext_RestFrames
#include "SimpleAnalysis/RestFramesHelper.h"
#endif

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
#endif

enum AnalysisObjectType
{
    ELECTRON,
    MUON,
    TAU,
    PHOTON,
    JET,
    FATJET,
    MET,
    COMBINED,
    NONE
};

enum AnalysisCommonID
{
    NotBit = 1 << 31
};
#define NOT(x) (NotBit | x)

enum AnalysisElectronID
{
    EVeryLooseLH = 1 << 0,
    ELooseLH = 1 << 1,
    EMediumLH = 1 << 2,
    ETightLH = 1 << 3,
    ELooseBLLH = 1 << 4,
    EIsoGradientLoose = 1 << 8,
    EIsoBoosted = 1 << 9, // from 3-bjet paper
    EIsoFixedCutTight = 1 << 10,
    EIsoLooseTrack = 1 << 11,
    EIsoLoose = 1 << 12,
    EIsoGradient = 1 << 13,
    EIsoFixedCutLoose = 1 << 14,
    EIsoFixedCutTightTrackOnly = 1 << 15,
    ED0Sigma5 = 1 << 16,
    EZ05mm = 1 << 17,
    EGood = EVeryLooseLH | ELooseLH | EMediumLH | ETightLH | ELooseBLLH | ED0Sigma5 | EZ05mm,
    EIsoGood = EGood | EIsoGradientLoose | EIsoBoosted | EIsoFixedCutTight | EIsoLooseTrack | EIsoLoose | EIsoGradient | EIsoFixedCutLoose | EIsoFixedCutTightTrackOnly
};

enum AnalysisMuonID
{
    MuLoose = 1 << 0,
    MuMedium = 1 << 1,
    MuTight = 1 << 2,
    MuVeryLoose = 1 << 3,
    MuIsoGradientLoose = 1 << 8,
    MuIsoBoosted = 1 << 9, // from 3-bjet paper
    MuIsoFixedCutTightTrackOnly = 1 << 10,
    MuIsoLooseTrack = 1 << 11,
    MuIsoLoose = 1 << 12,
    MuIsoGradient = 1 << 13,
    MuIsoFixedCutLoose = 1 << 14,
    MuD0Sigma3 = 1 << 16,
    MuZ05mm = 1 << 17,
    MuNotCosmic = 1 << 18,
    MuQoPSignificance = 1 << 19,
    MuCaloTaggedOnly = 1 << 20,
    MuGood = MuLoose | MuMedium | MuTight | MuVeryLoose | MuD0Sigma3 | MuZ05mm | MuNotCosmic | MuQoPSignificance,
    MuIsoGood = MuGood | MuIsoGradientLoose | MuIsoBoosted | MuIsoFixedCutTightTrackOnly | MuIsoLooseTrack
};

enum AnalysisTauID
{
    TauLoose = 1 << 0,
    TauMedium = 1 << 1,
    TauTight = 1 << 2,
    TauOneProng = 1 << 10,
    TauThreeProng = 1 << 11,
    TauGood = TauLoose | TauMedium | TauTight,
    TauIsoGood = TauGood
};

enum AnalysisPhotonID
{
    PhotonLoose = 1 << 0,
    PhotonTight = 1 << 1,
    PhotonIsoFixedCutLoose = 1 << 8,
    PhotonIsoFixedCutTight = 1 << 9,
    PhotonGood = PhotonLoose | PhotonTight,
    PhotonIsoGood = PhotonGood | PhotonIsoFixedCutLoose | PhotonIsoFixedCutTight
};

enum AnalysisJetID
{
    LooseBadJet = 1 << 8,
    TightBadJet = 1 << 9,
    JVT50Jet = 1 << 10,
    LessThan3Tracks = 1 << 11, // most signal jets should fail this
    GoodJet = LooseBadJet | TightBadJet | JVT50Jet,
    BTag85MV2c20 = 1 << 0,
    BTag80MV2c20 = 1 << 1,
    BTag77MV2c20 = 1 << 2,
    BTag70MV2c20 = 1 << 3,
    BTag85MV2c10 = 1 << 4,
    BTag77MV2c10 = 1 << 5,
    BTag70MV2c10 = 1 << 6,
    BTag60MV2c10 = 1 << 7,
    GoodBJet = BTag85MV2c20 | BTag80MV2c20 | BTag77MV2c20 | BTag70MV2c20 | BTag85MV2c10 | BTag77MV2c10 | BTag70MV2c10 | BTag60MV2c10 | GoodJet,
    TrueLightJet = 1 << 27, //These should not be used for actual analysis selection, only for understanding
    TrueCJet = 1 << 28,
    TrueBJet = 1 << 29,
    TrueTau = 1 << 30,

};

enum AnalysisFatJetID
{
    LooseFatJet = 1 << 8,
    GoodFatJet = LooseFatJet
};

class AnalysisObject : public TLorentzVector
{
  public:
    AnalysisObject(double Px, double Py, double Pz, double E, int charge, int id, AnalysisObjectType type, int orgIndex, float recoProb = 1., int recoIndex = 0) : 
                                                                                                                        TLorentzVector(Px, Py, Pz, E),
                                                                                                                           _charge(charge),
                                                                                                                           _id(id),
                                                                                                                           _type(type),
                                                                                                                           _orgIndex(orgIndex),
                                                                                                                           _reco_prob(recoProb),
                                                                                                                           _recoIndex(recoIndex){};
    AnalysisObject(TLorentzVector tlv, int charge, int id, AnalysisObjectType type, int orgIndex, float recoProb = 1., int recoIndex = 0) : 
                                                                                                    TLorentzVector(tlv),
                                                                                                    _charge(charge),
                                                                                                    _id(id),
                                                                                                    _type(type),
                                                                                                    _orgIndex(orgIndex),
                                                                                                    _reco_prob(recoProb),
                                                                                                    _recoIndex(recoIndex){};
    virtual bool pass(int id) const
    {
        if (id & NotBit)
            return (id & _id) == 0;
        else
            return (id & _id) == id;
    };
    virtual int charge() const
    {
        return _charge;
    };
    virtual AnalysisObjectType type() const
    {
        return _type;
    };
    virtual bool valid() const
    {
        return _type != NONE;
    };
    virtual int id() const
    {
        return _id;
    }; // not supposed to be used directly except to store in ntuples
    virtual int index() const
    {
        return _orgIndex;
    }; //for internal use
    AnalysisObject transFourVect() const
    {
        TLorentzVector tlv;
        tlv.SetPtEtaPhiM(Pt(), 0.0, Phi(), M());
        return AnalysisObject(tlv, charge(), id(), type(), _orgIndex);
    };

    double getRecoProbability()
    {
        return _reco_prob;
    }

    int getRecoIndex(){
        return _recoIndex;
    }

  private:
    int _charge; //not used for jets or photons
    int _id;
    AnalysisObjectType _type;
    int _orgIndex;
    
    float _reco_prob;
    int _recoIndex;
};

AnalysisObject operator+(const AnalysisObject &lhs, const AnalysisObject &rhs);

typedef std::vector<AnalysisObject> AnalysisObjects;

AnalysisObjects operator+(const AnalysisObjects &lhs, const AnalysisObjects &rhs);

class AnalysisEvent
{
    public:
        virtual AnalysisObjects getElectrons(float ptCut, float etaCut, int isolation = 1) = 0;
        virtual AnalysisObjects getMuons(float ptCut, float etaCut, int isolation = 1) = 0;
        virtual AnalysisObjects getTaus(float ptCut, float etaCut, int isolation = 1) = 0;
        virtual AnalysisObjects getPhotons(float ptCut, float etaCut, int isolation = 1) = 0;
        virtual AnalysisObjects getJets(float ptCut, float etaCut, int btag = 0) = 0;
        virtual AnalysisObjects getFatJets(float ptCut, float etaCut, int btag = 0) = 0;
        virtual AnalysisObject getMET() = 0;
        virtual float getSumET() = 0;
        virtual float getGenMET() = 0;
        virtual float getGenHT() = 0;
        virtual int getMCNumber() = 0;    //Temporary until better solution found
        virtual int getSUSYChannel() = 0; //Temporary until better solution found
        virtual int getPDF_id1() = 0;
        virtual float getPDF_x1() = 0;
        virtual float getPDF_pdf1() = 0;
        virtual int getPDF_id2() = 0;
        virtual float getPDF_x2() = 0;
        virtual float getPDF_pdf2() = 0;
        virtual float getPDF_scale() = 0;
        virtual bool isSignal() = 0;
        virtual std::vector<float> getMCWeights() = 0; //Temporary until better solution found
        virtual AnalysisObject getTruthParticle(const AnalysisObject &obj) = 0;
        virtual ~AnalysisEvent(){};
};

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
class BTaggingTruthTaggingTool;
#endif

class AnalysisClass;

std::vector<AnalysisClass *> *getAnalysisList(); //for automatically tracking which analyses have been defined

class AnalysisClass
{
  public:
    AnalysisClass(const std::string &name) : _name(name), _output(0) { getAnalysisList()->push_back(this); };
    AnalysisClass(){};
    virtual void Init(){};
    virtual void Init(std::vector<std::string>& analysisOptions){Init();};
    virtual void ProcessEvent(AnalysisEvent *event) = 0;
    virtual void Final(){};
    virtual ~AnalysisClass(){};
    virtual const std::string &name() { return _name; };
    virtual void setOutput(OutputHandler *output) { _output = output; };
    OutputHandler *getOutput() { return _output; };
    virtual void addRegion(const std::string &label) { _output->addEntry(label); };
    virtual void addRegions(const std::vector<std::string> &labels) { _output->addEntries(labels); };
    virtual void addHistogram(const std::string &label, int bins, float min, float max) { _output->addHistogram(label, bins, min, max); };
    virtual void addHistogram(const std::string &label, int bins, float *edges) { _output->addHistogram(label, bins, edges); }
    virtual void addHistogram(const std::string &label, std::vector<float> &edges) { _output->addHistogram(label, edges); }
    virtual void addHistogram(const std::string &label, int binsX, float minX, float maxX,
                              int binsY, float minY, float maxY) { _output->addHistogram(label, binsX, minX, maxX, binsY, minY, maxY); };

    virtual void AddHistograms(int n_cuts, std::string prefix = "") {_output->AddHistograms(n_cuts, prefix);};
    virtual void AddHistogramshh(int n_cuts, std::string prefix = "") {_output->AddHistogramshh(n_cuts, prefix);};
    //----
    virtual void FillHistogramshh(int cutNumber,int n_taus, double ptTau1, double ptTau2, int n_leptons,
                                  double met, double met_HPTO, double deltaRtau, double deltaPhitau, double deltaEtatau, double RapiTaus, int n_jets, double ptJet1,
                                  double ptJet2, double RapiJets, double invMassJets, bool resultMass, double x1, double x2,
                                  double etaJet1 = NULL, double etaJet2 = NULL, double collmass = NULL, double mmc_mass = -999999., std::string prefix = "") {
      _output->FillHistogramshh(cutNumber,n_taus,ptTau1,ptTau2, n_leptons, met, met_HPTO, deltaRtau, deltaPhitau, deltaEtatau, RapiTaus, n_jets, ptJet1, ptJet2, RapiJets, invMassJets, resultMass, x1, x2, etaJet1, etaJet2, collmass, mmc_mass, prefix);
    };

    virtual void FillHistograms(int cutNumber,int n_muons, double ptElectron1, double ptMuon1, double invMassMuons, int n_leptons, int n_electrons, int n_taus,
                                     double met, double met_HPTO, double deltaRlep, double deltaEtalep, double deltaPhilep,int n_jets, double ptJet1,
                                      double ptJet2, double RapiJets, double invMassJets, int n_bjets, bool resultMass, double x1, double x2,
                                      double etaJet1 = NULL, double etaJet2 = NULL, double collmass = NULL, double mmc_mass = -999999., double etaMu1 = NULL, double etaMu2 = NULL, std::string prefix = "") {
                                                _output->FillHistograms(
                                                cutNumber,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass,mmc_mass,etaMu1,etaMu2,prefix);
                                      };

    virtual void setBinContent(const std::string &label,int x,float count) {_output->setBinContent(label,x,count);};
    virtual void setBinContent(const std::string &label,int x,int y,float count) {_output->setBinContent(label,x,y,count);};
    virtual void setEventWeight(float eventWeight) {_output->setEventWeight(eventWeight);};
    virtual double getEventWeight() {return _output->getEventWeight();};
    virtual void accept(const std::string &name, double weight = 1) { _output->pass(name, weight); };
    virtual void fill(const std::string &name, double x) { _output->fillHistogram(name, x); };
    virtual void fillWithoutWeight(const std::string &name, double x) { _output->fillHistogramWithoutWeight(name, x); };
    virtual void fill(const std::string &name, double x, double y) { _output->fillHistogram(name, x, y); };
    void ntupVar(const std::string &label, int value) { _output->ntupVar(label, value); };
    void ntupVar(const std::string &label, float value) { _output->ntupVar(label, value); };

    void ntupVar(const std::string &label, double value) { _output->ntupVar(label, (float)value); };
    void ntupVar(const std::string &label, std::vector<int> values) { _output->ntupVar(label, values); };
    void ntupVar(const std::string &label, std::vector<float> values) { _output->ntupVar(label, values); };
    void ntupVar(const std::string &label, AnalysisObject &object, bool saveMass = false, bool saveType = false);
    void ntupVar(const std::string &label, AnalysisObjects &objects, bool saveMass = false, bool saveType = false);
    void ntupFill() { _output->ntupFill(); };

    // Helper functions
    static AnalysisObjects overlapRemoval(const AnalysisObjects &cands,
                                          const AnalysisObjects &others,
                                          float deltaR, int passId = 0);

    static AnalysisObjects overlapRemoval(const AnalysisObjects &cands,
                                          const AnalysisObjects &others,
                                          std::function<float(const AnalysisObject &, const AnalysisObject &)> radiusFunc,
                                          int passId = 0);

    static AnalysisObjects lowMassRemoval(const AnalysisObjects &cand, std::function<bool(const AnalysisObject &, const AnalysisObject &)>, float MinMass = 0, float MaxMass = FLT_MAX, int type = -1);

    static AnalysisObjects filterObjects(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsR(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsHS(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsPU(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsI(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsIhs(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
    static AnalysisObjects filterObjectsIhsSmear(const AnalysisObjects &cands,
                                         float ptCut, float etaCut = 100., int id = 0);
 
    static AnalysisObjects filterIsolated(const AnalysisObjects &cands, const AnalysisObjects &electrons, const AnalysisObjects &muons,
                                          const AnalysisObjects &taus, const AnalysisObjects &jets);

    static AnalysisObjects filterCrack(const AnalysisObjects &cands, float minEta = 1.37, float maxEta = 1.52);

    static int countObjects(const AnalysisObjects &cands,
                            float ptCut, float etaCut = 100., int id = 0);

    static float sumObjectsPt(const AnalysisObjects &cands,
                              unsigned int maxNum = 10000, float ptCut = 0);

    static float sumObjectsM(const AnalysisObjects &cands,
                             unsigned int maxNum = 10000, float mCut = 0);

    static void sortObjectsByPt(AnalysisObjects &cands);
    static float minDphi(const AnalysisObject &met, const AnalysisObjects &cands, unsigned int maxNum = 10000, float ptCut = 0);
    static float minDphi(const AnalysisObject &met, const AnalysisObject &cand);
    static float minDR(const AnalysisObjects &cands, unsigned int maxNum = 10000, float ptCut = 0);

    static float calcMCT(const AnalysisObject &o1, const AnalysisObject &o2);
    static float calcMT(const AnalysisObject &lepton, const AnalysisObject &met);
    static float calcMTmin(const AnalysisObjects &cands, const AnalysisObject &met, int maxNum = 10000);
    static float calcMT2(const AnalysisObject &o1, const AnalysisObject &o2, const AnalysisObject &met);
    static float calcAMT2(const AnalysisObject &o1, const AnalysisObject &o2, const AnalysisObject &met, float m1, float m2);

    static float aplanarity(const AnalysisObjects &jets);
    static float sphericity(const AnalysisObjects &jets);

    static bool IsSFOS(const AnalysisObject &L, const AnalysisObject &L1);
    static std::pair<float, float> DiZSelection(const AnalysisObjects &electrons, const AnalysisObjects &muons);
    static bool PassZVeto(const AnalysisObjects &electrons, const AnalysisObjects &Muons, float Window = 10);

    static AnalysisObjects reclusterJets(const AnalysisObjects &jets, float radius, float ptmin, float rclus = -1, float ptfrac = -1);

    std::vector<BDT::BDTReader *> m_BDTReaders;

#ifdef ROOTCORE_PACKAGE_Ext_RestFrames
    RestFramesHelper m_RF_helper;
#endif

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
    BTaggingTruthTaggingTool *m_btt;
#endif

  protected:
    std::string _name;
    OutputHandler *_output;
};

#define DefineAnalysis(ANALYSISNAME)                                                                                                                                  \
    class ANALYSISNAME : public AnalysisClass                                                                                                                         \
    {                                                                                                                                                                 \
      public:                                                                                                                                                         \
        ANALYSISNAME() : AnalysisClass(#ANALYSISNAME){};                                                                                                              \
        void Init();                                                                                                                                         \
        void ProcessEvent(AnalysisEvent *event);                                                                                                                      \
        bool MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, const double metetx, const double metety, double &mass, double &xp1, double &xp2); \
    };                                                                                                                                                                \
    static const AnalysisClass *ANALYSISNAME_instance __attribute__((used)) = new ANALYSISNAME();

#endif
