#include <sys/time.h>
#include "SimpleAnalysis/WeightAnalysis_Res.h"

void WeightAnalysis_Res::Init(std::vector<std::string>& analysisOptions)
{
    useTrigger = false;
    useHPTOmet = false;
    useRun2kinCuts = false;
    considerLeptonCombinations = false;
    useMcWeight = false;
    maxJetCombinations = 12;
    maxTauCombinations = 3;
    leadingJetPtCut = 40.;
    RapiJetsCut = 3.;
    invMassJetsCut = 500;
    muEtaCut = 2.5;
    jetEtaCut = 4.0;
    muPtCut = 10.;
    deltaRlepCut = 2.5;
    deltaEtalepCut = 1.5;
    
    plotWeightFunctions = false;
    considerLostWeights = false;
    skipCombinations = false;
    useMMC = true;
    maxElectronCombinations = 3;
    maxMuonCombinations = 3;
    excludedJetCombination = -1;
    DEBUG = 0;
    metCut = 55.;
    x1Cut_1 = 0.1;
    x1Cut_2 = 1.;
    x2Cut_1 = 0.1;
    x2Cut_2 = 1.;

    for (const auto& option : analysisOptions){
        if (option == "useTrigger") useTrigger = true;
        if (option == "useHPTOmet") useHPTOmet = true;
        if (option == "useRun2kinCuts") useRun2kinCuts = true;
        if (option == "plotWeightFunctions") plotWeightFunctions = true;
        if (option == "considerLeptonCombinations") considerLeptonCombinations = true;
        if (option == "considerLostWeights") considerLostWeights = true;
        if (option.find("useMcWeight") == 0) useMcWeight = true;
        if (option.find("skipCombinations") == 0) skipCombinations = true;
        if (option.find("useMMC") == 0) useMMC = true;
        if (option.find("maxJetCombinations=") == 0) {
            maxJetCombinations = std::stoi(option.substr(19,std::string::npos));
        }
        if (option.find("maxTauCombinations=") == 0) {
            maxTauCombinations = std::stoi(option.substr(19,std::string::npos));
        }
        if (option.find("leadingJetPtCut=") == 0) {
            leadingJetPtCut = std::stod(option.substr(16,std::string::npos));
        }
        if (option.find("RapiJetsCut=") == 0) {
            RapiJetsCut = std::stod(option.substr(12,std::string::npos));
        }
        if (option.find("invMassJetsCut=") == 0) {
            invMassJetsCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("muEtaCut=") == 0) {
            muEtaCut = std::stod(option.substr(9,std::string::npos));
        }
        if (option.find("jetEtaCut=") == 0) {
            jetEtaCut = std::stod(option.substr(10,std::string::npos));
        }
        if (option.find("muPtCut=") == 0) {
            muPtCut = std::stod(option.substr(8,std::string::npos));
        }
        if (option.find("deltaRlepCut=") == 0) {
            deltaRlepCut = std::stod(option.substr(13,std::string::npos));
        }
        if (option.find("deltaEtalepCut=") == 0) {
            deltaEtalepCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("maxElectronCombinations=") == 0) {
            maxElectronCombinations = std::stoi(option.substr(24,std::string::npos));
        }
        if (option.find("maxMuonCombinations=") == 0) {
            maxMuonCombinations = std::stoi(option.substr(20,std::string::npos));
        }
        if (option.find("excludedJetCombination=") == 0) {
            excludedJetCombination = std::stoi(option.substr(23,std::string::npos));
        }
        if (option.find("DEBUG=") == 0){
            DEBUG = std::stoi(option.substr(6,std::string::npos));
        }
        if (option.find("metCut=") == 0) {
	    metCut = std::stod(option.substr(7,std::string::npos));
	}
        if (option.find("x1Cut_1=") == 0) {
	    x1Cut_1 = std::stod(option.substr(8,std::string::npos));
	}
        if (option.find("x1Cut_2=") == 0) {
	    x1Cut_2 = std::stod(option.substr(8,std::string::npos));
	}
        if (option.find("x2Cut_1=") == 0) {
	    x2Cut_1 = std::stod(option.substr(8,std::string::npos));
	}
        if (option.find("x2Cut_2=") == 0) {
	    x2Cut_2 = std::stod(option.substr(8,std::string::npos));
	}
    }
    
    for (int i = 0 ; i < 18 ; i++) sumSquaredWeights[i] = 0;

    int seed = 12345;

    m_random.SetSeed(seed);

    m_upgrade = new UpgradePerformanceFunctions();
	//m_upgrade->setLayout(layout);
	m_upgrade->setLayout(UpgradePerformanceFunctions::Step1p6);
	m_upgrade->setAvgMu(200);
	m_upgrade->setElectronWorkingPoint(UpgradePerformanceFunctions::looseElectron);
	// m_upgrade->setElectronRandomSeed(seed);
	m_upgrade->setMuonWorkingPoint(UpgradePerformanceFunctions::tightMuon);
	m_upgrade->setPhotonWorkingPoint(UpgradePerformanceFunctions::tightPhoton);
	// m_upgrade->setTauRandomSeed(seed);
	// m_upgrade->setJetRandomSeed(seed);
	// m_upgrade->setMETRandomSeed(seed);
	//m_upgrade->loadMETHistograms(METhistfile);
	m_upgrade->loadMETHistograms("UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root");
	// m_upgrade->setPileupRandomSeed(seed);
	m_upgrade->setPileupUseTrackConf(true);
	m_upgrade->setPileupJetPtThresholdMeV(30000.);
	m_upgrade->setPileupEfficiencyScheme(UpgradePerformanceFunctions::PU);
	m_upgrade->setPileupEff(0.02);
	m_upgrade->setPileupTemplatesPath("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/");
	m_upgrade->initPhotonFakeHistograms("UpgradePerformanceFunctions/PhotonFakes.root");
	m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");

    addHistogram("n_muons_ini_0",10,0,10);
    addHistogram("n_muons_ini_1",10,0,10);
    addHistogram("n_muons_ini_2",10,0,10);
    addHistogram("electronWeight_Function",1000,0,300,1000,0,5);
    addHistogram("muonWeight_Function",1000,0,300,1000,0,5);
    addHistogram("tauWeight_prong_1_Function",1000,0,300,1000,0,5);
    addHistogram("tauWeight_prong_3_Function",1000,0,300,1000,0,5);
    addHistogram("photonWeight_Function",1000,0,300,1000,0,5);
    addHistogram("photonFakeWeightFromElectron_Function",1000,0,300,1000,0,5);
    addHistogram("electronFakeWeight_Function",1000,0,300,1000,0,5);
    addHistogram("tauFakeWeight_prong_1_Function",1000,0,300,1000,0,5);
    addHistogram("tauFakeWeight_prong_3_Function",1000,0,300,1000,0,5);
    addHistogram("photonFakeWeight_Function",1000,0,300,1000,0,5);
    addHistogram("jetWeight_HS_Function",1000,0,300,1000,0,5);
    addHistogram("jetWeight_PU_Function",1000,0,300,1000,0,5);
    addHistogram("pileup_photonFakeWeight_Function",1000,0,300,1000,0,5);

    addHistogram("truthMet", 30, 0, 300);
    addHistogram("sumET", 30, 0, 300);
    addHistogram("dphiMin4", 50, 0, 5);
    addHistogram("dphi1jet", 50, 0, 5);
    addHistogram("meffi",20,0,2000);
    addHistogram("meffi_Comb",20,0,2000);
    addHistogram("meffi_AllWeights",20,0,2000);

    addHistogram("ptMuon", 30, 0, 300);
    addHistogram("etaMuon", 20, -5.0, 5.0);
    addHistogram("etaJet", 20, -5.0, 5.0);
    addHistogram("phiMuon", 20, -5.0, 5.0);
    addHistogram("truthPtMuon", 30, 0, 300);
    addHistogram("truthEtaMuon", 20, -5.0, 5.0);
    addHistogram("truthPhiMuon", 20, -5.0, 5.0);

   
    addHistogram("resJet_Tight", 50, -0.5, 0.5);
    addHistogram("resJet2_Tight", 50, -0.25, 0.25);
    addHistogram("resJet1_Tight", 50, -1., 1.);
    addHistogram("resJet_Loose", 50, -0.5, 0.5);
    addHistogram("resJet2_Loose", 50, -0.25, 0.25);
    addHistogram("resJet1_Loose", 50, -1., 1.);

    
    addHistogram("ptEtaMuon",30,0,300,20,-5.0,5.0);
    addHistogram("ptEtaElectron",30,0,300,20,-5.0,5.0);

    
    addHistogram("weight_leptons",50,-0.1,1.1);
    addHistogram("weight_muon",50,-0.1,1.1);
    addHistogram("weight_jet",50,-0.1,1.1);
    addHistogram("weight_final",50,-0.1,1.1);
    addHistogram("weight_lost",50,-0.1,1.1);
    addHistogram("weight_final_squared",50,-0.1,1.1);

    addHistogram("pt_LowEnergeticJets_0", 50,0,200);
    addHistogram("met_HPTOx_0", 60,0,200);
    addHistogram("met_HPTOy_0", 60,0,200);
    
    
    addHistogram("MMC", 102, -1, 301);    
    addHistogram("muonEta1_jetEta1", 20, -5.0, 5, 20, -5.0, 5.0);
    addHistogram("tauEta1_jetEta1_outWeight", 20, -5.0, 5, 20, -5.0, 5.0);
    addHistogram("etaTau_weighted_AllCombinations",20,-5,5);
    addHistogram("etaJet_weighted_AllCombinations",20,-5,5);
    addHistogram("tauEta1_jetEta1_weighted_AllCombinations", 20, -5.0, 5, 20, -5.0, 5.0);
    addHistogram("DEtaTauEtaJet_weighted_AllCombinations",30, -2, 6);
    addHistogram("RapiTaus_etaTau1",15,0.,5.,20, -4.5, 4.5);
    addHistogram("DRtau_etaTau1",50, 0, 5,20, -4.5, 4.5);
    addHistogram("DRtau_phiTau1",50, 0, 5, 20, -5.0, 5.0);

    addHistogram("weight_squared_sum", 20,0,30);

    addHistogram("TwoDJetpTvsEtaAll", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaHS", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaPU", 18, 20., 200., 20, -5.0, 5.0);

    //addHistogram("time", 15, 0, 15000000);

    if (plotWeightFunctions) PlotWeightFunctions();

    fMMC.SetCalibrationSet(MMCCalibrationSet::MMC2016MC15C);

    AddHistogramshh(17);

}

void WeightAnalysis_Res::ProcessEvent(AnalysisEvent *event)
{
 
    if (!useMcWeight){
        setEventWeight(1.);
        current_mcWeight = 1;
    } else {
        current_mcWeight = getEventWeight();
    }

    //timeval timer;
    //gettimeofday(&timer, 0);
    //float seconds;

    auto electrons = event->getElectrons(0, 200);
    auto muons = event->getMuons(0, 200);

    auto taus = event->getTaus(0, 200);

    auto jets = event->getJets(0, 200);

    auto signalJets = filterObjects(jets, 0);

    auto signalElectrons = filterObjects(electrons, 0);

    auto signalMuons = filterObjects(muons, 0);

    auto signalLeptons = signalElectrons + signalMuons;
    auto leptons = electrons + muons;

    auto signalTaus = filterObjects(taus, 0);


    auto metVec = event->getMET();
    //metVec = event->getTruthParticle(metVec);
    //double met = metVec.Et();
    double met = metVec.Pt();
    auto sumET = event->getSumET();

    float dphiMin4 = minDphi(metVec, signalJets, 4);
    float dphi1jet = minDphi(metVec, signalJets, 1);

    TLorentzVector xNull(0., 0., 0., 0.);

    // Reco Taus
    auto recoTau = (signalTaus.size() > 0) ? signalTaus[0] : xNull;

    // Truth met
    auto truthMet = event->getTruthParticle(metVec);
    // Truth Electrons
    // auto truthElectron = (signalElectrons.size() > 0) ? event->getTruthParticle(signalElectrons[0]) : xNull;
    // Truth Muons
    // auto truthMuon = (signalMuons.size() > 0) ? event->getTruthParticle(signalMuons[0]) : xNull;
    // Truth Taus
    auto truthTau = (signalTaus.size() > 0) ? event->getTruthParticle(signalTaus[0]) : xNull;
    // Truth Jets
    AnalysisObject truthJet = (signalJets.size() > 0) ? event->getTruthParticle(signalJets[0]) : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    fill("truthMet", truthMet.Pt());
    fill("sumET", sumET);
    fill("dphiMin4", dphiMin4);
    fill("dphi1jet", dphi1jet);

    if (skipCombinations) return;

    AnalysisObjects losts;

    AnalysisObjects signalJets_UpToMaxCombinations;
    if (maxJetCombinations < signalJets.size()){
        for (uint i = 0 ; i < signalJets.size() ; i ++){
            if (i < maxJetCombinations) {signalJets_UpToMaxCombinations.push_back(signalJets[i]);}
            else {losts.push_back(signalJets[i]);}
        }
    } else {
        signalJets_UpToMaxCombinations = signalJets;
    }

    AnalysisObjects signalTaus_UpToMaxCombinations;
    if (maxTauCombinations < signalTaus.size()){
        for (uint i = 0 ; i < signalTaus.size() ; i ++){
            if (i < maxTauCombinations) {signalTaus_UpToMaxCombinations.push_back(signalTaus[i]);}
            else {losts.push_back(signalTaus[i]);}
        }
    } else {
        signalTaus_UpToMaxCombinations = signalTaus;
    }

    AnalysisObjects signalMuons_UpToMaxCombinations;
    if (maxMuonCombinations < signalMuons.size()){
        for (uint i = 0 ; i < signalMuons.size() ; i ++){
            if (i < maxMuonCombinations) {signalMuons_UpToMaxCombinations.push_back(signalMuons[i]);}
            else {losts.push_back(signalMuons[i]);}
        }
    } else {
        signalMuons_UpToMaxCombinations = signalMuons;
    }
    

    AnalysisObjects signalElectrons_UpToMaxCombinations;
    if (maxElectronCombinations < signalElectrons.size()){
        for (uint i = 0 ; i < signalElectrons.size() ; i ++){
            if (i < maxElectronCombinations) {signalElectrons_UpToMaxCombinations.push_back(signalElectrons[i]);}
            else {losts.push_back(signalElectrons[i]);}
        }
    } else {
        signalElectrons_UpToMaxCombinations = signalElectrons;
    }

    double lostWeight = 1;
    if (considerLostWeights){
        if (losts.size() > 0){
            double noneReco = 1;
            double allReco = 1;
            double oneReco = 0;
            for (uint i = 0 ; i < losts.size() ; i++) {
                noneReco *= 1 - losts[i].getRecoProbability();
                allReco *= losts[i].getRecoProbability();
                if (losts.size() > 1){
                    double oneReco_i = 1;
                    for (uint j = 0 ; j < losts.size() ; j++){
                        if (i == j) oneReco_i *= losts[j].getRecoProbability();
                        if (i != j) oneReco_i *= 1 - losts[j].getRecoProbability();
                    }
                    oneReco += oneReco_i;
                }
            }
            lostWeight = noneReco + allReco + oneReco;
        }
    }

    double pmissx = metVec.Px();
    double pmissy = metVec.Py();

    if (DEBUG > 0) {
        std::cout << "--------------------------------" << std::endl;
        std::cout << "There are " << signalTaus.size() << " taus, " << signalJets.size() << " jets " << electrons.size() << " electrons and " << muons.size() << " muons" << std::endl;
        std::cout << "Current sum squared of weights: ";
        for (int i = 0 ; i < 18 ; i++) std::cout << sumSquaredWeights[i] << " ";
        std::cout << std::endl;
    }

    if (signalTaus.size() > 0){
        for (uint i = 0 ; i <= signalTaus_UpToMaxCombinations.size() ; i++) {
            std::vector<int> recoTausIndices;
            std::vector<int> recoJetsIndices;
            std::vector<int> recoElectronsIndices;
            Combinations(i, signalTaus_UpToMaxCombinations, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus_UpToMaxCombinations, signalJets_UpToMaxCombinations, signalElectrons_UpToMaxCombinations, signalMuons_UpToMaxCombinations, 0, met, pmissx, pmissy, lostWeight, event);            
        }
    } else if (signalJets.size() > 0){
        for (uint j = 0 ; j <= signalJets_UpToMaxCombinations.size() ; j++){
            std::vector<int> recoTausIndices;
            std::vector<int> recoJetsIndices;
            std::vector<int> recoElectronsIndices;
            Combinations(j, signalJets_UpToMaxCombinations, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus_UpToMaxCombinations, signalJets_UpToMaxCombinations, signalElectrons_UpToMaxCombinations, signalMuons_UpToMaxCombinations, 1, met, pmissx, pmissy, lostWeight, event);            
        }
    } else if (signalElectrons.size() > 0 && considerLeptonCombinations) {
        for (uint k = 0 ; k <= signalElectrons_UpToMaxCombinations.size() ; k++){
            std::vector<int> recoTausIndices;
            std::vector<int> recoJetsIndices;
            std::vector<int> recoElectronsIndices;
            Combinations(k, signalElectrons_UpToMaxCombinations, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus_UpToMaxCombinations, signalJets_UpToMaxCombinations, signalElectrons_UpToMaxCombinations, signalMuons_UpToMaxCombinations, 2, met, pmissx, pmissy, lostWeight, event);            
        }
    } else if (signalMuons.size() > 0 && considerLeptonCombinations) {
        for (uint l = 0 ; l <= signalMuons_UpToMaxCombinations.size() ; l++){
            std::vector<int> recoTausIndices;
            std::vector<int> recoJetsIndices;
            std::vector<int> recoElectronsIndices;
            Combinations(l, signalMuons_UpToMaxCombinations, recoTausIndices, recoJetsIndices, recoElectronsIndices, signalTaus_UpToMaxCombinations, signalJets_UpToMaxCombinations, signalElectrons_UpToMaxCombinations, signalMuons_UpToMaxCombinations, 3, met, pmissx, pmissy, lostWeight, event);            
        }
    }
    if (DEBUG > 1) std::cout << std::endl;

    //setEventWeight(1.);
    //timeval timer1;
    //gettimeofday(&timer1, 0);
    //seconds = 1000000*timer1.tv_sec + timer1.tv_usec - 1000000*timer.tv_sec - timer.tv_usec;
    //std::cout << "Microseconds: " << seconds << std::endl;

    //fill("time", seconds);

    return;

}

void WeightAnalysis_Res::Analyze(std::vector<int> recoTausIndices, std::vector<int> recoJetsIndices, std::vector<int> recoElectronsIndices, std::vector<int> recoMuonsIndices,
                 AnalysisObjects signalTaus, AnalysisObjects signalJets, AnalysisObjects signalElectrons, AnalysisObjects signalMuons,
                 double met, double pmissx, double pmissy, double lostWeight, AnalysisEvent *event){

    setEventWeight(current_mcWeight);

    if (DEBUG > 1) {
        std::cout << "------------------------" << std::endl;
        std::cout << "Current sum squared of weights: ";
        for (int i = 0 ; i < 18 ; i++) std::cout << sumSquaredWeights[i] << " ";
        std::cout << std::endl;
    }

    if (DEBUG > 1 ){
        std::cout << "ANALYSING" << std::endl;
        std::cout << "Tau Combination: ";
        for (int i : recoTausIndices) {
            std::cout << i;
        }
        std::cout << std::endl;
        std::cout << "Jet Combination: ";
        for (int i : recoJetsIndices) {
            std::cout << i;
        }
        std::cout << std::endl;
        std::cout << "Electron Combination: ";
        for (int i : recoElectronsIndices) {
            std::cout << i;
        }
        std::cout << std::endl;
        std::cout << "Muon Combination: ";
        for (int i : recoMuonsIndices) {
            std::cout << i;
        }
        std::cout << std::endl;
    }

    AnalysisObjects recoTaus;
    for (uint i = 0 ; i < recoTausIndices.size() ; i++) {
        recoTaus.push_back(signalTaus[recoTausIndices[i]]);
    }
    AnalysisObjects recoElectrons;
    for (uint i = 0 ; i < recoElectronsIndices.size() ; i++) {
        recoElectrons.push_back(signalElectrons[recoElectronsIndices[i]]);
    }

    bool validCombination = true;
    for (auto tau : recoTaus){
        if (tau.index() == -1) {
            int i = getRecoSource(tau, signalJets);
            if (!contains(recoJetsIndices,i)){
                validCombination = false;
            }
        }
    }
    for (auto electrons : recoElectrons){
        if (electrons.index() == -1) {
            int i = getRecoSource(electrons, signalJets);
            if (!contains(recoJetsIndices,i)){
                validCombination = false;
            }
        }
    }
    if (!validCombination) return;

    AnalysisObjects recoJets;
    for (uint i = 0 ; i < recoJetsIndices.size() ; i++) {
        recoJets.push_back(signalJets[recoJetsIndices[i]]);
    }
    AnalysisObjects recoMuons;
    for (uint i = 0 ; i < recoMuonsIndices.size() ; i++) {
        recoMuons.push_back(signalMuons[recoMuonsIndices[i]]);
    }
    //float ePt = 15.;
    float ePt = 15.;
    float muPt = 10.;
    //float tauPt = 15.;
    float tauPt = 20.;
    //float jetPt = 20.;
    float jetPt = 30.;
    //float eEta = 4.;    
    float eEta = 2.47;
    float muEta = 2.5;
    //float tauEta = 4.;
    float tauEta = 2.5;
    float jetEta = 4.0;

    if (useRun2kinCuts){
        ePt = 15.;
        muPt = 10.;
        tauPt = 20.;
        jetPt = 30.;
        eEta = 2.47;
        muEta = 2.5;
        tauEta = 2.5;
        jetEta = 4.0;
    }

    recoElectrons = filterObjects(recoElectrons,ePt,eEta);
    recoMuons = filterObjects(recoMuons,muPt,muEta);
    recoTaus = filterObjects(recoTaus,tauPt,tauEta);
    recoJets = filterObjects(recoJets,jetPt,jetEta);


    auto radiusCalcJet = [](const AnalysisObject &, const AnalysisObject &muon) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcMuon = [](const AnalysisObject &muon, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcElec = [](const AnalysisObject &elec, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / elec.Pt()); };

    recoElectrons = overlapRemoval(recoElectrons, recoMuons, 0.01);    
    recoJets = overlapRemoval(recoJets, recoElectrons, 0.2);
    recoElectrons = overlapRemoval(recoElectrons, recoJets, radiusCalcElec);

    auto recoBJets = filterObjects(recoJets, jetPt, jetEta, GoodBJet);  //70 working point
    recoMuons = overlapRemoval(recoMuons, recoBJets, 0.2);

    recoJets = overlapRemoval(recoJets, recoMuons, radiusCalcJet);
    recoMuons = overlapRemoval(recoMuons, recoJets, radiusCalcMuon);
    recoTaus = overlapRemoval(recoTaus, recoElectrons, 0.1);
    recoJets = overlapRemoval(recoJets, recoTaus, 0.2);

    auto finalJets = filterObjects(recoJets, jetPt);

    auto finalElectrons = filterObjects(recoElectrons, ePt, eEta);
    auto finalMuons = filterObjects(recoMuons, muPt, muEta);

    auto finalLeptons = finalElectrons + finalMuons;

    auto finalTaus = filterObjects(recoTaus, tauPt);

    if (finalJets.size() == excludedJetCombination) return;

    double tauWeight = 1.;
    for (uint i = 0 ; i < signalTaus.size() ; i++) {
        if (contains(recoTausIndices,i)){
            tauWeight *= signalTaus[i].getRecoProbability();
        } else {
            if (signalTaus[i].index() == -1){
                int jetSource = getRecoSource(signalTaus[i],signalJets);
                if (contains(recoJetsIndices,jetSource)) tauWeight *= 1 - signalTaus[i].getRecoProbability();
            } else {
                tauWeight *= 1 - signalTaus[i].getRecoProbability();
            }
        }
    }

    double jetWeight = 1.;
    for (uint i = 0 ; i < signalJets.size() ; i++) {
        if (contains(recoJetsIndices,i)){
            jetWeight *= signalJets[i].getRecoProbability();
        } else {
            jetWeight *= 1 - signalJets[i].getRecoProbability();
        }
    }

/*
    double jetWeight = 1.;
    for (uint i = 0 ; i < signalJets.size() ; i++) {
        if (contains(recoJetsIndices,i)){
            if (signalJets[i].index()!=-1) jetWeight *= signalJets[i].getRecoProbability()*m_upgrade->getTrackJetConfirmEff(signalJets[i].E()*1000., signalJets[i].Eta(), "HS");
            if(signalJets[i].index()==-1) jetWeight*= signalJets[i].getRecoProbability()*m_upgrade->getTrackJetConfirmEff(signalJets[i].E()*1000., signalJets[i].Eta(), "PU");
        } else {
            if(signalJets[i].index()!=-1) jetWeight *= 1 - signalJets[i].getRecoProbability()*m_upgrade->getTrackJetConfirmEff(signalJets[i].E()*1000., signalJets[i].Eta(), "HS");
            if(signalJets[i].index()==-1) jetWeight *= 1 - signalJets[i].getRecoProbability()*m_upgrade->getTrackJetConfirmEff(signalJets[i].E()*1000., signalJets[i].Eta(), "PU");
        }
    }
*/
    double electronWeight = 1.;
    for (uint i = 0 ; i < signalElectrons.size() ; i++) {
        if (contains(recoElectronsIndices,i)){
            electronWeight *= signalElectrons[i].getRecoProbability();
        } else {
            if (signalElectrons[i].index() == -1){
                int jetSource = getRecoSource(signalElectrons[i],signalJets);
                if (contains(recoJetsIndices,jetSource)) electronWeight *= 1 - signalElectrons[i].getRecoProbability();
            } else {
                electronWeight *= 1 - signalElectrons[i].getRecoProbability();
            }
        }
    }

    double muonWeight = 1.;
    for (uint i = 0 ; i < signalMuons.size() ; i++) {
        if (contains(recoMuonsIndices,i)){
            muonWeight *= signalMuons[i].getRecoProbability();
        } else {
            muonWeight *= 1 - signalMuons[i].getRecoProbability();
        }
    }

    if (DEBUG > 1) std::cout << "Current event weight " << tauWeight * jetWeight * electronWeight * muonWeight * lostWeight << std::endl;
    double weightMC = current_mcWeight * tauWeight * jetWeight * electronWeight * muonWeight * lostWeight;
    setEventWeight(weightMC);
    float meffi = met + sumObjectsPt(finalJets) + sumObjectsPt(finalLeptons);
    //setEventWeight(current_mcWeight * tauWeight * jetWeightIni * electronWeight * muonWeight * lostWeight);
    setEventWeight(weightMC);
    fill("meffi", meffi);
    double weightComb = tauWeight * jetWeight * electronWeight * muonWeight * lostWeight;
    setEventWeight(weightComb);
    fill("meffi_Comb", meffi);

    bool passTriger = false;
    double triggerWeight = 0.;
    if (useTrigger) {

        std::vector<AnalysisObjects> tauPairs;
        std::vector<int> indexes;
        getPairs(signalTaus, indexes, tauPairs);

        //        bool passTriger = false;
        for (auto tauPair : tauPairs){
            float eff = TruthSmear::getDiTauTriggerEfficiency(tauPair[0].Pt()*1000, tauPair[1].Pt()*1000,
                                                             tauPair[0].Eta(), tauPair[1].Eta(),
                                                              1, 1, m_upgrade);
            triggerWeight=eff;
            if (m_random.Uniform(1.0) < eff) {
                passTriger = true;
                break;
            }
        }
        //ntupVar("Trigger_weight",triggerWeight);

    } else triggerWeight = 1.;


    double weightTot = triggerWeight*weightMC;
    setEventWeight(weightTot);
    fill("meffi_AllWeights", meffi);


    setEventWeight(weightMC);

    int n_taus = finalTaus.size();
    int n_electrons = finalElectrons.size();
    int n_muons = finalMuons.size();
    int n_leptons = n_electrons+n_muons;
    int n_jets = finalJets.size();
    double pt_Jet1 = NULL;
    double pt_Jet2 = NULL;
    double etaJet1 = NULL;
    double etaJet2 = NULL;
    float RapiJets = NULL;
    float invMassJets = NULL;
    bool resultMass = false;
    double x1, x2;
    double ptTau1 = NULL;
    double ptTau2 = NULL;
    double deltaEtatau = NULL;
    double deltaPhitau = NULL;
    double deltaRtau = NULL;
    float DRtau =  NULL;
    float RapiTaus = NULL;
    double collmass;//this is only defined and then directly fill in the histograms, does not change its value. leave?
    double mmc_mass = -999999.;//same as colmass, to erase them we have to modify FillHistogramshh function
    double MET_HPTO = NULL, MET_HPTOx = 0., MET_HPTOy = 0.;

    if (n_taus > 0) ptTau1 = finalTaus[0].Pt();
    if (n_taus > 1) {
        ptTau2 = finalTaus[1].Pt();
        DRtau = minDR(finalTaus, 2 ,20);
        RapiTaus = fabs(finalTaus[0].Eta() - finalTaus[1].Eta());
        // MET HPTO
        // MET_HPTOx= finalTaus[0].Px() + finalTaus[1].Px();
        // MET_HPTOy= finalTaus[0].Py() + finalTaus[1].Py();
        MET_HPTOx= -finalTaus[0].Px() - finalTaus[1].Px();
        MET_HPTOy= -finalTaus[0].Py() - finalTaus[1].Py();
        for (int i = 0 ; i < finalJets.size() ; i++){
            // MET_HPTOx += finalJets[i].Px();
            // MET_HPTOy += finalJets[i].Py();
            MET_HPTOx += -finalJets[i].Px();
            MET_HPTOy += -finalJets[i].Py();
        }
        MET_HPTO = sqrt(MET_HPTOx*MET_HPTOx + MET_HPTOy*MET_HPTOy);

        // COLLINEAR APPROXIMATION
        if (useHPTOmet){
            resultMass = MassCollinearCore(finalTaus[0], finalTaus[1], //particles
                                    MET_HPTOx, MET_HPTOy,
                                    collmass, x1, x2);
        } else {
            resultMass = MassCollinearCore(finalTaus[0], finalTaus[1], //particles
                                    pmissx, pmissy,                     
                                    collmass, x1, x2);
        }
    }
    if (finalJets.size() > 0)
    {
        pt_Jet1 = finalJets[0].Pt();
        etaJet1 = finalJets[0].Eta();
    }
    if (finalJets.size() > 1)
    {
        pt_Jet2 = finalJets[1].Pt();
        etaJet2 = finalJets[1].Eta();
        RapiJets = fabs(finalJets[0].Eta() - finalJets[1].Eta());
        invMassJets = (finalJets[0] + finalJets[1]).M();
    }

    for (int i = 2 ; i < finalJets.size() ; i++) {
        fill("pt_LowEnergeticJets_0", finalJets[i].Pt());
    }
    if (MET_HPTOx > 0){
        fill("met_HPTOx_0",MET_HPTOx);
        fill("met_HPTOy_0",MET_HPTOy);
    }

    if (finalTaus.size() > 1){
      deltaEtatau = finalTaus[0].Eta() - finalTaus[1].Eta();
      deltaPhitau = fabs(finalTaus[0].Phi() - finalTaus[1].Phi());
      if (deltaPhitau > 3.141592654) deltaPhitau = 2*3.141592654 - deltaPhitau;
      deltaRtau = sqrt(deltaEtatau*deltaEtatau + deltaPhitau*deltaPhitau);
}

    //std::cout << "1 mcWeight / tauWeight / jetWeight / electronWeight / muonWeight / lostWeight = " << current_mcWeight  << " / " <<  tauWeight  << " / " <<  jetWeight  << " / " <<  electronWeight  << " / " <<  muonWeight  << " / " <<  lostWeight << std::endl;
    //std::cout << "1 weightMC / triggerWeight / weightTot / n_taus_0 = " << weightMC << " / " << triggerWeight << " / " << weightTot << " / " << n_taus << std::endl;
    //Condition skimming
    double tauLead = 0.;
    double tauSubL = 0.;
    for(int i=0; i<finalTaus.size(); i++){
      if(finalTaus[i].Pt() > 33.) tauLead++;
      if(finalTaus[i].Pt() > 23.) tauSubL++;
    }
    if(!( (tauLead>=1) && (tauSubL>=2) )) return;


    //Event_weight = getEventWeight();

    //preselection cuts
    //if (!passTriger) return;//applying trigger
    if (!(finalJets.size() > 0)) return;
    if (!(finalJets[0].Pt() > 70. && fabs(finalJets[0].Eta()) < 3.2)) return;
    if (!(( x1 > 0.1 &&  x1 < 1.4) && (x2 > 0.1 && x2 < 1.4 ))) return;
    if(!(n_muons==0 && n_electrons==0)) return;//electrons+muons-->check
    if (!(RapiTaus < 2.)) return;
    if (!(deltaRtau < 2.8)) return;


    //ntupVar("mmc",mmc_mass1);
    //ntupVar("met",met);
//Selection 
//
    setEventWeight(weightTot);
    //setEventWeight(weightMC);

    //std::cout << "2 weightMC / triggerWeight / weightTot / n_taus_0 = " << weightMC << " / " << triggerWeight << " / " << weightTot << " / " << n_taus << std::endl;

    //no cut - HISTO n0
    FillHistogramshh(0, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[0] += weightTot * weightTot;


    // trigger - histo n1 for ow ot used
    FillHistogramshh(1, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[1] += weightTot * weightTot;


    //met >20 - HISTO n2
    //NB the option on the met cut changes wrt the option in the submission line
    if (useHPTOmet){
      if (!(MET_HPTO > 20.)) return;//met cut with HPTO method
    } else {
      if (!(met > 20.)) return;//met cut
    }
    FillHistogramshh(2, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[2] += weightTot * weightTot;

    //DR cut - HISTO n3
    if (!(n_taus >1)) return;
    if (!((deltaRtau > 0.8) && (deltaRtau < 2.5))) return;
    FillHistogramshh(3, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[3] += weightTot * weightTot;

    //pt leading tau >40 - HISTO n4 
    if (!(finalTaus[0].Pt() > 40.)) return;
    FillHistogramshh(4, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[4] += weightTot * weightTot;

    //pt subleading tau >30 - HISTO n5
    if (!(finalTaus[1].Pt() > 30.)) return;
    FillHistogramshh(5, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[5] += weightTot * weightTot;

    //x1 and x2 cut - HISTO n6
    if (!(( x1 > 0.1 &&  x1 < 1.4) && (x2 > 0.1 && x2 < 1.4 ))) return;//ok this goes after leading and subleading cut on tau pt
    FillHistogramshh(6, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
       sumSquaredWeights[6] += weightTot * weightTot;

    //no leptons, n_muons=0 and n_electrns=0 - HISTO n7
    if(!(n_muons==0 && n_electrons==0)) return;//electrons+muons-->check
    FillHistogramshh(7, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[7] += weightTot * weightTot;

    //two taus particle, same charge in absolute value - HISTO n8 
    //    if (!(n_taus == 2)) return;
    if (!(fabs(finalTaus[0].charge()) == 1. && fabs(finalTaus[1].charge()) == 1.)) return;
    FillHistogramshh(8, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[8] += weightTot * weightTot;

    //leading and subleading tau with opposite charge - HISTO n9
    if (!(finalTaus[0].charge() * finalTaus[1].charge() < 0.)) return;
    FillHistogramshh(9, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[9] += weightTot * weightTot;

    //(tau_0_n_charged_tracks==1tau_0_n_charged_tracks==3)&&(tau_1_n_charged_tracks==1tau_1_n_charged_tracks==3) - HISTO n10 NOT USED NOW
    //if () return;
    FillHistogramshh(10, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[10] += weightTot * weightTot;

    //ditau_mmc_mlm_fit_status==1 - HISTO n11 NT USED NOW
    //if () return;
    FillHistogramshh(11, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[11] += weightTot * weightTot;

    // CBA VBF inclusive
    //pt leading jet >70 and eta<3.2 - HISTO n12
    // if (!(finalJets[0].Pt() > 70.)) return;
    // if (!(fabs(finalJets[0].Eta()) < 3.2)) return;
    if (!(finalJets.size() > 0)) return;
    if (!(finalJets[0].Pt() > 70. && fabs(finalJets[0].Eta()) < 3.2)) return;
    FillHistogramshh(12, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[12] += weightTot * weightTot;

    //tau rapidity cut - HISTO n13
    if (!(RapiTaus < 1.5)) return;
    FillHistogramshh(13, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[13] += weightTot * weightTot;

    //requiring a subleading jet, if not program CRASH
    if (!(finalJets.size() > 1)) return;
    //leadingJet pT>70 && subleadingJet pT>30 && RapiJets>3. && invmass>400. - HISTO n14
    if (!(finalJets[0].Pt() > 70. && finalJets[1].Pt() > 30. && RapiJets > 3. && invMassJets > 400.)) return;
    FillHistogramshh(14, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[14] += weightTot * weightTot;

    //(leading jet eta < leading tau eta && leading tau eta < subleading jet eta) OR (subleading jet eta < leading tau eta && leading tau eta <leading jet eta)
    //leading tau topology - HISTO n15 leading tau central, tau produced in the center wrt jets
    if (!((finalJets[0].Eta() < finalTaus[0].Eta() && finalTaus[0].Eta() < finalJets[1].Eta()) || 
	  (finalJets[1].Eta() < finalTaus[0].Eta() && finalTaus[0].Eta() < finalJets[0].Eta()))) return;//eta leading tau - eta leading jet
    FillHistogramshh(15, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[15] += weightTot * weightTot;

    //subeading tau topology - HISTO n16
    if (!((finalJets[0].Eta() < finalTaus[1].Eta() && finalTaus[1].Eta() < finalJets[1].Eta()) || 
	  (finalJets[1].Eta() < finalTaus[1].Eta() && finalTaus[1].Eta() < finalJets[0].Eta()))) return;//eta leading jet vs eta leading tau
    FillHistogramshh(16, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[16] += weightTot * weightTot;

    //jets in two oposite emisphere opposite eta between eading and subleading jets - HISTO n17
    if (!(finalJets[0].Eta() * finalJets[1].Eta() < 0)) return;
    FillHistogramshh(17, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
        sumSquaredWeights[17] += weightTot * weightTot;

    for (int i = 0 ; i < finalJets.size() ; i++){
      fill("TwoDJetpTvsEtaAll",finalJets[i].Pt(),finalJets[i].Eta());
      if (finalJets[i].index()!=-1) fill("TwoDJetpTvsEtaHS",finalJets[i].Pt(),finalJets[i].Eta());
      if (finalJets[i].index()==-1) fill("TwoDJetpTvsEtaPU",finalJets[i].Pt(),finalJets[i].Eta());
    }


    return;
}

void WeightAnalysis_Res::Final() {
    for (int i = 0 ; i < 18 ; i++){
        setBinContent("weight_squared_sum",i,sumSquaredWeights[i]);
    }
    std::cout << "SUM SQUARED WEIGHTS: " << sumSquaredWeights[14] << std::endl;
}
