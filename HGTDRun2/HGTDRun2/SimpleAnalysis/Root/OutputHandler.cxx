#include <stdlib.h>
#include <math.h>

#include "SimpleAnalysis/OutputHandler.h"
// #include "SimpleAnalysis/histograms.h"

#include <TFile.h>
#include <TH1D.h>
#include <TH2D.h>


const int NUMBER_OF_HISTOGRAMShh = 21;
const int NUMBER_OF_HISTOGRAMS = 26;

std::string histograms_Nameshh[NUMBER_OF_HISTOGRAMShh] = {"ptJet1", "ptTau1", "ptTau2", "n_leptons", "met", "deltaRtau", "deltaPhitau", "deltaEtatau", "RapiTaus", "n_jets",
                                                      "n_taus", "ptJet2", "met_HPTO", "etaJet1", "etaJet2",
                                                      "RapiJets", "invMassJets", "x1", "x2", "collmass","mmc_mass"};

std::string histograms_Names[NUMBER_OF_HISTOGRAMS] = {"n_muons", "ptElectron1", "ptMuon1", "invMassMuons", "n_leptons", "n_electrons", "n_taus", "met", "met_HPTO", "deltaRlep", "deltaEtalep","deltaPhilep",
                                    "n_jets", "ptJet1", "ptJet2", "RapiJets", "invMassJets", "n_bjets",
                                    "x1", "x2", "etaJet1", "etaJet2", "collmass","mmc_mass","etaMu1","etaMu2"};

int OutputHandler::addEntry(const std::string &name) {
  int num=eventCounts.size();
  if (label2idx.find(name)!=label2idx.end()) {
    std::cerr<<"Duplicate signal region label: "<<name<<std::endl;
    exit(1);
  }
  if (name.find_first_not_of("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ01234567890_") != std::string::npos) {
    std::cerr << "Illegal signal region name: "<<name<<std::endl;
    std::cerr << "Signal region names should only have alphanumeric characters and '_'\n"<<std::endl;
    exit(1);
  }
  label2idx[name]=num;
  idx2label[num]=name;
  eventCounts.push_back(0);
  weightedSums.push_back(0);
  weightedSquaredSums.push_back(0);

  return num;
}

void OutputHandler::addEntries(const std::vector<std::string> &labels) {
  for(const auto& label: labels ) addEntry(label);
}

void OutputHandler::addHistogram(const std::string &label,int bins,float min,float max) {
  TH1 *hist=new TH1D((_title+label).c_str(),label.c_str(),bins,min,max);
  hist->SetDirectory(_oFile);
  histograms[label]=hist; 
}

void OutputHandler::addHistogram(const std::string &label,int bins,float *edges) {
  TH1 *hist=new TH1D((_title+label).c_str(),label.c_str(),bins,edges);
  hist->SetDirectory(_oFile);
  histograms[label]=hist;
}

void OutputHandler::addHistogram(const std::string &label,std::vector<float> &edges) {
  float* my_edges = new float[ edges.size() ];
  for (unsigned int e=0;e<edges.size();++e) my_edges[e] = edges[e];
  TH1 *hist=new TH1D((_title+label).c_str(),label.c_str(),edges.size(),my_edges);
  hist->SetDirectory(_oFile);
  histograms[label]=hist;
}

void OutputHandler::addHistogram(const std::string &label,
				 int binsX,float minX,float maxX,
				 int binsY,float minY,float maxY) {
  TH1 *hist=new TH2D((_title+label).c_str(),label.c_str(),binsX,minX,maxX,binsY,minY,maxY);
  hist->SetDirectory(_oFile);
  histograms[label]=hist; 
}

void OutputHandler::AddHistogramshh(int n_cuts, std::string prefix)
{
  for (int i = 0 ; i < NUMBER_OF_HISTOGRAMShh ; i++)
    {
      std::string histName = histograms_Nameshh[i];
      for (int j = 0 ; j < n_cuts + 1 ; j++)
        {
          if (histName.compare("n_taus") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -0.5, 19.5);
          //else if (histName.compare("ptTau1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 30, 0, 300);
          else if (histName.compare("ptTau1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 20, 220);
          //else if (histName.compare("ptTau2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 30, 0, 300);
          else if (histName.compare("ptTau2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 0, 100);
          else if (histName.compare("n_leptons") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -0.5, 19.5);
          //else if (histName.compare("met") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),60,0,200);
          else if (histName.compare("met") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,0,200);
          else if (histName.compare("met_HPTO") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,0,200);
          //else if (histName.compare("deltaRtau") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 50, 0, 5);
          else if (histName.compare("deltaRtau") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 25, 0, 2.5);
          else if (histName.compare("deltaPhitau") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 40, -2, 2);
          //else if (histName.compare("deltaEtatau") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 40, -2, 2);
          else if (histName.compare("deltaEtatau") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 40, -2, 2);
          else if (histName.compare("RapiTaus") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),15,0.,5.);
          else if (histName.compare("n_jets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 15, -0.5, 14.5);
          //else if (histName.compare("ptJet1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),50,0,200);
          else if (histName.compare("ptJet1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),21,40,250);
          else if (histName.compare("ptJet2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,0,100);
          else if (histName.compare("RapiJets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),60,0.,15.);
          //else if (histName.compare("invMassJets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),60,0,2500.);
          else if (histName.compare("invMassJets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,300,2300.);
          else if (histName.compare("resultMass") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,300,2300.);
          else if (histName.compare("x1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 60, 0., 1.5);
          else if (histName.compare("x2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 60, 0., 1.5);
          else if (histName.compare("etaJet1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -4.5, 4.5);
          else if (histName.compare("etaJet2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -4.5, 4.5);
          else if (histName.compare("collmass") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 402, -1, 401);
          else if (histName.compare("mmc_mass") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 302, -1, 301);


        }
    }
}

void OutputHandler::FillHistogramshh(int cutNumber,int n_taus, double ptTau1, double ptTau2, int n_leptons,
                                     double met, double met_HPTO, double deltaRtau, double deltaPhitau, double deltaEtatau, double RapiTaus, int n_jets, double ptJet1,
                                   double ptJet2, double RapiJets, double invMassJets, bool resultMass, double x1, double x2,
                                   double etaJet1 = NULL, double etaJet2 = NULL, double collmass = NULL, double mmc_mass = -999999., std::string prefix = "")
{
  for (int i = 0 ; i < NUMBER_OF_HISTOGRAMShh ; i++)
    {
      std::string histName = histograms_Nameshh[i];
      if (histName.compare("n_taus") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_taus);
      else if (histName.compare("ptTau1") == 0 && ptTau1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), ptTau1);
      else if (histName.compare("ptTau2") == 0 && ptTau2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), ptTau2);
      else if (histName.compare("n_leptons") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_leptons);
      else if (histName.compare("met") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),met);
      else if (histName.compare("met_HPTO") == 0 && met_HPTO != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),met_HPTO);
      else if (histName.compare("deltaRtau") == 0 && deltaRtau != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaRtau);
      else if (histName.compare("deltaPhitau") == 0 && deltaPhitau != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaPhitau);
      else if (histName.compare("deltaEtatau") == 0 && deltaEtatau != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaEtatau);
      else if (histName.compare("RapiTaus") == 0 && RapiTaus != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),RapiTaus);
      else if (histName.compare("n_jets") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_jets);
      else if (histName.compare("ptJet1") == 0 && ptJet1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),ptJet1);
      else if (histName.compare("ptJet2") == 0 && ptJet2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),ptJet2);
      else if (histName.compare("RapiJets") == 0 && RapiJets != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),RapiJets);
      else if (histName.compare("invMassJets") == 0 && invMassJets != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),invMassJets);
      else if (histName.compare("resultMass") == 0 && resultMass != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),resultMass);
      else if (histName.compare("x1") == 0 && resultMass) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),x1);
      else if (histName.compare("x2") == 0 && resultMass) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), x2);
      else if (histName.compare("etaJet1") == 0 && etaJet1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaJet1);
      else if (histName.compare("etaJet2") == 0 && etaJet2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaJet2);
      else if (histName.compare("collmass") == 0 && collmass != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),collmass);
      else if (histName.compare("mmc_mass") == 0 && mmc_mass != -999999.) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),mmc_mass);
    }
}

void OutputHandler::AddHistograms(int n_cuts, std::string prefix)
{
    for (int i = 0 ; i < NUMBER_OF_HISTOGRAMS ; i++)
    {
        std::string histName = histograms_Names[i];
        for (int j = 0 ; j < n_cuts + 1 ; j++)
        {

            if (histName.compare("n_muons") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 0, 20); //(intervals, range_min, range_max)
            else if (histName.compare("ptElectron1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 50, 0, 100);
            else if (histName.compare("ptMuon1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 50, 0, 100);
            else if (histName.compare("invMassMuons") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 50, 0, 150);
            else if (histName.compare("n_leptons") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 0, 20);
            else if (histName.compare("n_electrons") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 0, 20);
            else if (histName.compare("n_taus") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, 0, 20);
            else if (histName.compare("met") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,0,200);
            else if (histName.compare("met_HPTO") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,0,200);
            else if (histName.compare("deltaRlep") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 90, 0, 4.5);
            else if (histName.compare("deltaEtalep") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 40, -2, 2);
            else if (histName.compare("deltaPhilep") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 90, 0, 4.5);
            else if (histName.compare("n_jets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 15, 0, 15);
            else if (histName.compare("ptJet1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),22,30,250);
            else if (histName.compare("ptJet2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),22,30,250);
            else if (histName.compare("RapiJets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),60,0.,15.);
            else if (histName.compare("invMassJets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),20,300,2300.);
            else if (histName.compare("n_bjets") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 15, 0, 15);
            else if (histName.compare("x1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 60, 0., 1.5);
            else if (histName.compare("x2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 60, 0., 1.5);
            else if (histName.compare("etaJet1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -4.5, 4.5);
            else if (histName.compare("etaJet2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 20, -4.5, 4.5);
            else if (histName.compare("collmass") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 402, -1, 401);
            else if (histName.compare("mmc_mass") == 0) addHistogram(prefix + histName + "_" + std::to_string(j),152, -1, 151);
            else if (histName.compare("etaMu1") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 30, -4.5, 4.5);
            else if (histName.compare("etaMu2") == 0) addHistogram(prefix + histName + "_" + std::to_string(j), 30, -4.5, 4.5);
        }
    }
}

void OutputHandler::FillHistograms(int cutNumber,int n_muons, double ptElectron1, double ptMuon1, double invMassMuons, int n_leptons, int n_electrons, int n_taus,
                                   double met, double met_HPTO, double deltaRlep, double deltaEtalep, double deltaPhilep, int n_jets, double ptJet1,
                                   double ptJet2, double RapiJets, double invMassJets, int n_bjets, bool resultMass, double x1, double x2,
                                   double etaJet1 = NULL, double etaJet2 = NULL, double collmass = NULL, double mmc_mass = -999999., double etaMu1 = NULL, double etaMu2 = NULL, std::string prefix = "")
{
    for (int i = 0 ; i < NUMBER_OF_HISTOGRAMS ; i++)
    {
        std::string histName = histograms_Names[i];
        if (histName.compare("n_muons") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_muons);
        else if (histName.compare("ptElectron1") == 0 && ptElectron1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), ptElectron1);
        else if (histName.compare("ptMuon1") == 0 && ptMuon1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), ptMuon1);
        else if (histName.compare("invMassMuons") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), invMassMuons);
        else if (histName.compare("n_leptons") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_leptons);
        else if (histName.compare("n_electrons") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_electrons);
        else if (histName.compare("n_taus") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_taus);
        else if (histName.compare("met") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),met);
        else if (histName.compare("met_HPTO") == 0 && met_HPTO != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),met_HPTO);
        else if (histName.compare("deltaRlep") == 0 && deltaRlep != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaRlep);
        else if (histName.compare("deltaEtalep") == 0 && deltaEtalep != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaEtalep);
        else if (histName.compare("deltaPhilep") == 0 && deltaPhilep != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), deltaPhilep);
        else if (histName.compare("n_jets") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_jets);
        else if (histName.compare("ptJet1") == 0 && ptJet1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),ptJet1);
        else if (histName.compare("ptJet2") == 0 && ptJet2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),ptJet2);
        else if (histName.compare("RapiJets") == 0 && RapiJets != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),RapiJets);
        else if (histName.compare("invMassJets") == 0 && invMassJets != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),invMassJets);
        else if (histName.compare("n_bjets") == 0) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), n_bjets);
        else if (histName.compare("x1") == 0 && resultMass) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),x1);
        else if (histName.compare("x2") == 0 && resultMass) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber), x2);
        else if (histName.compare("etaJet1") == 0 && etaJet1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaJet1);
        else if (histName.compare("etaJet2") == 0 && etaJet2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaJet2);
        else if (histName.compare("collmass") == 0 && collmass != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),collmass);
        else if (histName.compare("mmc_mass") == 0 && mmc_mass != -999999.) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),mmc_mass);
        else if (histName.compare("etaMu1") == 0 && etaMu1 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaMu1);
        else if (histName.compare("etaMu2") == 0 && etaMu2 != NULL) fillHistogram(prefix + histName + "_" + std::to_string(cutNumber),etaMu2);
    }
}

void OutputHandler::pass(int num,double weight) {
  weight*=_eventWeight;
  if (idx2label.find(num)==idx2label.end()) {
    std::cerr<<"Unknown signal region number: "<<num<<std::endl;
    exit(1);
  }
  if (num==0) {
    ntupVar("eventWeight",weight);
  } else {
    ntupVar(idx2label[num],weight);
  }
  eventCounts[num]++;
  weightedSums[num]+=weight;
  weightedSquaredSums[num]+=weight*weight;
}

void OutputHandler::pass(const std::string & name,double weight) {
  if (label2idx.find(name)==label2idx.end()) {
    std::cerr<<"Unknown signal region label: "<<name<<std::endl;
    exit(1);
  }
  pass(label2idx[name],weight);
}

void OutputHandler::fillHistogram(const std::string &label,
				 double x) {
  if (histograms.find(label)==histograms.end()) {
    std::cerr<<"Unknown histogram label: "<<label<<std::endl;
    exit(1);
  }
  histograms[label]->Fill(x,_eventWeight);
}

void OutputHandler::fillHistogramWithoutWeight(const std::string &label,
				 double x) {
  if (histograms.find(label)==histograms.end()) {
    std::cerr<<"Unknown histogram label: "<<label<<std::endl;
    exit(1);
  }
  histograms[label]->Fill(x);
}

void OutputHandler::fillHistogram(const std::string &label,
				  double x,double y) {
  if (histograms.find(label)==histograms.end()) {
    std::cerr<<"Unknown histogram label: "<<label<<std::endl;
    exit(1);
  }
  ((TH2* ) histograms[label])->Fill(x,y,_eventWeight);
}

void OutputHandler::setBinContent(const std::string &label,
				  int x,float count) 
  {
    if (histograms.find(label)==histograms.end()) {
      std::cerr<<"Unknown histogram label: "<<label<<std::endl;
      exit(1);
  }
  ((TH1* ) histograms[label])->SetBinContent(x,count);
}

void OutputHandler::setBinContent(const std::string &label,
				  int x,int y,float count) 
  {
    if (histograms.find(label)==histograms.end()) {
      std::cerr<<"Unknown histogram label: "<<label<<std::endl;
      exit(1);
  }
  ((TH2* ) histograms[label])->SetBinContent(x,y,count);
}

void OutputHandler::setEventWeight(double weight) {
  for(auto & ntup : ntupInts) *ntup=0;
  for(auto & ntup : ntupFloats) *ntup=0;
  for(auto & ntup : ntupDoubles) *ntup=0;
  for(auto & ntupVec: ntupVectorFloats) ntupVec->clear();
  for(auto & ntupVec: ntupVectorDoubles) ntupVec->clear();
  for(auto & ntupVec: ntupVectorInts) ntupVec->clear();

  _eventWeight = weight;
  pass(0); //count all events
}

double OutputHandler::getEventWeight() {
  return _eventWeight;
}

void OutputHandler::createNtuple() {
  _ntuple=new TTree((_title+"ntuple").c_str(),"Simple Analysis ntuple");
  _ntuple->SetDirectory(_oFile);   
}

void OutputHandler::ntupVar(const std::string &label,int value) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupInt.find(label)==ntupInt.end()) {
    ntupInt[label]=0;
    TBranch *branch=_ntuple->Branch(label.c_str(),&(ntupInt[label]),(label+"/I").c_str());
    ntupInts.push_back(&(ntupInt[label]));
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  ntupInt[label]=value;
}

void OutputHandler::ntupVar(const std::string &label,float value) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupFloat.find(label)==ntupFloat.end()) {
    ntupFloat[label]=0;
    TBranch *branch=_ntuple->Branch(label.c_str(),&(ntupFloat[label]),(label+"/F").c_str());
    ntupFloats.push_back(&(ntupFloat[label]));
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  ntupFloat[label]=value;
}
void OutputHandler::ntupVar(const std::string &label,double value) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupDouble.find(label)==ntupDouble.end()) {
    ntupDouble[label]=0;
    TBranch *branch=_ntuple->Branch(label.c_str(),&(ntupDouble[label]),(label+"/F").c_str());
    ntupDoubles.push_back(&(ntupDouble[label]));
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  ntupDouble[label]=value;
}

void OutputHandler::ntupVar(const std::string &label,std::vector<float> &values) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupVectorFloat.find(label)==ntupVectorFloat.end()) {
    ntupVectorFloat[label]=new std::vector<float>;
    TBranch *branch=_ntuple->Branch(label.c_str(),ntupVectorFloat[label]);
    ntupVectorFloats.push_back(ntupVectorFloat[label]);
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  std::vector<float> *dest=ntupVectorFloat[label];
  for(float value : values)
    dest->push_back(value);
}
void OutputHandler::ntupVar(const std::string &label,std::vector<double> &values) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupVectorDouble.find(label)==ntupVectorDouble.end()) {
    //ntupVectorDouble[label]=new std::vector<double>;
    ntupVectorDouble[label]=new std::vector<double>;
    TBranch *branch=_ntuple->Branch(label.c_str(),ntupVectorDouble[label]);
    ntupVectorDoubles.push_back(ntupVectorDouble[label]);
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  std::vector<double> *dest=ntupVectorDouble[label];
  for(double value : values)
    dest->push_back(value);
}

void OutputHandler::ntupVar(const std::string &label,std::vector<int> &values) {
  //if (!_doNtuple) return;
  if (!_ntuple) createNtuple();
  //createNtuple();  
  if (ntupVectorInt.find(label)==ntupVectorInt.end()) {
    ntupVectorInt[label]=new std::vector<int>;
    TBranch *branch=_ntuple->Branch(label.c_str(),ntupVectorInt[label]);
    ntupVectorInts.push_back(ntupVectorInt[label]);
    for(int ii=0;ii<_ntuple->GetEntries();++ii) branch->Fill(); //backfill
  }
  std::vector<int> *dest=ntupVectorInt[label];
  for(int value : values)
    dest->push_back(value);
}


void OutputHandler::saveRegions(std::ostream& filehandle,bool header) {
  //  csv_ofile << "SR,Acceptance,Sum,WeightedSum,WeightedSquareSum"<<std::endl;
  if (header) {
    filehandle<<"SR,events,acceptance,err"<<std::endl;
    // Not so obvious, but we only want to save to ROOT file once
    _oFile->Write();
    _oFile->Close();
  }
  for(const auto& idx: idx2label) {
    filehandle<< _title+idx.second<<","<<eventCounts[idx.first];
    if (idx.first==0) {
      filehandle<<","<<weightedSums[0]<<","<<weightedSquaredSums[0];
    } else {
      double acc=weightedSums[idx.first]/weightedSums[0];
      double err=sqrt(weightedSquaredSums[idx.first])/weightedSums[0];
      filehandle<<","<<acc<<","<<err;
    }
    filehandle<<std::endl;
  }
}

