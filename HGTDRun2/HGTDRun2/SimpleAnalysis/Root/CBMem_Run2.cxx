#include "SimpleAnalysis/CBMem_Run2.h"

void CBMem_Run2::Init(std::vector<std::string>& analysisOptions)
{

    useTrigger = false;
    useMMC = false;
    useHPTOmet = false;
    useRun2kinCuts = false;
    channelEle = true;
    channelMu =false;
    int seed = 12345;

    for (const auto& option : analysisOptions){
        if (option == "useTrigger"){ useTrigger = true;}
        if (option == "useMMC"){ useMMC = true;}
        if (option == "useHPTOmet"){ useHPTOmet = true;}
        if (option == "useRun2kinCuts"){ useRun2kinCuts = true;}
        if (option == "channelEle"){ channelEle = true;}
        if (option == "channelMu"){ channelMu = true;}
        //else if (option.find("seed=") == 0) {
	//		seed = stoi(option.substr(5));
	//	}
	if (option.find("muEtaCut") == 0){
            muEtaCut = std::stod(option.substr(9,std::string::npos)); //9 tells the position after which the substring is formed
        }
        if (option.find("jetEtaCut") == 0){
            jetEtaCut = std::stod(option.substr(10,std::string::npos));
        }
        if (option.find("muPtCut") == 0){
            muPtCut = std::stod(option.substr(8,std::string::npos));
        }
        if (option.find("deltaRlepCut") == 0){
            deltaRlepCut = std::stod(option.substr(13,std::string::npos));
        }
        if (option.find("deltaEtalepCut") == 0){
            deltaEtalepCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("leadingJetPtCut") == 0){
            leadingJetPtCut = std::stod(option.substr(16,std::string::npos));
        }
        if (option.find("invMassJetsCut") == 0){
            invMassJetsCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("RapiJetsCut") == 0){
            RapiJetsCut = std::stod(option.substr(12,std::string::npos));
        }

    }

    for (int i = 0 ; i < 30 ; i++) sumSquaredWeights[i]=0;

    m_random.SetSeed(seed);

    m_upgrade = new UpgradePerformanceFunctions();
	//m_upgrade->setLayout(layout);
	m_upgrade->setLayout(UpgradePerformanceFunctions::Step1p6);
	m_upgrade->setAvgMu(200);
	m_upgrade->setElectronWorkingPoint(UpgradePerformanceFunctions::looseElectron);
	// m_upgrade->setElectronRandomSeed(seed);
	m_upgrade->setMuonWorkingPoint(UpgradePerformanceFunctions::tightMuon);
	m_upgrade->setPhotonWorkingPoint(UpgradePerformanceFunctions::tightPhoton);
	// m_upgrade->setTauRandomSeed(seed);
	// m_upgrade->setJetRandomSeed(seed);
	// m_upgrade->setMETRandomSeed(seed);
	//m_upgrade->loadMETHistograms(METhistfile);
	m_upgrade->loadMETHistograms("UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root");
	// m_upgrade->setPileupRandomSeed(seed);
	m_upgrade->setPileupUseTrackConf(true);
	m_upgrade->setPileupJetPtThresholdMeV(30000.);
	m_upgrade->setPileupEfficiencyScheme(UpgradePerformanceFunctions::PU);
	m_upgrade->setPileupEff(0.02);
	m_upgrade->setPileupTemplatesPath("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/");
	m_upgrade->initPhotonFakeHistograms("UpgradePerformanceFunctions/PhotonFakes.root");
	m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");

    addHistogram("multiMuonsIni", 10, 0, 10);
    addHistogram("multiMuonsIniB", 10, 0, 10);
    addHistogram("multiMuonsTot", 10, 0, 10);
    addHistogram("multiMuonsTotB", 10, 0, 10);
    addHistogram("multiMuonsHS", 10, 0, 10);
    addHistogram("multiMuonsPU", 10, 0, 10);
    addHistogram("truthMet", 30, 0, 300);
    addHistogram("sumET", 30, 0, 300);
    addHistogram("dphiMin4", 50, 0, 5);
    addHistogram("dphi1jet", 50, 0, 5);
    addHistogram("meffi",20,0,2000);
    addHistogram("meffi_Comb",20,0,2000);
    addHistogram("meffi_AllWeights",20,0,2000);

    addHistogram("ptMuon", 30, 0, 300);
    addHistogram("etaMuon", 20, -5.0, 5.0);
    addHistogram("etaJet", 20, -5.0, 5.0);
    addHistogram("phiMuon", 20, -5.0, 5.0);
    addHistogram("truthPtMuon", 30, 0, 300);
    addHistogram("truthEtaMuon", 20, -5.0, 5.0);
    addHistogram("truthPhiMuon", 20, -5.0, 5.0);

    addHistogram("pt_LowEnergeticJets_0", 50,0,200);
    addHistogram("met_HPTOx_0", 60,0,200);
    addHistogram("met_HPTOy_0", 60,0,200);

    addHistogram("weight_squared_sum", 30,0,30);
    addHistogram("TwoDJetpTvsEtaAll", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaHS", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaPU", 18, 20., 200., 20, -5.0, 5.0);

    addHistogram("multiPUOneHS_1", 18, 0., 10.);
    addHistogram("multiHS_1", 18, 0., 10.);
    addHistogram("multiPU_1", 18, 0., 10.);
    addHistogram("multiPUZeroHS_1", 18, 0., 10.);
    addHistogram("multiHS_24", 18, 0., 10.);
    addHistogram("multiPU_24", 18, 0., 10.);
    addHistogram("multiPUOneHS_24", 18, 0., 10.);
    addHistogram("multiPUZeroHS_24", 18, 0., 10.);
    AddHistograms(29);
 
    fMMC.SetCalibrationSet(MMCCalibrationSet::MMC2016MC15C);
}

void CBMem_Run2::ProcessEvent(AnalysisEvent *event)
{
    Event_weight = getEventWeight();

    float ePt = 15.;
    float muPt = 10.;
    float tauPt = 20.;
    float jetPt = 30.;
    float eEta = 2.47;
    float muEta = 2.5;
    float tauEta = 2.5;
    float jetEta = 4.0;

    if (useRun2kinCuts){
        ePt = 15.;
        muPt = 10.;
        tauPt = 20.;
        jetPt = 30.;
        eEta = 2.47;
        muEta = 2.5;
        tauEta = 2.5;
        jetEta = 4.0;
    }
    auto electrons = event->getElectrons(13., eEta);
    auto muons = event->getMuons(9.,muEta);
    auto taus = event->getTaus(tauPt, tauEta);

    auto jets = event->getJets(jetPt, jetEta);

    auto radiusCalcJet = [](const AnalysisObject &, const AnalysisObject &muon) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcMuon = [](const AnalysisObject &muon, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcElec = [](const AnalysisObject &elec, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / elec.Pt()); };

    electrons = overlapRemoval(electrons, muons, 0.01);
    jets = overlapRemoval(jets, electrons, 0.2);
    electrons = overlapRemoval(electrons, jets, radiusCalcElec);

    auto candBjets     = filterObjects(jets, jetPt, jetEta, GoodBJet);  //70 working point
    muons      = overlapRemoval(muons, candBjets, 0.2);

    jets = overlapRemoval(jets, muons, radiusCalcJet);
    muons = overlapRemoval(muons, jets, radiusCalcMuon);
    taus = overlapRemoval(taus, electrons, 0.1);
    jets = overlapRemoval(jets, taus, 0.2);

    auto finalJetsF = filterObjects(jets, jetPt, 4.0);

    auto signalElectrons = filterObjects(electrons, ePt, eEta);

    //auto signalElectrons = filterIsolated(electrons, electrons, muons, taus, finalJetsF);

    auto signalMuons = filterObjects(muons, muPt, muEta);
    //auto signalMuons = filterIsolated(muons, electrons, muons, taus, finalJetsF);

    auto signalLeptons = signalElectrons + signalMuons;

    auto signalTaus = filterObjects(taus, tauPt);

    int n_jets = finalJetsF.size();
    int n_bjets = candBjets.size();

    auto metVec = event->getMET();
    //auto metVec = event->getTruthParticle(metVec); //TruthMET
    double met = metVec.Et();
    //double met = metVec.Pt(); //truthMET
    auto sumET = event->getSumET();

    // inclusive - all jets + leptons
    float meffi = met + sumObjectsPt(finalJetsF) + sumObjectsPt(signalLeptons);
    // dphimin between leading 4 signal jets and met
    float dphiMin4 = minDphi(metVec, finalJetsF, 4);
    // leading lepton and met
    float mT = (signalLeptons.size() > 0) ? calcMT(signalLeptons[0], metVec) : 0.0;
    // sum of leading 4 reclustered jet masses
    //float mjsum = sumObjectsM(fatJets, 4);
    // dPhi(j1, MET) for Gbb
    float dphi1jet = minDphi(metVec, finalJetsF, 1);

    TLorentzVector xNull(0., 0., 0., 0.); //Lorentz vector (px,py,px,E)

    // Reco Electrons
    auto recoElectron = (signalElectrons.size() > 0) ? signalElectrons[0] : xNull;
    // Reco Muons
    auto recoMuon = (signalMuons.size() > 0) ? signalMuons[0] : xNull;
    // Reco Taus
    auto recoTau = (signalTaus.size() > 0) ? signalTaus[0] : xNull;
    // Reco Jets
    AnalysisObject recoJet = (finalJetsF.size() > 0) ? finalJetsF[0] : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    // Truth met
    auto truthMet = event->getTruthParticle(metVec);
    // Truth Electrons
    auto truthElectron = (signalElectrons.size() > 0) ? event->getTruthParticle(signalElectrons[0]) : xNull;
    // Truth Muons
    auto truthMuon = (signalMuons.size() > 0) ? event->getTruthParticle(signalMuons[0]) : xNull;
    // Truth Taus
    auto truthTau = (signalTaus.size() > 0) ? event->getTruthParticle(signalTaus[0]) : xNull;
    // Truth Jets
    AnalysisObject truthJet = (finalJetsF.size() > 0) ? event->getTruthParticle(finalJetsF[0]) : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    fill("truthMet", truthMet.Pt());
    fill("sumET", sumET);
    fill("dphiMin4", dphiMin4);
    fill("dphi1jet", dphi1jet);
    fill("meffi", meffi);
    double cweight = getEventWeight();
    setEventWeight(1.);

    fill("meffi_Comb", meffi);
    setEventWeight(cweight);

    float triggerWeight = 0.;
    float triggerWeighte = 0.;
    float triggerWeightm = 0.;
    if (useTrigger) {

        if (signalElectrons.size() > 0){
        float eff = TruthSmear::getSingleElectronTriggerEfficiency((signalElectrons[0].Pt())*1000, signalElectrons[0].Eta(), m_upgrade);
        triggerWeighte = eff;
        } else {
        float eff = TruthSmear::getSingleElectronTriggerEfficiency(0, 0, m_upgrade);
        triggerWeighte = eff;
        }

        if (signalMuons.size() > 0){
        float eff = TruthSmear::getSingleMuonTriggerEfficiency((signalMuons[0].Pt())*1000, signalMuons[0].Eta(), m_upgrade);
        triggerWeightm = eff;
        } else {
        float eff = TruthSmear::getSingleMuonTriggerEfficiency(0, 0, m_upgrade);
        triggerWeightm = eff;
        }
        triggerWeight = std::max(triggerWeighte,triggerWeightm); 
        //ntupVar("Trigger_weight",triggerWeight);
    } else triggerWeight = 1.;

    //triggerWeight = 1.; //Don't consider trigger weight to reproduce Roger's results.
 
    //cweight = getEventWeight();
    //setEventWeight(cweight*triggerWeight);

    fill("meffi_AllWeights",meffi);

    if (signalMuons.size() > 0)
    {
        fill("ptMuon", recoMuon.Pt());
        fill("etaMuon", recoMuon.Eta());
        fill("phiMuon", recoMuon.Phi());
        fill("truthPtMuon", truthMuon.Pt());
        fill("truthEtaMuon", truthMuon.Eta());
        fill("truthPhiMuon", truthMuon.Phi());

    }

    if (finalJetsF.size() > 0) {
        fill("etaJet",finalJetsF[0].Eta());
    }

    //Selection
/*
    float triggerWeight = 0.;
    if (useTrigger) {

        if (signalMuons.size() > 0){
        float eff = TruthSmear::getSingleMuonTriggerEfficiency((signalMuons[0].Pt())*1000, signalMuons[0].Eta(), m_upgrade);
        triggerWeight = eff;
        } else {
        float eff = TruthSmear::getSingleMuonTriggerEfficiency(0, 0, m_upgrade);
        triggerWeight = eff;
        }
        //ntupVar("Trigger_weight",triggerWeight);
    }
*/
    //std::cout << "Trigger weight = " << triggerWeight << std::endl;

    //double cweight = getEventWeight();
    //setEventWeight(cweight*triggerWeight);

    int n_taus = signalTaus.size();
    int n_muons = signalMuons.size();
    int n_electrons = signalElectrons.size();
    int n_leptons = signalLeptons.size();
    double ptJet1 = NULL;
    double ptJet2 = NULL;
    double etaJet1 = NULL;
    double etaJet2 = NULL;
    float RapiJets = NULL;
    float invMassJets = NULL;
    double invMassMuons = NULL;
    bool resultMass = false;
    int mmc_stat = 0;
    double x1, x2;
    double ptMuon1 = NULL;
    double ptMuon2 = NULL;
    double ptElectron1 = NULL;
    double ptElectron2 = NULL;
    double deltaEtalep =  NULL;
    double deltaPhilep =  NULL;
    double deltaRlep =  NULL;
    double pmissx = metVec.Px();
    double pmissy = metVec.Py();
    double collmass;
    double met_HPTO = NULL, met_HPTOx = 0., met_HPTOy = 0.;
    double mmc_mass1=-999999.;

    if (n_muons > 0) ptMuon1 = signalMuons[0].Pt();
    if (n_electrons > 0) ptElectron1 = signalElectrons[0].Pt();
    if (n_muons > 0 && n_electrons > 0) {
        // met HPTO
        met_HPTOx= - signalElectrons[0].Px() - signalMuons[0].Px();
        met_HPTOy= - signalElectrons[0].Py() - signalMuons[0].Py();
        for (int i = 0 ; i < finalJetsF.size() ; i++){
            met_HPTOx += - finalJetsF[i].Px();
            met_HPTOy += - finalJetsF[i].Py();
        }
        met_HPTO = sqrt(met_HPTOx*met_HPTOx + met_HPTOy*met_HPTOy);

        // COLLINEAR APPROXIMATION
        if (useHPTOmet){
            resultMass = MassCollinearCore(signalElectrons[0], signalMuons[0], //particles
                                    met_HPTOx, met_HPTOy,
                                    collmass, x1, x2);
        } else {
            resultMass = MassCollinearCore(signalElectrons[0], signalMuons[0], //particles
                                    pmissx, pmissy,                     
                                    collmass, x1, x2);
        }
    }

    if (finalJetsF.size() > 0)
    {
        ptJet1 = finalJetsF[0].Pt();
        etaJet1 = finalJetsF[0].Eta();
    }
    if (finalJetsF.size() > 1)
    {
        ptJet2 = finalJetsF[1].Pt();
        etaJet2 = finalJetsF[1].Eta();
        RapiJets = fabs(finalJetsF[0].Eta() - finalJetsF[1].Eta());
        invMassJets = (finalJetsF[0] + finalJetsF[1]).M();
    }

    for (int i = 2 ; i < finalJetsF.size() ; i++) {
        fill("pt_LowEnergeticJets_0", finalJetsF[i].Pt());
    }
    if (met_HPTOx > 0){
        fill("met_HPTOx_0",met_HPTOx);
        fill("met_HPTOy_0",met_HPTOy);
    }

    if (n_muons > 0 && n_electrons > 0){
      invMassMuons = (signalElectrons[0]+signalMuons[0]).M(); 
      deltaEtalep = signalElectrons[0].Eta() - signalMuons[0].Eta();
      deltaPhilep = fabs(signalElectrons[0].Phi() - signalMuons[0].Phi());
      if (deltaPhilep>3.141592654) deltaPhilep = 2.*3.141592654 - deltaPhilep;

      deltaRlep = sqrt(deltaEtalep*deltaEtalep+deltaPhilep*deltaPhilep);
    }

    if (useMMC){
        double sumET_HPTO = signalElectrons[0].Perp() + signalMuons[0].Perp();
        for (int i=0; i< finalJetsF.size();i++){
            sumET_HPTO = sumET_HPTO + finalJetsF[i].Perp();
        }

        TLorentzVector Met4Vec(0.,0.,0.,0.);
        Met4Vec.SetPxPyPzE(met_HPTOx,met_HPTOy,0.,met_HPTO);

        fMMC.SetMetVec(Met4Vec.EtaPhiVector()); // input: TVector2 for HPTO met
        fMMC.SetVisTauVec(0,signalElectrons[0]); // smeared lepton 4-vector; please read instruction above to properly set mass of leptons  
        fMMC.SetVisTauVec(1,signalMuons[0]); // smeared tau 4 vector; please read instructions above to properly set tau mass

        int tau0Type = 1;
        int tau1Type = 1;

        fMMC.SetVisTauType(0,tau0Type); // tau0Type=0 for electron and tau0Type=1 for muon 
        fMMC.SetVisTauType(1,tau1Type); // tau1Type=10 for 1-prongs and tau1Type=30 for 3-prongs
        fMMC.SetSumEt(sumET_HPTO);                  // passing event sumEt (RefFinal_BDT_medium)
        // std::cout << "sumET met / sumET HPTO = " << event->getSumET() << " / " << sumET_HPTO << std::endl;
        mmc_stat=fMMC.RunMissingMassCalculator(); // run MMC
        mmc_stat=fMMC.GetFitStatus();
        if(mmc_stat==1)
        {
            mmc_mass1=fMMC.GetFittedMass(MMCFitMethod::MLM); // recommended way to access MMC mass 
        }
        //ntupVar("MMC",mmc_mass1);
    }

    // Condition skimming
    double muLead = 0.;
    double eleLead = 0.;
    double muSubL = 0.;
    for (int i=0;i<electrons.size();i++){
      if (electrons[i].Pt() > 13. && ((fabs(electrons[i].Eta()) < 1.37) || (fabs(electrons[i].Eta()) > 1.52)) ) eleLead++;
    }
    for (int i=0;i<muons.size();i++){
      if (muons[i].Pt() > 13.) muLead++;
    }
    for (int i=0;i<muons.size();i++){
      if (muons[i].Pt() > 9.) muSubL++;
    }

    int countHS, countPU;
    Event_weight = getEventWeight();
    FillHistograms(0,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[0] += Event_weight * Event_weight;
    if (!(((muLead+eleLead)>=1)&&((muSubL+eleLead)>=2))) return;
     FillHistograms(1,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    countHS = 0;
    countPU = 0;
    for (int i=0;i<finalJetsF.size();i++){
      if (finalJetsF[i].index() != -1) countHS++;
      if (finalJetsF[i].index() == -1) countPU++;
    }
    fill("multiHS_1",countHS); 
    fill("multiPU_1",countPU); 
    if (countHS==0) fill("multiPUZeroHS_1",countPU); 
    if (countHS==1) fill("multiPUOneHS_1",countPU); 

    sumSquaredWeights[1] += Event_weight * Event_weight;
    if (!(finalJetsF.size() > 0)) return;
    FillHistograms(2,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[2] += Event_weight * Event_weight;
    if (!(finalJetsF[0].Pt() > 40.)) return;
    FillHistograms(3,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[3] += Event_weight * Event_weight;
    if (!(( x1 > 0.1 &&  x1 < 1.0) && (x2 > 0.1 && x2 < 1.0 ))) return;
    FillHistograms(4,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[4] += Event_weight * Event_weight;
    if (fabs(deltaEtalep) >= 1.5 ) return;
    FillHistograms(5,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[5] += Event_weight * Event_weight;
    if (deltaRlep >= 2.5 ) return;
    FillHistograms(6,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[6] += Event_weight * Event_weight;
    if (!useHPTOmet&&(met <= 20.)) return;
    FillHistograms(7,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
 
    sumSquaredWeights[7] += Event_weight * Event_weight;
    if (useHPTOmet&&(met_HPTO <= 20.)) return;
    Event_weight = getEventWeight();
    FillHistograms(8,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[8] += Event_weight * Event_weight;

    if (channelEle){
      if (!(signalElectrons.size()>0)) return;
      if (signalMuons.size()>0&&signalElectrons[0].Pt()<signalMuons[0].Pt()) return;
      
    } else if (channelMu){
      if (!(signalMuons.size()>0)) return;
      if (signalElectrons.size()>0&&signalMuons[0].Pt()<signalElectrons[0].Pt()) return;
    }

    FillHistograms(9,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[9] += Event_weight * Event_weight;

    if (channelEle){
      if (!(signalMuons.size()>0)) return;
      
    } else if (channelMu){
      if (!(signalElectrons.size()>0)) return;
    }

    FillHistograms(10,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[10] += Event_weight * Event_weight;
 
    if (!((signalElectrons.size()+signalMuons.size())==2)) return;
    FillHistograms(11,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[11] += Event_weight * Event_weight;

    if (!(signalTaus.size()==0)) return;
    FillHistograms(12,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[12] += Event_weight * Event_weight;

    Event_weight = Event_weight*triggerWeight;
    setEventWeight(Event_weight);
    FillHistograms(13,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[13] += Event_weight * Event_weight;
 
    if (!(signalElectrons.size()>0 && signalMuons.size()>0 && signalElectrons[0].charge()*signalMuons[0].charge() < 0.)) return;
    FillHistograms(14,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[14] += Event_weight * Event_weight;
 
    if (!((signalElectrons[0]+signalMuons[0]).M()>30. && (signalElectrons[0]+signalMuons[0]).M()< 100.)) return; //Gets de invariant mass of the Lorentz Vector muons[i]
    FillHistograms(15,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[15] += Event_weight * Event_weight;
 
    if (!(finalJetsF.size() > 0)) return;
    if (!(finalJetsF[0].Pt() > 40.)) return;
    FillHistograms(16,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[16] += Event_weight * Event_weight;
 
    if (!useHPTOmet&&(met <= 20.)) return;
    if (useHPTOmet&&(met_HPTO <= 20.)) return;
    FillHistograms(17,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[17] += Event_weight * Event_weight;
 
    //if (!(met_HPTO > 20.)) return;
    FillHistograms(18,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[18] += Event_weight * Event_weight;
   
    if (!(( x1 > 0.1 &&  x1 < 1.0) && (x2 > 0.1 && x2 < 1.0 ))) return;
    FillHistograms(19,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[19] += Event_weight * Event_weight;
  
    if (fabs(deltaEtalep) >= deltaEtalepCut ) return;
    FillHistograms(20,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[20] += Event_weight * Event_weight;

    if (deltaRlep >= 2.0 ) return;
    FillHistograms(21,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[21] += Event_weight * Event_weight;
 
    if (!(resultMass && collmass > 66.1876)) return;
    FillHistograms(22,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[22] += Event_weight * Event_weight;

    //if (mmc_stat!=1) return;
    FillHistograms(23,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[23] += Event_weight * Event_weight;

    if (candBjets.size()!=0) return;
    FillHistograms(24,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[24] += Event_weight * Event_weight;
    countHS = 0;
    countPU = 0;
    for (int i=0;i<finalJetsF.size();i++){
      if (finalJetsF[i].index() != -1) countHS++;
      if (finalJetsF[i].index() == -1) countPU++;
    }
    fill("multiHS_24",countHS); 
    fill("multiPU_24",countPU); 
    if (countHS==0) fill("multiPUZeroHS_24",countPU); 
    if (countHS==1) fill("multiPUOneHS_24",countPU); 

    // CBA VBF inclusive
    if (finalJetsF.size() < 2 ) return;
    //Subleading jet
    if (!(finalJetsF[1].Pt() > 30.)) return;
    FillHistograms(25,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[25] += Event_weight * Event_weight;
    
    if (fabs(finalJetsF[0].Eta()-finalJetsF[1].Eta()) <= 3.0) return;
    FillHistograms(26,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[26] += Event_weight * Event_weight;
   
    if (!(invMassJets > 400.)) return;
    FillHistograms(27,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[27] += Event_weight * Event_weight;
 
    if (finalJetsF[0].Eta()*finalJetsF[1].Eta() > 0.) return;
    FillHistograms(28,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[28] += Event_weight * Event_weight;

    if (!((finalJetsF[0].Eta() < signalElectrons[0].Eta() && signalElectrons[0].Eta() < finalJetsF[1].Eta()) ||
            (finalJetsF[1].Eta() < signalElectrons[0].Eta() && signalElectrons[0].Eta() < finalJetsF[0].Eta()))) return;


    if (!((finalJetsF[0].Eta() < signalMuons[0].Eta() && signalMuons[0].Eta() < finalJetsF[1].Eta()) ||
            (finalJetsF[1].Eta() < signalMuons[0].Eta() && signalMuons[0].Eta() < finalJetsF[0].Eta()))) return;

    FillHistograms(29,n_muons,ptElectron1,ptMuon1,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,deltaPhilep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[29] += Event_weight * Event_weight;


    for (int i = 0 ; i < finalJetsF.size() ; i++){
      fill("TwoDJetpTvsEtaAll",finalJetsF[i].Pt(),finalJetsF[i].Eta());
      if (finalJetsF[i].index()!=-1) fill("TwoDJetpTvsEtaHS",finalJetsF[i].Pt(),finalJetsF[i].Eta());
      if (finalJetsF[i].index()==-1) fill("TwoDJetpTvsEtaPU",finalJetsF[i].Pt(),finalJetsF[i].Eta());
    }

    return;
}

void CBMem_Run2::Final() {
    for  (int i = 0 ; i < 30 ; i++){
        setBinContent("weight_squared_sum",i,sumSquaredWeights[i]);
    }
    std::cout << "SUM SQUARED WEIGHTS: " << sumSquaredWeights[14] << std::endl;
}
