#include "SimpleAnalysis/CBM_SmearJets.h"

void CBM_SmearJets::Init(std::vector<std::string>& analysisOptions)
{

    useTrigger = false;
    useHPTOmet = false;
    useRun2kinCuts = false;
    int seed = 12345;

    for (const auto& option : analysisOptions){
        if (option == "useTrigger"){ useTrigger = true;}
        if (option == "useHPTOmet"){ useHPTOmet = true;}
        if (option == "useRun2kinCuts"){ useRun2kinCuts = true;}
        //else if (option.find("seed=") == 0) {
	//		seed = stoi(option.substr(5));
	//	}
	if (option.find("muEtaCut") == 0){
            muEtaCut = std::stod(option.substr(9,std::string::npos)); //9 tells the position after which the substring is formed
        }
        if (option.find("jetEtaCut") == 0){
            jetEtaCut = std::stod(option.substr(10,std::string::npos));
        }
        if (option.find("muPtCut") == 0){
            muPtCut = std::stod(option.substr(8,std::string::npos));
        }
        if (option.find("deltaRlepCut") == 0){
            deltaRlepCut = std::stod(option.substr(13,std::string::npos));
        }
        if (option.find("deltaEtalepCut") == 0){
            deltaEtalepCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("leadingJetPtCut") == 0){
            leadingJetPtCut = std::stod(option.substr(16,std::string::npos));
        }
        if (option.find("invMassJetsCut") == 0){
            invMassJetsCut = std::stod(option.substr(15,std::string::npos));
        }
        if (option.find("RapiJetsCut") == 0){
            RapiJetsCut = std::stod(option.substr(12,std::string::npos));
        }

    }

    for (int i = 0 ; i < 15 ; i++) sumSquaredWeights[i]=0;

    m_random.SetSeed(seed);

    m_upgrade = new UpgradePerformanceFunctions();
	//m_upgrade->setLayout(layout);
	m_upgrade->setLayout(UpgradePerformanceFunctions::Step1p6);
	m_upgrade->setAvgMu(200);
	m_upgrade->setElectronWorkingPoint(UpgradePerformanceFunctions::looseElectron);
	// m_upgrade->setElectronRandomSeed(seed);
	m_upgrade->setMuonWorkingPoint(UpgradePerformanceFunctions::tightMuon);
	m_upgrade->setPhotonWorkingPoint(UpgradePerformanceFunctions::tightPhoton);
	// m_upgrade->setTauRandomSeed(seed);
	// m_upgrade->setJetRandomSeed(seed);
	// m_upgrade->setMETRandomSeed(seed);
	//m_upgrade->loadMETHistograms(METhistfile);
	m_upgrade->loadMETHistograms("UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root");
	// m_upgrade->setPileupRandomSeed(seed);
	m_upgrade->setPileupUseTrackConf(true);
	m_upgrade->setPileupJetPtThresholdMeV(30000.);
	m_upgrade->setPileupEfficiencyScheme(UpgradePerformanceFunctions::PU);
	m_upgrade->setPileupEff(0.02);
	m_upgrade->setPileupTemplatesPath("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/");
	m_upgrade->initPhotonFakeHistograms("UpgradePerformanceFunctions/PhotonFakes.root");
	m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");

    addHistogram("truthMet", 30, 0, 300);
    addHistogram("sumET", 30, 0, 300);
    addHistogram("dphiMin4", 50, 0, 5);
    addHistogram("dphi1jet", 50, 0, 5);
    addHistogram("meffi",20,0,2000);
    addHistogram("meffi_AllWeights",20,0,2000);

    addHistogram("ptMuon", 30, 0, 300);
    addHistogram("etaMuon", 20, -5.0, 5.0);
    addHistogram("etaJet", 20, -5.0, 5.0);
    addHistogram("phiMuon", 20, -5.0, 5.0);
    addHistogram("truthPtMuon", 30, 0, 300);
    addHistogram("truthEtaMuon", 20, -5.0, 5.0);
    addHistogram("truthPhiMuon", 20, -5.0, 5.0);

    addHistogram("pt_LowEnergeticJets_0", 50,0,200);
    addHistogram("met_HPTOx_0", 60,0,200);
    addHistogram("met_HPTOy_0", 60,0,200);

    addHistogram("weight_squared_sum", 20,0,30);

    AddHistograms(14);
    
}

void CBM_SmearJets::ProcessEvent(AnalysisEvent *event)
{
    Event_weight = getEventWeight();

    float ePt = 20.;
    float muPt = muPtCut;
    float tauPt = 20.;
    float jetPt = 30.;
    float eEta = 3.8;
    float muEta = muEtaCut;
    float tauEta = 3.8;
    float jetEta = jetEtaCut;

    if (useRun2kinCuts){
        ePt = 15.;
        muPt = 10.;
        tauPt = 20.;
        jetPt = 30.;
        eEta = 2.47;
        muEta = 2.5;
        tauEta = 2.5;
        jetEta = 4.0;
    }

    auto electrons = event->getElectrons(ePt, eEta);
    auto muons = event->getMuons(muPt, muEta);
    auto taus = event->getTaus(tauPt, tauEta);

    auto jets = event->getJets(jetPt, jetEta);

    auto radiusCalcJet = [](const AnalysisObject &, const AnalysisObject &muon) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcMuon = [](const AnalysisObject &muon, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcElec = [](const AnalysisObject &elec, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / elec.Pt()); };

    electrons = overlapRemoval(electrons, muons, 0.01);
    jets = overlapRemoval(jets, electrons, 0.2);
    electrons = overlapRemoval(electrons, jets, radiusCalcElec);

    auto candBjets     = filterObjects(jets, jetPt, jetEta, GoodBJet);  //70 working point
    muons      = overlapRemoval(muons, candBjets, 0.2);

    jets = overlapRemoval(jets, muons, radiusCalcJet);
    muons = overlapRemoval(muons, jets, radiusCalcMuon);
    taus = overlapRemoval(taus, electrons, 0.1);
    jets = overlapRemoval(jets, taus, 0.2);

    auto signalJets = filterObjects(jets, jetPt);
    auto finalJetsFhs = filterObjectsIhs(jets, jetPt, 4.0);
    AnalysisObjects finalJetsF;
    for (int i=0;i<finalJetsFhs.size();i++){
      auto jetTruth = event->getTruthParticle(finalJetsFhs[i]);
      finalJetsF.push_back(jetTruth);
    } 
    auto signalElectrons = filterObjects(electrons, ePt, eEta);

    //auto signalElectrons = filterIsolated(electrons, electrons, muons, taus, finalJetsF);

    auto signalMuons = filterObjects(muons, muPt, muEta);
    //auto signalMuons = filterIsolated(muons, electrons, muons, taus, finalJetsF);

    auto signalLeptons = signalElectrons + signalMuons;

    auto signalTaus = filterObjects(taus, tauPt);

    int n_jets = finalJetsF.size();
    int n_bjets = candBjets.size();

    auto metVec = event->getMET();
    //auto metVec = event->getTruthParticle(metVec); //TruthMET
    double met = metVec.Et();
    //double met = metVec.Pt(); //truthMET
    auto sumET = event->getSumET();

    // inclusive - all jets + leptons
    float meffi = met + sumObjectsPt(finalJetsF) + sumObjectsPt(signalLeptons);
    // dphimin between leading 4 signal jets and met
    float dphiMin4 = minDphi(metVec, finalJetsF, 4);
    // leading lepton and met
    float mT = (signalLeptons.size() > 0) ? calcMT(signalLeptons[0], metVec) : 0.0;
    // sum of leading 4 reclustered jet masses
    //float mjsum = sumObjectsM(fatJets, 4);
    // dPhi(j1, MET) for Gbb
    float dphi1jet = minDphi(metVec, finalJetsF, 1);

    TLorentzVector xNull(0., 0., 0., 0.); //Lorentz vector (px,py,px,E)

    // Reco Electrons
    auto recoElectron = (signalElectrons.size() > 0) ? signalElectrons[0] : xNull;
    // Reco Muons
    auto recoMuon = (signalMuons.size() > 0) ? signalMuons[0] : xNull;
    // Reco Taus
    auto recoTau = (signalTaus.size() > 0) ? signalTaus[0] : xNull;
    // Reco Jets
    AnalysisObject recoJet = (finalJetsF.size() > 0) ? finalJetsF[0] : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    // Truth met
    auto truthMet = event->getTruthParticle(metVec);
    // Truth Electrons
    auto truthElectron = (signalElectrons.size() > 0) ? event->getTruthParticle(signalElectrons[0]) : xNull;
    // Truth Muons
    auto truthMuon = (signalMuons.size() > 0) ? event->getTruthParticle(signalMuons[0]) : xNull;
    // Truth Taus
    auto truthTau = (signalTaus.size() > 0) ? event->getTruthParticle(signalTaus[0]) : xNull;
    // Truth Jets
    AnalysisObject truthJet = (finalJetsF.size() > 0) ? event->getTruthParticle(finalJetsF[0]) : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    fill("truthMet", truthMet.Pt());
    fill("sumET", sumET);
    fill("dphiMin4", dphiMin4);
    fill("dphi1jet", dphi1jet);
    fill("meffi", meffi);

    float triggerWeight = 0.;
    if (useTrigger) {

        if (signalMuons.size() > 0){
        float eff = TruthSmear::getSingleMuonTriggerEfficiency((signalMuons[0].Pt())*1000, signalMuons[0].Eta(), m_upgrade);
        triggerWeight = eff;
        } else {
        float eff = TruthSmear::getSingleMuonTriggerEfficiency(0, 0, m_upgrade);
        triggerWeight = eff;
        }
        //ntupVar("Trigger_weight",triggerWeight);
    } else triggerWeight = 1.;
 
    double cweight = getEventWeight();
    setEventWeight(cweight*triggerWeight);

    fill("meffi_AllWeights",meffi);

    if (signalMuons.size() > 0)
    {
        fill("ptMuon", recoMuon.Pt());
        fill("etaMuon", recoMuon.Eta());
        fill("phiMuon", recoMuon.Phi());
        fill("truthPtMuon", truthMuon.Pt());
        fill("truthEtaMuon", truthMuon.Eta());
        fill("truthPhiMuon", truthMuon.Phi());

    }

    if (finalJetsF.size() > 0) {
        fill("etaJet",finalJetsF[0].Eta());
    }

    //Selection
/*
    float triggerWeight = 0.;
    if (useTrigger) {

        if (signalMuons.size() > 0){
        float eff = TruthSmear::getSingleMuonTriggerEfficiency((signalMuons[0].Pt())*1000, signalMuons[0].Eta(), m_upgrade);
        triggerWeight = eff;
        } else {
        float eff = TruthSmear::getSingleMuonTriggerEfficiency(0, 0, m_upgrade);
        triggerWeight = eff;
        }
        //ntupVar("Trigger_weight",triggerWeight);
    }
*/
    //std::cout << "Trigger weight = " << triggerWeight << std::endl;

    //double cweight = getEventWeight();
    //setEventWeight(cweight*triggerWeight);

    int n_taus = signalTaus.size();
    int n_muons = signalMuons.size();
    int n_electrons = signalElectrons.size();
    int n_leptons = signalLeptons.size();
    double ptJet1 = NULL;
    double ptJet2 = NULL;
    double etaJet1 = NULL;
    double etaJet2 = NULL;
    float RapiJets = NULL;
    float invMassJets = NULL;
    double invMassMuons = NULL;
    bool resultMass = false;
    double x1, x2;
    double ptMuon1 = NULL;
    double ptMuon2 = NULL;
    double deltaEtalep =  NULL;
    double deltaPhilep =  NULL;
    double deltaRlep =  NULL;
    double pmissx = metVec.Px();
    double pmissy = metVec.Py();
    double collmass;
    double met_HPTO = NULL, met_HPTOx = 0., met_HPTOy = 0.;

    if (n_muons > 0) ptMuon1 = signalMuons[0].Pt();
    if (n_muons > 1) {
        ptMuon2 = signalMuons[1].Pt();
        // met HPTO
        met_HPTOx= - signalMuons[0].Px() - signalMuons[1].Px();
        met_HPTOy= - signalMuons[0].Py() - signalMuons[1].Py();
        for (int i = 0 ; i < finalJetsF.size() ; i++){
            met_HPTOx += - finalJetsF[i].Px();
            met_HPTOy += - finalJetsF[i].Py();
        }
        met_HPTO = sqrt(met_HPTOx*met_HPTOx + met_HPTOy*met_HPTOy);

        // COLLINEAR APPROXIMATION
        if (useHPTOmet){
            resultMass = MassCollinearCore(signalMuons[0], signalMuons[1], //particles
                                    met_HPTOx, met_HPTOy,
                                    collmass, x1, x2);
        } else {
            resultMass = MassCollinearCore(signalMuons[0], signalMuons[1], //particles
                                    pmissx, pmissy,                     
                                    collmass, x1, x2);
        }
    }

    if (finalJetsF.size() > 0)
    {
        ptJet1 = finalJetsF[0].Pt();
        etaJet1 = finalJetsF[0].Eta();
    }
    if (finalJetsF.size() > 1)
    {
        ptJet2 = finalJetsF[1].Pt();
        etaJet2 = finalJetsF[1].Eta();
        RapiJets = fabs(finalJetsF[0].Eta() - finalJetsF[1].Eta());
        invMassJets = (finalJetsF[0] + finalJetsF[1]).M();
    }

    for (int i = 2 ; i < finalJetsF.size() ; i++) {
        fill("pt_LowEnergeticJets_0", finalJetsF[i].Pt());
    }
    if (met_HPTOx > 0){
        fill("met_HPTOx_0",met_HPTOx);
        fill("met_HPTOy_0",met_HPTOy);
    }

    if (muons.size() > 1){
      invMassMuons = (signalMuons[0]+signalMuons[1]).M(); 
      deltaEtalep = muons[0].Eta() - muons[1].Eta();
      deltaPhilep = fabs(muons[0].Phi() - muons[1].Phi());
      if (deltaPhilep>3.141592654) deltaPhilep = 2.*3.141592654 - deltaPhilep;

      deltaRlep = sqrt(deltaEtalep*deltaEtalep+deltaPhilep*deltaPhilep);
    }

    FillHistograms(0,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[0] += Event_weight * Event_weight;

    if (!(signalTaus.size()==0 && signalMuons.size()==2 && signalMuons[0].charge()*signalMuons[1].charge() < 0.)) return;
    FillHistograms(1,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[1] += Event_weight * Event_weight;
 
    if (!((signalMuons[0]+signalMuons[1]).M()>30. && (signalMuons[0]+signalMuons[1]).M()< 75.)) return; //Gets de invariant mass of the Lorentz Vector muons[i]
    FillHistograms(2,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[2] += Event_weight * Event_weight;
 
    if (!(finalJetsF.size() > 0)) return;
    FillHistograms(3,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[3] += Event_weight * Event_weight;
    
    if (!(finalJetsF[0].Pt() > leadingJetPtCut)) return;
    FillHistograms(4,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[4] += Event_weight * Event_weight;
 
    //if (!(met > 55.)) return;
    FillHistograms(5,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[5] += Event_weight * Event_weight;
 
    if (!(met_HPTO > 55.)) return;
    FillHistograms(6,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[6] += Event_weight * Event_weight;
   
    if (!(( x1 > 0.1 &&  x1 < 1.0) && (x2 > 0.1 && x2 < 1.0 ))) return;
    FillHistograms(7,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[7] += Event_weight * Event_weight;
  
    if (deltaRlep >= deltaRlepCut || fabs(deltaEtalep) >= deltaEtalepCut ) return;
    FillHistograms(8,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[8] += Event_weight * Event_weight;
 
    if (!(resultMass && collmass > 91.2 -25.)) return;
    FillHistograms(9,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[9] += Event_weight * Event_weight;
 
    // CBA VBF inclusive
    if (finalJetsF.size() < 2 ) return;
    FillHistograms(10,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[10] += Event_weight * Event_weight;
    
    if (fabs(finalJetsF[0].Eta()-finalJetsF[1].Eta()) <= RapiJetsCut) return;
    FillHistograms(11,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[11] += Event_weight * Event_weight;
   
    if (!(invMassJets > invMassJetsCut)) return;
    FillHistograms(12,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[12] += Event_weight * Event_weight;
 
    if (candBjets.size()!=0) return;
    FillHistograms(13,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[13] += Event_weight * Event_weight;
 
    if (finalJetsF[0].Eta()*finalJetsF[1].Eta() > 0.) return;
    FillHistograms(14,n_muons,ptMuon1,ptMuon2,invMassMuons,n_leptons,n_electrons,n_taus,
                                                met,met_HPTO,deltaRlep,deltaEtalep,n_jets,ptJet1,
                                                ptJet2,RapiJets,invMassJets,n_bjets,resultMass,x1,x2,
                                                etaJet1,etaJet2,collmass);
    sumSquaredWeights[14] += Event_weight * Event_weight;
    return;
}

void CBM_SmearJets::Final() {
    for  (int i = 0 ; i < 15 ; i++){
        setBinContent("weight_squared_sum",i,sumSquaredWeights[i]);
    }
    std::cout << "SUM SQUARED WEIGHTS: " << sumSquaredWeights[14] << std::endl;
}
