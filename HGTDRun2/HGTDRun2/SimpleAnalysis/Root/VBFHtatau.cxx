#include "SimpleAnalysis/AnalysisClass.h"
#include <RootCore/Packages.h>

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
static  BTaggingTruthTaggingTool *m_btt;
#endif

DefineAnalysis(VBFHtautau)


void VBFHtautau::Init()
{
  addRegions({
              "SR_Gbb_A",
              "SR_Gbb_B",
              "SR_Gbb_C",
              "SR_Gbb_D",
              "SR_Gtt_0l_A",
              "SR_Gtt_0l_B",
              "SR_Gtt_0l_C",
              "SR_Gtt_1l_A",
              "SR_Gtt_1l_B",
              "SR_Gtt_1l_C",
              "SR_0l_Hnj_Hmeff",
              "SR_0l_Hnj_Imeff",
              "SR_0l_Hnj_Lmeff",
              "SR_0l_IStR",
              "SR_0l_Inj_Imeff",
              "SR_0l_Inj_Lmeff",
              "SR_0l_Lnj_Hmeff",
              "SR_0l_Lnj_Imeff",
              "SR_0l_Lnj_Lmeff",
              "SR_1l_Hnj_Hmeff",
              "SR_1l_Hnj_Imeff",
              "SR_1l_Hnj_Lmeff",
              "SR_1l_Inj_Imeff",
              "SR_1l_Inj_Lmeff"
            });

  addRegions({
              "CR_Gbb_A",
              "CR_Gbb_B",
              "CR_Gbb_C",
              "CR_Gbb_D",
              "CR_Gtt_0l_A",
              "CR_Gtt_0l_B",
              "CR_Gtt_0l_C",
              "CR_Gtt_1l_A",
              "CR_Gtt_1l_B",
              "CR_Gtt_1l_C",
              "CR_Hnj_Hmeff",
              "CR_Hnj_Imeff",
              "CR_Hnj_Lmeff",
              "CR_IStR",
              "CR_Inj_Imeff",
              "CR_Inj_Lmeff",
              "CR_Lnj_Hmeff",
              "CR_Lnj_Imeff",
              "CR_Lnj_Lmeff"
            });

  addRegions({
              "VR1_Gtt_0l_A",
              "VR1_Gtt_0l_B",
              "VR1_Gtt_0l_C",
              "VR2_Gtt_0l_A",
              "VR2_Gtt_0l_B",
              "VR2_Gtt_0l_C",
              "VR1_Gtt_1l_A",
              "VR1_Gtt_1l_B",
              "VR1_Gtt_1l_C",
              "VR2_Gtt_1l_A",
              "VR2_Gtt_1l_B",
              "VR2_Gtt_1l_C",
              "VR1_Gbb_A",
              "VR1_Gbb_B",
              "VR1_Gbb_C",
              "VR1_Gbb_D",
              "VR2_Gbb_A",
              "VR2_Gbb_B",
              "VR2_Gbb_C",
              "VR2_Gbb_D",
              "VR0l_Lnj_Lmeff",
              "VR0l_Lnj_Imeff",
              "VR0l_Lnj_Hmeff",
              "VR0l_Inj_Lmeff",
              "VR0l_Inj_Imeff",
              "VR0l_Hnj_Lmeff",
              "VR0l_Hnj_Imeff",
              "VR0l_Hnj_Hmeff",
              "VR0l_IStR",
              "VR1l_Inj_Lmeff",
              "VR1l_Inj_Imeff",
              "VR1l_Hnj_Lmeff",
              "VR1l_Hnj_Imeff",
              "VR1l_Hnj_Hmeff"
            });

  //addHistogram("met",50,-2.,3.);
  addHistogram("x1",60,0.,1.2);
  addHistogram("x2",60,0.,1.2);
  addHistogram("x1F",60,0.,1.2);
  addHistogram("x2F",60,0.,1.2);
  addHistogram("met",60,0,200);
  addHistogram("met1TeV",100,0,1000);
  addHistogram("met1TeV_HPTO",100,0,1000);
  addHistogram("metvsmet_HPTO",100,0,1000,100,0,1999);
  addHistogram("deltaRlep",100,0.,10.);
  addHistogram("deltaRlepvsdeltaEtalep",100,0.,10.,50,-5.,5.);
  addHistogram("deltaEtalep",50,-5.,5.);
  addHistogram("truthMet",30,0,300);
  addHistogram("invMassJets",100,0,5000.);
  addHistogram("invMassIDR",100,0,5000.);
  addHistogram("invMassIDRPU",100,0,5000.);
  addHistogram("invMassIDRHS",100,0,5000.);
  addHistogram("multEvents",5,0,4.);

  addHistogram("sumET",30,0,300);
  addHistogram("meffi",20,0,2000);
  addHistogram("mT", 20, 0, 500);
  addHistogram("dphiMin4", 50, 0, 5);
  addHistogram("dphi1jet", 50, 0, 5);

  addHistogram("n_jets", 20, -0.5, 19.5);
  addHistogram("n_bjets", 20, -0.5, 19.5);
  addHistogram("n_jetsCheck", 20, -0.5, 19.5);
  addHistogram("n_electrons", 20, -0.5, 19.5);
  addHistogram("n_muons", 20, -0.5, 19.5);
  addHistogram("n_taus", 20, -0.5, 19.5);
  addHistogram("n_leptons", 20, -0.5, 19.5);

  //addHistogram("ptEle",60,-0.3,0.3);
  addHistogram("ptEle",50,0,200.);
  addHistogram("etaEle",60,-0.05,0.05);
  //addHistogram("etaEle",20,-5.0,5.0);
  addHistogram("phiEle",60,-0.05,0.05);
  //addHistogram("phiEle",20,-5.0,5.0);
  addHistogram("truthPtEle",30,0,300);
  addHistogram("truthEtaEle",20,-5.0,5.0);
  addHistogram("truthPhiEle",20,-5.0,5.0);

  //addHistogram("ptMu",60,-0.3,0.3);
  addHistogram("ptMu",50,0,200);
  addHistogram("etaMu",60,-0.05,0.05);
  //addHistogram("etaMu",20,-5.0,5.0);
  addHistogram("phiMu",60,-0.05,0.05);
  //addHistogram("phiMu",20,-5.0,5.0);
  addHistogram("truthPtMu",30,0,300);
  addHistogram("truthEtaMu",20,-5.0,5.0);
  addHistogram("truthPhiMu",20,-5.0,5.0);

  addHistogram("p1x",50,-200,200);
  addHistogram("p1y",50,-200,200);
  addHistogram("p2x",50,-200,200);
  addHistogram("p2y",50,-200,200);
  addHistogram("pmissx",50,-200,200);
  addHistogram("pmissy",50,-200,200);
  addHistogram("den_r",50,-200,200);
  addHistogram("num_r_1",50,-200,200);
  addHistogram("num_r_2",50,-200,200);
  addHistogram("den_rL",50,-20000,20000);
  addHistogram("num_r_1L",50,-20000,20000);
  addHistogram("num_r_2L",50,-20000,20000);

  addHistogram("m_ll",50,0.,200.);
  addHistogram("mcoll_tt",60,0.,300.);
  addHistogram("mcoll_ttF",60,0.,300.);

  addHistogram("ptTau",30,0,300);
  addHistogram("etaTau",20,-5.0,5.0);
  addHistogram("phiTau",20,-5.0,5.0);
  addHistogram("truthPtTau",30,0,300);
  addHistogram("truthEtaTau",20,-5.0,5.0);
  addHistogram("truthPhiTau",20,-5.0,5.0);

  addHistogram("ptJet_Hard",50,-1.,1.);
  //addHistogram("ptJet_PileUp",60,0.,180.);
  addHistogram("ptJet",50,0,200);
  addHistogram("ptJet1TeV",100,0,1000);

  addHistogram("ptLeadingJet",100,0,500);
  addHistogram("ptSubLeadingJet",100,0,500);
  addHistogram("etaLeadingJet",50,-5.,5.);
  addHistogram("etaLeadingJetF",50,-5.,5.);
  addHistogram("etaSubLeadingJet",50,-5.,5.);
  addHistogram("etaSubLeadingJetF",50,-5.,5.);
  addHistogram("phiLeadingJet",50,-5.,5.);
  addHistogram("phiSubLeadingJet",50,-5.,5.);
  addHistogram("ptResLeadingJet",100,-1.,1.);
  addHistogram("ptResSubLeadingJet",100,-1.,1.);
  addHistogram("etaResLeadingJet",50,-0.5,0.5);
  addHistogram("etaResSubLeadingJet",50,-0.5,0.5);
  addHistogram("phiResLeadingJet",50,-0.5,0.5);
  addHistogram("phiResSubLeadingJet",50,-0.5,0.5);


  addHistogram("etaJet",50,-0.5,0.5);
  addHistogram("etaJet1IDR",50,-5.0,5.0);
  addHistogram("etaJet2IDR",50,-5.0,5.0);
  addHistogram("etaJet1IDRPU",50,-5.0,5.0);
  addHistogram("etaJet2IDRPU",50,-5.0,5.0);
  addHistogram("etaJet1IDRHS",50,-5.0,5.0);
  addHistogram("etaJet2IDRHS",50,-5.0,5.0);

  //addHistogram("etaJet",20,-5.0,5.0);
  addHistogram("phiJet",50,-0.5,0.5);
  //addHistogram("phiJet",20,-5.0,5.0);
  addHistogram("truthPtJet",30,0,300);
  addHistogram("truthEtaJet",20,-5.0,5.0);
  addHistogram("truthPhiJet",20,-5.0,5.0);
  addHistogram("Anom_eventsEtaJets",50,-5.,5.,50,-5.,5.);
  addHistogram("Anom_eventsPhiJets",50,-5.,5.,50,-5.,5.);

  addHistogram("DiffRapi",60,0.,15.);

  addHistogram("GenMET",40,0,2000);
  addHistogram("GenHT",40,0,2000);

  addHistogram("mc_weight", 1, 0, 1);
}

static int jetFlavor(AnalysisObject &jet) {
  if (jet.pass(TrueBJet)) return 5;
  if (jet.pass(TrueCJet)) return 4;
  if (jet.pass(TrueTau)) return 15;
  return 0;
}

void VBFHtautau::ProcessEvent(AnalysisEvent *event)
{

  float gen_met      = event->getGenMET();
  float gen_ht       = event->getGenHT();
  int channel_number = event->getMCNumber();

  fill("mc_weight", event->getMCWeights()[0]);

  //get electrons
  auto electrons  = event->getElectrons(20, 3.8);
  //get muons
  auto muons      = event->getMuons(20, 3.8);
  //get taus
  auto taus       = event->getTaus(20., 3.8);
  //get jets
  auto candJets   = event->getJets(30., 3.8);
  auto metVec     = event->getMET();
  double met      = metVec.Et();
  auto sumET     = event->getSumET();

  // Overlap removal
  auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muons) { return std::min(0.4, 0.04 + 10./muons.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muons, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10./muons.Pt()); };
  auto radiusCalcElec = [] (const AnalysisObject& electrons, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10./electrons.Pt()); };

  //std::cout << "radiusCalcJet = " << radiusCalcJet << std::endl;
  //std::cout << "radiusCalcMuon = " << radiusCalcMuon << std::endl;
  //std::cout << "radiusCalcElec = " << radiusCalcElec << std::endl;

  electrons  = overlapRemoval(electrons, muons, 0.01);
  candJets   = overlapRemoval(candJets, electrons, 0.2);
  electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);

  auto candBjets     = filterObjects(candJets, 30, 3.8, GoodBJet);  //70 working point
  muons      = overlapRemoval(muons, candBjets, 0.2);

  candJets   = overlapRemoval(candJets, muons, radiusCalcJet);
  muons      = overlapRemoval(muons, candJets, radiusCalcMuon);

  taus      = overlapRemoval(taus, electrons, 0.1);

  auto leptons   = electrons + muons;
 
  int n_jets        = candJets.size();
  int n_leptons     = leptons.size();

  // inclusive - all jets + leptons
  float meffi  = met + sumObjectsPt(candJets) + sumObjectsPt(leptons);
  // dphimin between leading 4 signal jets and met
  float dphiMin4  = minDphi(metVec, candJets, 4);
  // leading lepton and met
  float mT = (leptons.size()>0)? calcMT(leptons[0], metVec) : 0.0;
  // dPhi(j1, MET) for Gbb
  float dphi1jet = minDphi(metVec, candJets, 1);


  TLorentzVector xNull(0.,0.,0.,0.);

  // Reco Electrons 
  auto recoElectron= (electrons.size()>0)? electrons[0]: xNull;
  // Reco Muons 
  auto recoMuon= (muons.size()>0)? muons[0]: xNull;
  // Reco Taus 
  auto recoTau= (taus.size()>0)? taus[0]: xNull;
  // Reco Jets 
  AnalysisObject recoJet= (candJets.size()>0)? candJets[0]: AnalysisObject(0.,0.,0.,0.,0,0,JET,0);

  // Truth met
  auto truthMet= event->getTruthParticle(metVec);
  // Truth Electrons 
  auto truthElectron= (electrons.size()>0)? event->getTruthParticle(electrons[0]): xNull;
  // Truth Muons 
  auto truthMuon= (muons.size()>0)? event->getTruthParticle(muons[0]): xNull;
  // Truth Taus 
  auto truthTau= (taus.size()>0)? event->getTruthParticle(taus[0]): xNull;
  // Truth Jets 
  AnalysisObject truthJet= (candJets.size()>0)? event->getTruthParticle(candJets[0]): AnalysisObject(0.,0.,0.,0.,0,0,JET,0);


  fill("truthMet", truthMet.Pt());

  fill("sumET", sumET);

  fill("n_jets", n_jets);
  fill("n_electrons", electrons.size());
  //fill("n_muons", muons.size());
  fill("n_taus", taus.size());
  fill("n_leptons", n_leptons);

  if (electrons.size()>0) {
    fill("ptEle", recoElectron.Pt());
    fill("etaEle", (recoElectron.Eta()-truthElectron.Eta())/truthElectron.Eta());
    fill("phiEle", (recoElectron.Phi()-truthElectron.Phi())/truthElectron.Phi());
    fill("truthPtEle", truthElectron.Pt());
    fill("truthEtaEle", truthElectron.Eta());
    fill("truthPhiEle", truthElectron.Phi());
  }

  if (muons.size()>0) {
    fill("ptMu", recoMuon.Pt());
    fill("etaMu", (recoMuon.Eta()-truthMuon.Eta())/truthMuon.Eta());
    fill("phiMu", (recoMuon.Phi()-truthMuon.Phi())/truthMuon.Phi());
    fill("truthPtMu", truthMuon.Pt());
    fill("truthEtaMu", truthMuon.Eta());
    fill("truthPhiMu", truthMuon.Phi());

  }

  fill("n_muons",muons.size());

  if (!(taus.size()==0&&muons.size()==2 && muons[0].charge()*muons[1].charge() < 0.)) return;

  double invMass = (muons[0]+muons[1]).M();
 
  fill("m_ll",invMass);

  if (!(invMass > 30. && invMass < 75.)) return;

  double pmissx = metVec.Px();
  double pmissy = metVec.Py();
  double x1, x2;
  double collmass;
  bool resultMass=false;
  resultMass=MassCollinearCore(muons[0], muons[1], //particles
                              pmissx, pmissy, //met
                             collmass, x1, x2);
  if (resultMass){
    fill("x1", x1);
    fill("x2", x2);
  }
 
  if (!( x1 > 0.1  &&  x1 < 1.0 )) return;
  if (!( x2 > 0.1  &&  x2 < 1.0 )) return;
 
  if (resultMass){
    fill("mcoll_tt",collmass);
  }
 
  if (!(resultMass && collmass > 91.2 -25.)) return;

  double deltaEtalep = muons[0].Eta() - muons[1].Eta();
  double deltaPhilep = fabs(muons[0].Phi() - muons[1].Phi());
  if (deltaPhilep>3.141592654) deltaPhilep = 2.*3.141592654 - deltaPhilep;

  double deltaRlep = sqrt(deltaEtalep*deltaEtalep+deltaPhilep*deltaPhilep);

  fill("deltaRlepvsdeltaEtalep",deltaRlep,deltaEtalep);
  fill("deltaRlep",deltaRlep);
  fill("deltaEtalep",deltaEtalep);

  if (deltaRlep >= 2.5 || fabs(deltaEtalep) >= 1.5 ) return;
 
  fill("n_jetsCheck",candJets.size());

  if (candJets.size() > 0) {
    fill("ptLeadingJet",candJets[0].Pt());
    fill("etaLeadingJet",candJets[0].Eta());
    fill("phiLeadingJet",candJets[0].Phi());
    truthJet= (candJets.size()>0)? event->getTruthParticle(candJets[0]): AnalysisObject(0.,0.,0.,0.,0,0,JET,0);
    fill("ptResLeadingJet",(candJets[0].Pt()-truthJet.Pt())/truthJet.Pt());
    fill("etaResLeadingJet",(candJets[0].Eta()-truthJet.Eta())/truthJet.Eta());
    fill("phiResLeadingJet",(candJets[0].Phi()-truthJet.Phi())/truthJet.Phi());
  }
  if (candJets.size() > 1) {
    fill("ptSubLeadingJet",candJets[1].Pt());
    fill("etaSubLeadingJet",candJets[1].Eta());
    fill("phiSubLeadingJet",candJets[1].Phi());
    truthJet= (candJets.size()>1)? event->getTruthParticle(candJets[1]): AnalysisObject(0.,0.,0.,0.,0,0,JET,0);
    fill("ptResSubLeadingJet",(candJets[1].Pt()-truthJet.Pt())/truthJet.Pt());
    fill("etaResSubLeadingJet",(candJets[1].Eta()-truthJet.Eta())/truthJet.Eta());
    fill("phiResSubLeadingJet",(candJets[1].Phi()-truthJet.Phi())/truthJet.Phi());
  }

  if (candJets.size() < 2 ) return;

  fill("ptJet", recoJet.Pt()); 

  if (!( candJets[0].Pt()>40. || candJets[1].Pt()>40. ) ) return;
  
  fill("DiffRapi", fabs(candJets[0].Eta()-candJets[1].Eta()));

  if (fabs(candJets[0].Eta()-candJets[1].Eta()) <= 3. || candJets[0].Eta()*candJets[1].Eta() > 0.) return;

  if (candJets.size() > 0) {
    fill("etaLeadingJetF",candJets[0].Eta());
  }
  if (candJets.size() > 1) {
    fill("etaSubLeadingJetF",candJets[1].Eta());
  }


  //if (fabs(candJets[0].Eta())<2.4 || fabs(candJets[1].Eta())<2.4) return;

  fill("invMassJets", (candJets[0]+candJets[1]).M());

  if ((candJets[0]+candJets[1]).M()<= 500. ) return;

  fill("met", met);
  fill("met1TeV", met);
  
  //if (met <= 55) return;

  if (resultMass){
    fill("x1F", x1);
    fill("x2F", x2);
    fill("mcoll_ttF",collmass);
  }

  fill("n_bjets",candBjets.size());

  if (candBjets.size()!=0) return;

  double MET_HPTOx= muons[0].Px() + muons[1].Px(); 
  double MET_HPTOy= muons[0].Py() + muons[1].Py(); 
  for (int i=0; i< candJets.size();i++){
    MET_HPTOx = MET_HPTOx+ candJets[i].Px();
    MET_HPTOy = MET_HPTOy+ candJets[i].Py();
  }  
  double MET_HPTO = sqrt(MET_HPTOx*MET_HPTOx+MET_HPTOy*MET_HPTOy);

  fill("metvsmet_HPTO",met,MET_HPTO);
  fill("met1TeV_HPTO", MET_HPTO);
  if (MET_HPTO <= 55) return;

  fill("meffi",meffi);

  fill("invMassIDR",(candJets[0]+candJets[1]).M());
  fill("etaJet1IDR",candJets[0].Eta());
  fill("etaJet2IDR",candJets[1].Eta());
  if (candJets[0].index()!=-1 && candJets[1].index()!=-1) {

    fill("invMassIDRHS",(candJets[0]+candJets[1]).M());
    fill("etaJet1IDRHS",candJets[0].Eta());
    fill("etaJet2IDRHS",candJets[1].Eta());
 
  } else {

    fill("invMassIDRPU",(candJets[0]+candJets[1]).M());
    fill("etaJet1IDRPU",candJets[0].Eta());
    fill("etaJet2IDRPU",candJets[1].Eta());
 
  }

  if (candJets[0].index()!=-1 && candJets[1].index()!=-1) {
    fill("multEvents",1.);
  } else if ((candJets[0].index()==-1 && candJets[1].index()!=-1) ||
             (candJets[0].index()!=-1 && candJets[1].index()==-1)){
    fill("multEvents",2.);
  } else {
    fill("multEvents",3.);
  }

  return;
}
bool VBFHtautau::MassCollinearCore(const TLorentzVector &k1, const TLorentzVector &k2, //particles
                               const double metetx, const double metety, //met
                               double &mass, double &xp1, double &xp2){ //result
  TMatrixD K(2, 2);
  K(0, 0) = k1.Px();      K(0, 1) = k2.Px();
  K(1, 0) = k1.Py();      K(1, 1) = k2.Py();

  if(K.Determinant()==0)
    return false;

  TMatrixD M(2, 1);
  M(0, 0) = metetx;
  M(1, 0) = metety;

  TMatrixD Kinv = K.Invert();

  TMatrixD X(2, 1);
  X = Kinv*M;

  double X1 = X(0, 0);    double X2 = X(1, 0);
  double x1 = 1./(1.+X1); double x2 = 1./(1.+X2);

  TLorentzVector p1 = k1*(1/x1);
  TLorentzVector p2 = k2*(1/x2);

  double m = (p1+p2).M();

  //return to caller
   mass = m;
 
   if(k1.Pt() > k2.Pt()){
      xp1 = x1; xp2 = x2;
   }else{
      xp1 = x2; xp2 = x1;
   }

return true;
}
