#include "SimpleAnalysis/AnalysisClass.h"
#include <RootCore/Packages.h>

#ifdef ROOTCORE_PACKAGE_BTaggingTruthTagging
#include "BTaggingTruthTagging/BTaggingTruthTaggingTool.h"
static  BTaggingTruthTaggingTool *m_btt;
#endif

DefineAnalysis(VBFHtautauhh)


void VBFHtautauhh::Init()
{
  addRegions({
              "SR_Gbb_A",
              "SR_Gbb_B",
              "SR_Gbb_C",
              "SR_Gbb_D",
              "SR_Gtt_0l_A",
              "SR_Gtt_0l_B",
              "SR_Gtt_0l_C",
              "SR_Gtt_1l_A",
              "SR_Gtt_1l_B",
              "SR_Gtt_1l_C",
              "SR_0l_Hnj_Hmeff",
              "SR_0l_Hnj_Imeff",
              "SR_0l_Hnj_Lmeff",
              "SR_0l_IStR",
              "SR_0l_Inj_Imeff",
              "SR_0l_Inj_Lmeff",
              "SR_0l_Lnj_Hmeff",
              "SR_0l_Lnj_Imeff",
              "SR_0l_Lnj_Lmeff",
              "SR_1l_Hnj_Hmeff",
              "SR_1l_Hnj_Imeff",
              "SR_1l_Hnj_Lmeff",
              "SR_1l_Inj_Imeff",
              "SR_1l_Inj_Lmeff"
            });

  addRegions({
              "CR_Gbb_A",
              "CR_Gbb_B",
              "CR_Gbb_C",
              "CR_Gbb_D",
              "CR_Gtt_0l_A",
              "CR_Gtt_0l_B",
              "CR_Gtt_0l_C",
              "CR_Gtt_1l_A",
              "CR_Gtt_1l_B",
              "CR_Gtt_1l_C",
              "CR_Hnj_Hmeff",
              "CR_Hnj_Imeff",
              "CR_Hnj_Lmeff",
              "CR_IStR",
              "CR_Inj_Imeff",
              "CR_Inj_Lmeff",
              "CR_Lnj_Hmeff",
              "CR_Lnj_Imeff",
              "CR_Lnj_Lmeff"
            });

  addRegions({
              "VR1_Gtt_0l_A",
              "VR1_Gtt_0l_B",
              "VR1_Gtt_0l_C",
              "VR2_Gtt_0l_A",
              "VR2_Gtt_0l_B",
              "VR2_Gtt_0l_C",
              "VR1_Gtt_1l_A",
              "VR1_Gtt_1l_B",
              "VR1_Gtt_1l_C",
              "VR2_Gtt_1l_A",
              "VR2_Gtt_1l_B",
              "VR2_Gtt_1l_C",
              "VR1_Gbb_A",
              "VR1_Gbb_B",
              "VR1_Gbb_C",
              "VR1_Gbb_D",
              "VR2_Gbb_A",
              "VR2_Gbb_B",
              "VR2_Gbb_C",
              "VR2_Gbb_D",
              "VR0l_Lnj_Lmeff",
              "VR0l_Lnj_Imeff",
              "VR0l_Lnj_Hmeff",
              "VR0l_Inj_Lmeff",
              "VR0l_Inj_Imeff",
              "VR0l_Hnj_Lmeff",
              "VR0l_Hnj_Imeff",
              "VR0l_Hnj_Hmeff",
              "VR0l_IStR",
              "VR1l_Inj_Lmeff",
              "VR1l_Inj_Imeff",
              "VR1l_Hnj_Lmeff",
              "VR1l_Hnj_Imeff",
              "VR1l_Hnj_Hmeff"
            });

  //addHistogram("met",50,-2.,3.);
  addHistogram("met",60,0,200);
  addHistogram("truthMet",30,0,300);
  addHistogram("invMassJets",100,0,5000.);
  addHistogram("DeltaPhi", 50, 0, 5);
  addHistogram("DRtau", 50, 0, 5);

  addHistogram("sumET",30,0,300);
  addHistogram("meffi",20,0,2000);
  addHistogram("mTb_min",20,0,800);
  addHistogram("mT", 20, 0, 500);
  addHistogram("dphiMin4", 50, 0, 5);
  addHistogram("dphi1jet", 50, 0, 5);

  addHistogram("n_jetFls", 20, -0.5, 19.5);
  addHistogram("n_jets", 20, -0.5, 19.5);
  addHistogram("n_electrons", 20, -0.5, 19.5);
  addHistogram("n_muons", 20, -0.5, 19.5);
  addHistogram("n_taus", 20, -0.5, 19.5);
  addHistogram("n_leptonsFl", 20, -0.5, 19.5);
  addHistogram("n_leptons", 20, -0.5, 19.5);

  //addHistogram("ptEle",60,-0.3,0.3);
  addHistogram("ptEle",50,0,200.);
  addHistogram("etaEle",60,-0.05,0.05);
  //addHistogram("etaEle",20,-5.0,5.0);
  addHistogram("phiEle",60,-0.05,0.05);
  //addHistogram("phiEle",20,-5.0,5.0);
  addHistogram("truthPtEle",30,0,300);
  addHistogram("truthEtaEle",20,-5.0,5.0);
  addHistogram("truthPhiEle",20,-5.0,5.0);

  //addHistogram("ptMu",60,-0.3,0.3);
  addHistogram("ptMu",50,0,200);
  addHistogram("etaMu",60,-0.05,0.05);
  //addHistogram("etaMu",20,-5.0,5.0);
  addHistogram("phiMu",60,-0.05,0.05);
  //addHistogram("phiMu",20,-5.0,5.0);
  addHistogram("truthPtMu",30,0,300);
  addHistogram("truthEtaMu",20,-5.0,5.0);
  addHistogram("truthPhiMu",20,-5.0,5.0);

  addHistogram("m_ll",50,0.,200.);

  addHistogram("ptTau",30,0,300);
  addHistogram("etaTau",20,-5.0,5.0);
  addHistogram("phiTau",20,-5.0,5.0);
  addHistogram("truthPtTau",30,0,300);
  addHistogram("truthEtaTau",20,-5.0,5.0);
  addHistogram("truthPhiTau",20,-5.0,5.0);

  addHistogram("ptJet_Hard",50,-1.,1.);
  //addHistogram("ptJet_PileUp",60,0.,180.);
  addHistogram("ptJet",50,0,200);
  addHistogram("etaJet",50,-0.5,0.5);
  //addHistogram("etaJet",20,-5.0,5.0);
  addHistogram("phiJet",50,-0.5,0.5);
  //addHistogram("phiJet",20,-5.0,5.0);
  addHistogram("truthPtJet",30,0,300);
  addHistogram("truthEtaJet",20,-5.0,5.0);
  addHistogram("truthPhiJet",20,-5.0,5.0);
  addHistogram("Anom_eventsEtaJets",50,-5.,5.,50,-5.,5.);
  addHistogram("Anom_eventsPhiJets",50,-5.,5.,50,-5.,5.);

  addHistogram("DiffRapi",60,0.,15.);
  addHistogram("RapiTaus",60,0.,15.);

  addHistogram("GenMET",40,0,2000);
  addHistogram("GenHT",40,0,2000);

  addHistogram("mc_weight", 1, 0, 1);
}

static int jetFlavor(AnalysisObject &jet) {
  if (jet.pass(TrueBJet)) return 5;
  if (jet.pass(TrueCJet)) return 4;
  if (jet.pass(TrueTau)) return 15;
  return 0;
}

void VBFHtautauhh::ProcessEvent(AnalysisEvent *event)
{

  float gen_met      = event->getGenMET();
  float gen_ht       = event->getGenHT();
  int channel_number = event->getMCNumber();
  
  fill("mc_weight", event->getMCWeights()[0]);

  // handle HT slicing now
  if(channel_number==410000 && gen_ht>600) return;
  if(channel_number==410001 && gen_ht>600) return;
  if(channel_number==410002 && gen_ht>600) return;
  if(channel_number==410225 && gen_ht>600) return;
  if(channel_number==410004 && gen_ht>600) return;
  if(channel_number==407018 && gen_ht>500) return;
  if(channel_number==407020 && gen_ht>500) return;


  auto electrons  = event->getElectrons(20, 3.8);

  auto muons      = event->getMuons(20, 3.8);

  auto taus       = event->getTaus(20., 3.8);

  auto candJets   = event->getJets(30., 3.4);

  auto metVec     = event->getMET();
  double met      = metVec.Et();
  auto sumET     = event->getSumET();

  auto radiusCalcJet  = [] (const AnalysisObject& , const AnalysisObject& muon) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcMuon = [] (const AnalysisObject& muon, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/muon.Pt()); };
  auto radiusCalcElec = [] (const AnalysisObject& elec, const AnalysisObject& ) { return std::min(0.4, 0.04 + 10/elec.Pt()); };

  electrons  = overlapRemoval(electrons, muons, 0.01);
  candJets   = overlapRemoval(candJets, electrons, 0.2);
  electrons  = overlapRemoval(electrons, candJets, radiusCalcElec);

  candJets   = overlapRemoval(candJets, muons, radiusCalcJet);
  muons      = overlapRemoval(muons, candJets, radiusCalcMuon);
  taus      = overlapRemoval(taus, electrons, 0.1);

  auto signalJets      = filterObjects(candJets, 30.);

  auto signalElectronsNonIso = filterObjects(electrons, 20, 3.8);

  auto signalElectrons = filterIsolated(electrons, electrons, muons, taus, candJets);

  auto signalMuonsNonIso     = filterObjects(muons, 20, 3.8);
  auto signalMuons = filterIsolated(muons, electrons, muons, taus, candJets);

  auto signalLeptons   = signalElectrons + signalMuons;

  auto signalTaus      = filterObjects(taus, 20);

  auto candBJets = filterObjects(signalJets, 30., 3.4);
 
  int n_jets        = signalJets.size();
  int n_leptons     = signalLeptons.size();

  // inclusive - all jets + leptons
  float meffi  = met + sumObjectsPt(signalJets) + sumObjectsPt(signalLeptons);
  // dphimin between leading 4 signal jets and met
  float dphiMin4  = minDphi(metVec, signalJets, 4);
  // leading lepton and met
  float mT = (signalLeptons.size()>0)? calcMT(signalLeptons[0], metVec) : 0.0;
  // sum of leading 4 reclustered jet masses
  //float mjsum = sumObjectsM(fatJets, 4);
  // dPhi(j1, MET) for Gbb
  float dphi1jet = minDphi(metVec, signalJets, 1);


  TLorentzVector xNull(0.,0.,0.,0.);

  // Reco Electrons 
  auto recoElectron= (signalElectrons.size()>0)? signalElectrons[0]: xNull;
  // Reco Muons 
  auto recoMuon= (signalMuons.size()>0)? signalMuons[0]: xNull;
  // Reco Taus 
  auto recoTau= (signalTaus.size()>0)? signalTaus[0]: xNull;
  // Reco Jets 
  AnalysisObject recoJet= (signalJets.size()>0)? signalJets[0]: AnalysisObject(0.,0.,0.,0.,0,0,JET,0);
  //if (signalJets.size()>0) std::cout << "7 VBFHtautauhh recoJet bJets Pt / Eta / Phi = " << recoJet.Pt() << "/" << recoJet.Eta() << "/" << recoJet.Phi() << std::endl;

  // Truth met
  auto truthMet= event->getTruthParticle(metVec);
  // Truth Electrons 
  auto truthElectron= (signalElectrons.size()>0)? event->getTruthParticle(signalElectrons[0]): xNull;
  // Truth Muons 
  auto truthMuon= (signalMuons.size()>0)? event->getTruthParticle(signalMuons[0]): xNull;
  // Truth Taus 
  auto truthTau= (signalTaus.size()>0)? event->getTruthParticle(signalTaus[0]): xNull;
  // Truth Jets 
  AnalysisObject truthJet= (signalJets.size()>0)? event->getTruthParticle(signalJets[0]): AnalysisObject(0.,0.,0.,0.,0,0,JET,0);

  fill("truthMet", truthMet.Pt());

  fill("sumET", sumET);
  fill("dphiMin4", dphiMin4);
  fill("dphi1jet", dphi1jet);

  fill("n_electrons", signalElectrons.size());
  fill("n_muons", signalMuons.size());
  fill("n_taus", taus.size());
  fill("n_leptonsFl", n_leptons);

  if (signalElectrons.size()>0) {
    fill("ptEle", recoElectron.Pt());
    fill("etaEle", (recoElectron.Eta()-truthElectron.Eta())/truthElectron.Eta());
    fill("phiEle", (recoElectron.Phi()-truthElectron.Phi())/truthElectron.Phi());
    fill("truthPtEle", truthElectron.Pt());
    fill("truthEtaEle", truthElectron.Eta());
    fill("truthPhiEle", truthElectron.Phi());
  }

  if (signalMuons.size()>0) {
    fill("ptMu", recoMuon.Pt());
    fill("etaMu", (recoMuon.Eta()-truthMuon.Eta())/truthMuon.Eta());
    fill("phiMu", (recoMuon.Phi()-truthMuon.Phi())/truthMuon.Phi());
    fill("truthPtMu", truthMuon.Pt());
    fill("truthEtaMu", truthMuon.Eta());
    fill("truthPhiMu", truthMuon.Phi());

  }
  if (signalTaus.size()>0) {
    fill("ptTau", recoTau.Pt());
    fill("etaTau", recoTau.Eta());
    fill("phiTau", recoTau.Phi());
    fill("truthPtTau", truthTau.Pt());
    fill("truthEtaTau", truthTau.Eta());
    fill("truthPhiTau", truthTau.Phi());
   }

  //Selection
  if (!(signalTaus.size()==2 && signalTaus[0].charge()*signalTaus[1].charge() <0 )) return;
  
  fill("n_leptons",signalLeptons.size());

  if (!(n_leptons==0)) return;

  fill("met", met);

  if (!(met > 40.)) return;

  fill("DeltaPhi",minDphi(metVec,signalTaus,2));

  float dphi1 = metVec.DeltaPhi(signalTaus[0]); 
  float dphi2 = metVec.DeltaPhi(signalTaus[1]); 

  if (!(dphi1*dphi2 <0 ||  minDphi(metVec,signalTaus,2) < 0.785398163) ) return;

  fill("DRtau",minDR(signalTaus,2,20));

  if (!((0.8 < minDR(signalTaus,2,20)) && (minDR(signalTaus,2,20) < 2.4 ))) return;

  fill ("RapiTaus",fabs(signalTaus[0].Eta()-signalTaus[1].Eta()));

  if (!(fabs(signalTaus[0].Eta()-signalTaus[1].Eta()) < 1.5 )) return;
  
  fill("n_jets", signalJets.size());

  if (signalJets.size() < 2 ) return;

  //if (event->runSignal() && recoJet.index()==-1) return; //For signal only 
  fill("ptJet", recoJet.Pt()); 

  if (!( signalJets[0].Pt()>50. || signalJets[1].Pt()>50. ) ) return;

  fill("DiffRapi", fabs(signalJets[0].Eta()-signalJets[1].Eta()));

  if (fabs(signalJets[0].Eta()-signalJets[1].Eta()) < 4.4) return;

  fill("invMassJets", (signalJets[0]+signalJets[1]).M());

  if ((signalJets[0]+signalJets[1]).M()>700. ) fill("meffi",meffi);

  return;
}
