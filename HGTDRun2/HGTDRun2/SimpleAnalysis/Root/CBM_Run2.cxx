#include "SimpleAnalysis/CBM_Run2.h"

void CBM_Run2::Init(std::vector<std::string>& analysisOptions)
{

    useTrigger = false;
    useHPTOmet = false;
    useRun2kinCuts = false;
    int seed = 12345;
    tauEta = 2.5;
    jetEta = 4.5;
    for (const auto& option : analysisOptions){
        if (option == "useTrigger") useTrigger = true;
        if (option == "useHPTOmet") useHPTOmet = true;
        if (option == "useRun2kinCuts") useRun2kinCuts = true;
        if (option.find("seed=") == 0) {
			seed = stoi(option.substr(5));
		}
        if (option.find("tauEta=") == 0){
            tauEta = std::stod(option.substr(7));
        }
        if (option.find("jetEta=") == 0){
            jetEta = std::stod(option.substr(7));
        }
    }

    for(int i =0; i<18; i++) sumSquaredWeights[i]=0;//for histo errors

    m_random.SetSeed(seed);

    m_upgrade = new UpgradePerformanceFunctions();
	//m_upgrade->setLayout(layout);
	m_upgrade->setLayout(UpgradePerformanceFunctions::Step1p6);
	m_upgrade->setAvgMu(200);
	m_upgrade->setElectronWorkingPoint(UpgradePerformanceFunctions::looseElectron);
	// m_upgrade->setElectronRandomSeed(seed);
	m_upgrade->setMuonWorkingPoint(UpgradePerformanceFunctions::tightMuon);
	m_upgrade->setPhotonWorkingPoint(UpgradePerformanceFunctions::tightPhoton);
	// m_upgrade->setTauRandomSeed(seed);
	// m_upgrade->setJetRandomSeed(seed);
	// m_upgrade->setMETRandomSeed(seed);
	//m_upgrade->loadMETHistograms(METhistfile);
	m_upgrade->loadMETHistograms("UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root");//are these histos affecting something?
	// m_upgrade->setPileupRandomSeed(seed);
	m_upgrade->setPileupUseTrackConf(true);
	m_upgrade->setPileupJetPtThresholdMeV(30000.);
	m_upgrade->setPileupEfficiencyScheme(UpgradePerformanceFunctions::PU);
	m_upgrade->setPileupEff(0.02);
	m_upgrade->setPileupTemplatesPath("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/");
	m_upgrade->initPhotonFakeHistograms("UpgradePerformanceFunctions/PhotonFakes.root");
	m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");
	//list of the histograms of the output root file
	addHistogram("truthMet", 30, 0, 300);
    //    addHistogram("met", 30, 0, 300);
    addHistogram("sumET", 30, 0, 300);
    addHistogram("dphiMin4", 50, 0, 5);
    addHistogram("dphi1jet", 50, 0, 5);
    addHistogram("meffi",20,0,2000);

    addHistogram("ptTau", 30, 0, 300);
    addHistogram("etaTau", 20, -5.0, 5.0);
    addHistogram("etaJet", 20, -5.0, 5.0);
    addHistogram("phiTau", 20, -5.0, 5.0);
    addHistogram("truthPtTau", 30, 0, 300);
    addHistogram("truthEtaTau", 20, -5.0, 5.0);
    addHistogram("truthPhiTau", 20, -5.0, 5.0);

    addHistogram("pt_LowEnergeticJets_0", 50,0,200);
    addHistogram("pt_LowEnergeticJets_17", 50,0,200);
    addHistogram("met_HPTOx_0", 60,0,200);
    addHistogram("met_HPTOx_17", 60,0,200);
    addHistogram("met_HPTOy_0", 60,0,200);
    addHistogram("met_HPTOy_17", 60,0,200);

    addHistogram("DEtaTauEtaJet",30, -2, 6);
    addHistogram("tauEta1_jetEta1", 20, -5.0, 5, 20, -5.0, 5.0);

    addHistogram("weight_squared_sum", 20,0,30);
    addHistogram("TwoDJetpTvsEtaAll", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaHS", 18, 20., 200., 20, -5.0, 5.0);
    addHistogram("TwoDJetpTvsEtaPU", 18, 20., 200., 20, -5.0, 5.0);

    AddHistogramshh(21);
    
}

void CBM_Run2::ProcessEvent(AnalysisEvent *event)
{

  //    setEventWeight(1.0);
    //    Event_weight = getEventWeight();

    float ePt = 15.;
    float muPt = 10.;
    float tauPt = 20.;
    float jetPt = 30.;
    float eEta = 2.47;
    float muEta = 2.5;
    tauEta = 2.5;
    jetEta = 4.5;

    if (useRun2kinCuts){
        ePt = 15.;
        muPt = 10.;
        tauPt = 20.;
        jetPt = 30.;
        eEta = 2.47;
        muEta = 2.5;
        tauEta = 2.5;
        jetEta = 4.5;
    }

    auto electrons = event->getElectrons(ePt, eEta);
    auto muons = event->getMuons(muPt, muEta);

    auto taus = event->getTaus(tauPt, tauEta);

    auto candJets = event->getJets(jetPt, jetEta);

    auto radiusCalcJet = [](const AnalysisObject &, const AnalysisObject &muon) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcMuon = [](const AnalysisObject &muon, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / muon.Pt()); };
    auto radiusCalcElec = [](const AnalysisObject &elec, const AnalysisObject &) { return std::min(0.4, 0.04 + 10 / elec.Pt()); };

    electrons = overlapRemoval(electrons, muons, 0.01);
    candJets = overlapRemoval(candJets, electrons, 0.2);
    electrons = overlapRemoval(electrons, candJets, radiusCalcElec);

    candJets = overlapRemoval(candJets, muons, radiusCalcJet);
    muons = overlapRemoval(muons, candJets, radiusCalcMuon);
    taus = overlapRemoval(taus, electrons, 0.1);
    candJets = overlapRemoval(candJets, taus, 0.2);

    auto signalJets = filterObjects(candJets, jetPt);

    //auto signalElectronsNonIso = filterObjects(electrons, ePt, eEta);

    // auto signalElectrons = filterIsolated(electrons, electrons, muons, taus, candJets);
    auto signalElectrons = filterObjects(electrons, ePt, eEta);

    //auto signalMuonsNonIso = filterObjects(muons, muPt, muEta);
    // auto signalMuons = filterIsolated(muons, electrons, muons, taus, candJets);
    auto signalMuons = filterObjects(muons, muPt, muEta);

    auto signalLeptons = signalElectrons + signalMuons;

    auto signalTaus = filterObjects(taus, tauPt, tauEta);

    auto candBJets = filterObjects(signalJets, jetPt, jetEta);

    int n_jets = signalJets.size();

    auto metVec = event->getMET();
    double met = metVec.Et();//there is no istogram to cut on this variable!
    auto sumET = event->getSumET();

    // inclusive - all jets + leptons
    float meffi = met + sumObjectsPt(signalJets) + sumObjectsPt(signalLeptons);
    // dphimin between leading 4 signal jets and met
    float dphiMin4 = minDphi(metVec, signalJets, 4);
    // leading lepton and met
    //float mT = (signalLeptons.size() > 0) ? calcMT(signalLeptons[0], metVec) : 0.0;
    // sum of leading 4 reclustered jet masses
    //float mjsum = sumObjectsM(fatJets, 4);
    // dPhi(j1, MET) for Gbb
    float dphi1jet = minDphi(metVec, signalJets, 1);

    TLorentzVector xNull(0., 0., 0., 0.);//start clean from here!

    // Reco Electrons
    //auto recoElectron = (signalElectrons.size() > 0) ? signalElectrons[0] : xNull;
    // Reco Muons
    //auto recoMuon = (signalMuons.size() > 0) ? signalMuons[0] : xNull;
    // Reco Taus
    auto recoTau = (signalTaus.size() > 0) ? signalTaus[0] : xNull;//used later, leave?
    // Reco Jets
    AnalysisObject recoJet = (signalJets.size() > 0) ? signalJets[0] : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    // Truth met
    auto truthMet = event->getTruthParticle(metVec);
    // Truth Electrons
    //    auto truthElectron = (signalElectrons.size() > 0) ? event->getTruthParticle(signalElectrons[0]) : xNull;
    // Truth Muons
    //    auto truthMuon = (signalMuons.size() > 0) ? event->getTruthParticle(signalMuons[0]) : xNull;
    // Truth Taus
    auto truthTau = (signalTaus.size() > 0) ? event->getTruthParticle(signalTaus[0]) : xNull;
    // Truth Jets
    AnalysisObject truthJet = (signalJets.size() > 0) ? event->getTruthParticle(signalJets[0]) : AnalysisObject(0., 0., 0., 0., 0, 0, JET, 0);

    fill("truthMet", truthMet.Pt());
    fill("sumET", sumET);
    fill("dphiMin4", dphiMin4);
    fill("dphi1jet", dphi1jet);
    fill("meffi", meffi);

    if (signalTaus.size() > 0)
    {
        fill("ptTau", recoTau.Pt());
        fill("etaTau", recoTau.Eta());
        fill("phiTau", recoTau.Phi());
        fill("truthPtTau", truthTau.Pt());
        fill("truthEtaTau", truthTau.Eta());
        fill("truthPhiTau", truthTau.Phi());

        if (signalJets.size() > 0) {
            fill("DEtaTauEtaJet",fabs(signalTaus[0].Eta() - signalJets[0].Eta()));
            fill("tauEta1_jetEta1", signalTaus[0].Eta(), signalJets[0].Eta());
        }
    }

    if (signalJets.size() > 0) {
        fill("etaJet",signalJets[0].Eta());
    }


    //Selection
    bool passTriger = false;
    if (useTrigger) {
        std::vector<AnalysisObjects> tauPairs;
        std::vector<int> indexes;
        getPairs(signalTaus, indexes, tauPairs);

	//        bool passTriger = false;
        for (auto tauPair : tauPairs){
            float eff = TruthSmear::getDiTauTriggerEfficiency(tauPair[0].Pt()*1000, tauPair[1].Pt()*1000,
                                                             tauPair[0].Eta(), tauPair[1].Eta(),
                                                              1, 1, m_upgrade);
            if (m_random.Uniform(1.0) < eff) {
                passTriger = true;
                break;
            }
        }
	//        if (!passTriger) return;
    }

    int n_taus = signalTaus.size();
    int n_electrons = signalElectrons.size();
    int n_muons = signalMuons.size();
    int n_leptons = signalLeptons.size();
    double pt_Jet1 = NULL;
    double pt_Jet2 = NULL;
    double etaJet1 = NULL;
    double etaJet2 = NULL;
    float RapiJets = NULL;
    float invMassJets = NULL;
    bool resultMass = false;
    double x1, x2;
    double ptTau1 = NULL;
    double ptTau2 = NULL;
    double deltaEtatau = NULL;
    double deltaPhitau = NULL;
    double deltaRtau = NULL;
    float DRtau =  NULL;
    float RapiTaus = NULL;
    double pmissx = metVec.Px();
    double pmissy = metVec.Py();
    double collmass;//this is only defined and then directly fill in the histograms, does not change its value. leave?
    double mmc_mass = -999999.;//same as colmass, to erase them we have to modify FillHistogramshh function
    double MET_HPTO = NULL, MET_HPTOx = 0., MET_HPTOy = 0.;

    if (n_taus > 0) ptTau1 = signalTaus[0].Pt();
    if (n_taus > 1) {
        ptTau2 = signalTaus[1].Pt();
        DRtau = minDR(signalTaus, 2 ,20);
        RapiTaus = fabs(signalTaus[0].Eta() - signalTaus[1].Eta());
        // MET HPTO
        // MET_HPTOx= signalTaus[0].Px() + signalTaus[1].Px();
        // MET_HPTOy= signalTaus[0].Py() + signalTaus[1].Py();
        MET_HPTOx= -signalTaus[0].Px() - signalTaus[1].Px();
        MET_HPTOy= -signalTaus[0].Py() - signalTaus[1].Py();
        for (int i = 0 ; i < signalJets.size() ; i++){
            // MET_HPTOx += signalJets[i].Px();
            // MET_HPTOy += signalJets[i].Py();
            MET_HPTOx += -signalJets[i].Px();
            MET_HPTOy += -signalJets[i].Py();
        }
        MET_HPTO = sqrt(MET_HPTOx*MET_HPTOx + MET_HPTOy*MET_HPTOy);

        // COLLINEAR APPROXIMATION
        if (useHPTOmet){
            resultMass = MassCollinearCore(signalTaus[0], signalTaus[1], //particles
                                    MET_HPTOx, MET_HPTOy,
                                    collmass, x1, x2);
        } else {
            resultMass = MassCollinearCore(signalTaus[0], signalTaus[1], //particles
                                    pmissx, pmissy,                     
                                    collmass, x1, x2);
        }
    }
    if (signalJets.size() > 0)
    {
        pt_Jet1 = signalJets[0].Pt();
        etaJet1 = signalJets[0].Eta();
    }
    if (signalJets.size() > 1)
    {
        pt_Jet2 = signalJets[1].Pt();
        etaJet2 = signalJets[1].Eta();
        RapiJets = fabs(signalJets[0].Eta() - signalJets[1].Eta());
        invMassJets = (signalJets[0] + signalJets[1]).M();
    }

    for (int i = 2 ; i < signalJets.size() ; i++) {
        fill("pt_LowEnergeticJets_0", signalJets[i].Pt());
    }
    if (MET_HPTOx > 0){
        fill("met_HPTOx_0",MET_HPTOx);
        fill("met_HPTOy_0",MET_HPTOy);
    }

    if (taus.size() > 1){
      deltaEtatau = taus[0].Eta() - taus[1].Eta();
      deltaPhitau = fabs(taus[0].Phi() - taus[1].Phi());
      if (deltaPhitau > 3.141592654) deltaPhitau = 2*3.141592654 - deltaPhitau;
      deltaRtau = sqrt(deltaEtatau*deltaEtatau + deltaPhitau*deltaPhitau);
}

    //Condition skimming
    double tauLead = 0.;
    double tauSubL = 0.;
    for(int i=0; i<signalTaus.size(); i++){
      if(signalTaus[i].Pt() > 33.) tauLead++;
      if(signalTaus[i].Pt() > 23.) tauSubL++;
    }
    if(!( (tauLead>=1) && (tauSubL>=2) )) return;


    Event_weight = getEventWeight();

    //preselection cuts
    if (!passTriger) return;//applying trigger
    if (!(signalJets.size() > 0)) return;
    if (!(signalJets[0].Pt() > 70. && fabs(signalJets[0].Eta()) < 3.2)) return;
    if (!(( x1 > 0.1 &&  x1 < 1.4) && (x2 > 0.1 && x2 < 1.4 ))) return;
    if(!(n_muons==0 && n_electrons==0)) return;//electrons+muons-->check
    if (!(RapiTaus < 2.)) return;
    if (!(deltaRtau < 2.8)) return;

    //no cut - HISTO n0
    FillHistogramshh(0, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[0] += Event_weight * Event_weight;


    // trigger - histo n1 for ow ot used
    FillHistogramshh(1, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[1] += Event_weight * Event_weight;


    //met >20 - HISTO n2
    //NB the option on the met cut changes wrt the option in the submission line
    if (useHPTOmet){
      if (!(MET_HPTO > 20.)) return;//met cut with HPTO method
    } else {
      if (!(met > 20.)) return;//met cut
    }
    FillHistogramshh(2, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
     sumSquaredWeights[2] += Event_weight * Event_weight;

    //DR cut - HISTO n3
    if (!(n_taus >1)) return;
    if (!((deltaRtau > 0.8) && (deltaRtau < 2.5))) return;
    FillHistogramshh(3, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[3] += Event_weight * Event_weight;

    //pt leading tau >40 - HISTO n4 
    if (!(signalTaus[0].Pt() > 40.)) return;
    FillHistogramshh(4, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[4] += Event_weight * Event_weight;

    //pt subleading tau >30 - HISTO n5
    if (!(signalTaus[1].Pt() > 30.)) return;
    FillHistogramshh(5, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[5] += Event_weight * Event_weight;

    //x1 and x2 cut - HISTO n6
    if (!(( x1 > 0.1 &&  x1 < 1.4) && (x2 > 0.1 && x2 < 1.4 ))) return;//ok this goes after leading and subleading cut on tau pt
    FillHistogramshh(6, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[6] += Event_weight * Event_weight;

    //no leptons, n_muons=0 and n_electrns=0 - HISTO n7
    if(!(n_muons==0 && n_electrons==0)) return;//electrons+muons-->check
    FillHistogramshh(7, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[7] += Event_weight * Event_weight;

    //two taus particle, same charge in absolute value - HISTO n8 
    //    if (!(n_taus == 2)) return;
    if (!(fabs(signalTaus[0].charge()) == 1. && fabs(signalTaus[1].charge()) == 1.)) return;
    FillHistogramshh(8, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[8] += Event_weight * Event_weight;

    //leading and subleading tau with opposite charge - HISTO n9
    if (!(signalTaus[0].charge() * signalTaus[1].charge() < 0.)) return;
    FillHistogramshh(9, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[9] += Event_weight * Event_weight;

    //(tau_0_n_charged_tracks==1tau_0_n_charged_tracks==3)&&(tau_1_n_charged_tracks==1tau_1_n_charged_tracks==3) - HISTO n10 NOT USED NOW
    //if () return;
    FillHistogramshh(10, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[10] += Event_weight * Event_weight;

    //ditau_mmc_mlm_fit_status==1 - HISTO n11 NT USED NOW
    //if () return;
    FillHistogramshh(11, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[11] += Event_weight * Event_weight;

    // CBA VBF inclusive
    //pt leading jet >70 and eta<3.2 - HISTO n12
    // if (!(signalJets[0].Pt() > 70.)) return;
    // if (!(fabs(signalJets[0].Eta()) < 3.2)) return;
    if (!(signalJets.size() > 0)) return;
    if (!(signalJets[0].Pt() > 70. && fabs(signalJets[0].Eta()) < 3.2)) return;
    FillHistogramshh(12, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[12] += Event_weight * Event_weight;

    //tau rapidity cut - HISTO n13
    if (!(RapiTaus < 1.5)) return;
    FillHistogramshh(13, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[13] += Event_weight * Event_weight;

    //requiring a subleading jet, if not program CRASH
    if (!(signalJets.size() > 1)) return;
    //leadingJet pT>70 && subleadingJet pT>30 && RapiJets>3. && invmass>400. - HISTO n14
    //Subleading jet cut
    if (!(signalJets[0].Pt() > 70. && signalJets[1].Pt() > 30. && RapiJets > 3. && invMassJets > 400.)) return;
    FillHistogramshh(14, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    //    sumSquaredWeights[14] += Event_weight * Event_weight;

    //(leading jet eta < leading tau eta && leading tau eta < subleading jet eta) OR (subleading jet eta < leading tau eta && leading tau eta <leading jet eta)
    //leading tau topology - HISTO n15 leading tau central, tau produced in the center wrt jets
    if (!((signalJets[0].Eta() < signalTaus[0].Eta() && signalTaus[0].Eta() < signalJets[1].Eta()) || 
	  (signalJets[1].Eta() < signalTaus[0].Eta() && signalTaus[0].Eta() < signalJets[0].Eta()))) return;//eta leading tau - eta leading jet
    FillHistogramshh(15, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[15] += Event_weight * Event_weight;

    //subeading tau topology - HISTO n16
    if (!((signalJets[0].Eta() < signalTaus[1].Eta() && signalTaus[1].Eta() < signalJets[1].Eta()) || 
	  (signalJets[1].Eta() < signalTaus[1].Eta() && signalTaus[1].Eta() < signalJets[0].Eta()))) return;//eta leading jet vs eta leading tau
    FillHistogramshh(16, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[16] += Event_weight * Event_weight;

    //jets in two oposite emisphere opposite eta between eading and subleading jets - HISTO n17
    if (!(signalJets[0].Eta() * signalJets[1].Eta() < 0)) return;
    FillHistogramshh(17, n_taus,ptTau1,ptTau2,n_leptons,met,MET_HPTO,deltaRtau,deltaPhitau,deltaEtatau,RapiTaus,n_jets,pt_Jet1,pt_Jet2,RapiJets,invMassJets,resultMass,x1,x2,etaJet1,etaJet2,collmass,mmc_mass);
    sumSquaredWeights[17] += Event_weight * Event_weight;

    for (int i = 0 ; i < signalJets.size() ; i++){
      fill("TwoDJetpTvsEtaAll",signalJets[i].Pt(),signalJets[i].Eta());
      if (signalJets[i].index()!=-1) fill("TwoDJetpTvsEtaHS",signalJets[i].Pt(),signalJets[i].Eta());
      if (signalJets[i].index()==-1) fill("TwoDJetpTvsEtaPU",signalJets[i].Pt(),signalJets[i].Eta());
    }

    for (int i = 2 ; i < signalJets.size() ; i++) {
        fill("pt_LowEnergeticJets_17", signalJets[i].Pt());
    }
    if (MET_HPTOx > 0){
        fill("met_HPTOx_17",MET_HPTOx);
        fill("met_HPTOy_17",MET_HPTOy);
    }

    return;
}

void CBM_Run2::Final() {
  for(int i=0; i<18; i++){
    setBinContent("weight_squared_sum", i, sumSquaredWeights[i]);
  }
  }
