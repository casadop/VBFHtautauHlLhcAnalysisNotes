#include "SimpleAnalysis/TruthEvent.h"
#include "SimpleAnalysis/TruthSmear.h"
#include "SimpleAnalysis/AnalysisClass.h"
#include <RootCore/Packages.h>

// #ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
// #include <UpgradePerformanceFunctions/UpgradePerformanceFunctions.h>
// #endif


TruthSmear::TruthSmear(std::vector<std::string>&
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
	options
#endif
) :
	smearElectrons(true), smearMuons(true), smearTaus(true), smearPhotons(true), smearJets(true), smearMET(true), addPileupJets(true), useHGTD0(false), useHGTD1(false) {
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions

	std::string mu = "None";
	std::string slayout = "gold";
	smearType = "CBM";
	smearJetPt = true;
	bool useTopoEM = true;
	int seed = 12345;
	UpgradePerformanceFunctions::UpgradeLayout layout = UpgradePerformanceFunctions::UpgradeLayout::run2;
	for (const auto& option : options) {
		if (option == "help") {
			std::cout << "Options for smearing:" << std::endl;
			std::cout << " noElectrons   - do not smear electrons" << std::endl;
			std::cout << " noMuons       - do not smear muons" << std::endl;
			std::cout << " noTaus        - do not smear taus" << std::endl;
			std::cout << " noPhotons     - do not smear photons" << std::endl;
			std::cout << " noJets        - do not smear jets" << std::endl;
			std::cout << " noMET         - do not smear MET" << std::endl;
			std::cout << " addPileupJets - add pileup jets" << std::endl;
			std::cout << " useHGTD0      - use HGTD v0 configuration" << std::endl;
			std::cout << " useHGTD1      - use HGTD v1 configuration" << std::endl;
			std::cout << " mu=<value>    - choice pile-up level (required)" << std::endl;
			std::cout << "                 Only mu=200 is allowed for now" << std::endl;
			std::cout << " seed=<value>  - set seed value (default: " << seed << std::endl;
			std::cout << " layout=<value>  - set layout (default: " << slayout << ")" << std::endl;
		}
		if (option == "noElectrons") smearElectrons = false;
		if (option == "noMuons")     smearMuons = false;
		if (option == "noTaus")      smearTaus = false;
		if (option == "noPhotons")   smearPhotons = false;
		if (option == "noJets")      smearJets = false;
		if (option == "noMET")       smearMET = false;
		if (option == "addPileupJets") addPileupJets = true;
		if (option == "useHGTD0") useHGTD0 = true;
		if (option == "useHGTD1") useHGTD1 = true;
		if (option == "useTopoEM") useTopoEM = true;
		if (option == "noSmearJetPt") smearJetPt = false;
		if (option.find("mu=") == 0) {
			mu = option.substr(3);
		}
		if (option.find("seed=") == 0) {
			seed = stoi(option.substr(5));
		}
		if (option.find("layout=") == 0) {
			slayout = option.substr(7);
			if (slayout == "run2") {
				layout = UpgradePerformanceFunctions::UpgradeLayout::run2;
				mu = "25"; //dummy value for now
			}
			else if (slayout == "gold") {
				layout = UpgradePerformanceFunctions::UpgradeLayout::Step1p6;
				mu = "200";
			}
			//add full list here?
			//else if...
		}
		if (option.find("weight") == 0) {
			smearType = "WEIGHT";
		}
	}
	std::cout << "Smearing with mu=" << mu << " and seed=" << seed << std::endl;

	if (mu != "200" && slayout != "run2") throw std::runtime_error("Unsupported pile-up level. Only mu=200 currently supported");

	std::string METhistfile = "UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root";
	if (slayout == "run2")
		METhistfile = "UpgradePerformanceFunctions/met_resol_tst_run2.root";

	m_upgrade = new UpgradePerformanceFunctions();
	//m_upgrade->setLayout(layout);
	m_upgrade->setLayout(UpgradePerformanceFunctions::Step1p6);
	m_upgrade->setAvgMu(stoi(mu));
	m_upgrade->setElectronWorkingPoint(UpgradePerformanceFunctions::looseElectron);
	m_upgrade->setElectronRandomSeed(seed);
	//m_upgrade->setPhotonRandomSeed(seed);
	m_upgrade->setMuonWorkingPoint(UpgradePerformanceFunctions::tightMuon);
	m_upgrade->setPhotonWorkingPoint(UpgradePerformanceFunctions::tightPhoton);
	m_upgrade->setTauRandomSeed(seed);
	m_upgrade->setJetRandomSeed(seed);
	if (useTopoEM){
		m_upgrade->setJetAlgorithm(UpgradePerformanceFunctions::TopoEM);
	} else {
		m_upgrade->setJetAlgorithm(UpgradePerformanceFunctions::PFlow);
	}
	m_upgrade->setMETRandomSeed(seed);
	//m_upgrade->loadMETHistograms(METhistfile);
	m_upgrade->loadMETHistograms("UpgradePerformanceFunctions/sumetPU_mu200_ttbar_gold.root");
	m_upgrade->setPileupRandomSeed(seed);
	m_upgrade->setPileupUseTrackConf(true);
	m_upgrade->setPileupJetPtThresholdMeV(30000.);
	m_upgrade->setPileupEfficiencyScheme(UpgradePerformanceFunctions::PU);
	m_upgrade->setPileupEff(0.02);
	m_upgrade->setPileupTemplatesPath("/cvmfs/atlas.cern.ch/repo/sw/database/GroupData/UpgradePerformanceFunctions/");
	m_upgrade->initPhotonFakeHistograms("UpgradePerformanceFunctions/PhotonFakes.root");
	if (slayout == "run2")
		m_upgrade->setFlavourTaggingCalibrationFilename("xAODBTaggingEfficiency/13TeV/2016-20_7-13TeV-MC15-CDI-2017-01-31_v1.root");
	else
		m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");
	m_upgrade->setFlavourTaggingCalibrationFilename("UpgradePerformanceFunctions/flavor_tags_v1.5.root");
	if (useHGTD0) m_upgrade->setUseHGTD0(false);
	if (useHGTD1) m_upgrade->setUseHGTD1(false);
	m_random.SetSeed(seed);
	std::cout << "--------------------" << std::endl;
	std::cout << "SMEARED FOR " << smearType << std::endl;
	std::cout << "UseTopoEM " << useTopoEM << std::endl;
	std::cout << "--------------------" << std::endl;

#else
	throw std::runtime_error("Compiled without smearing support - add UpgradePerformanceFunctions");
#endif
}

TruthEvent *TruthSmear::smearEvent(AnalysisEvent *
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
	event
#endif
) {
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
	if (smearType.find("WEIGHT") == 0){
		return weighedSmearing(
			#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
				event
			#endif
			);
	}
	auto electrons = event->getElectrons(1., 4.2); // Filter on pT, eta and "ID"
	auto muons = event->getMuons(1., 4.2);
	auto taus = event->getTaus(20., 2.5);
	auto photons = event->getPhotons(5., 4.2);
	auto jets = event->getJets(10., 5.2);
	auto fatjets = event->getFatJets(10., 5.2);
	auto met = event->getMET();
	auto sumet = event->getSumET();

	double met_x = met.Px();
	double met_y = met.Py();


	TruthEvent* smeared = new TruthEvent(sumet, met_x, met_y);
	smeared->setChannelInfo(event->getMCNumber(), event->getSUSYChannel());
	smeared->setGenMET(event->getGenMET());
	smeared->setGenHT(event->getGenHT());
	smeared->setMCWeights(event->getMCWeights());
	smeared->setTruth(event);
  
	for (const auto& electron : electrons) {
		if (smearElectrons) {
			float eff = m_upgrade->getElectronEfficiency(electron.Pt()*1000., electron.Eta());
			if (m_random.Uniform(1.0) < eff) {
				float electron_e = m_upgrade->getElectronSmearedEnergy(electron.E()*1000., electron.Eta()) / 1000.;
				TLorentzVector eLV;
				eLV.SetPtEtaPhiM(electron.Pt()*electron_e / electron.E(), electron.Eta(), electron.Phi(), 0.000510998910);
				int echarge = electron.charge();
				if (fabs(electron.Eta()) < 2.47) //FIXME: should have for full eta range
					if (m_random.Uniform(1.0) < m_upgrade->getElectronChargeFlipProb(electron.Pt()*1000., electron.Eta())) echarge *= -1;
				smeared->addElectron(eLV, echarge, electron.id(), electron.index());
				if (m_random.Uniform(1.0) < m_upgrade->getElectronToPhotonFakeRate(electron.Pt()*1000., electron.Eta()))
					smeared->addPhoton(eLV, PhotonIsoGood, -2);
			}
		}
		else {
			smeared->addElectron(electron, electron.charge(), electron.id(), electron.index());
		}
	}
	for (const auto& muon : muons) {
		if (smearMuons) {
			float eff = m_upgrade->getMuonEfficiency(muon.Pt()*1000., muon.Eta());
			if (m_random.Uniform(1.0) < eff) {
				float muonUnsmearedPt = muon.Pt()*1000.;
				float qoverpt = muon.charge() / muonUnsmearedPt;
				float muonQOverPtResolution = m_upgrade->getMuonQOverPtResolution(muonUnsmearedPt, muon.Eta());
				qoverpt += m_random.Gaus(0., muonQOverPtResolution);
				float muonSmearedPt = fabs(1. / qoverpt) / 1000.;
				int muonCharge = 1;
				if (qoverpt < 0) muonCharge = -1;
				TLorentzVector mLV;
				mLV.SetPtEtaPhiM(muonSmearedPt, muon.Eta(), muon.Phi(), 0.1056583715);
				smeared->addMuon(mLV, muonCharge, muon.id(), muon.index());
			}
		}
		else {
			smeared->addMuon(muon, muon.charge(), muon.id(), muon.index());
		}
	}
	for (const auto& tau : taus) {
		if (smearTaus) {
			short prong = 1;
			short wp = 2;  //tight tau
			if (tau.pass(TauThreeProng)) prong = 3;
			float eff = m_upgrade->getTauEfficiency(tau.Pt()*1000., tau.Eta(), prong, wp);
			if (m_random.Uniform(1.0) < eff) {
				float tau_E = m_upgrade->getTauSmearedEnergy(tau.E()*1000., tau.Eta(), prong) / 1000.;
				TLorentzVector tauLV;
				tauLV.SetPtEtaPhiM(tau.Pt()*tau_E / tau.E(), tau.Eta(), tau.Phi(), 1.777682);
				smeared->addTau(tauLV, tau.charge(), tau.id(), tau.index());
			}
		}
		else {
			smeared->addTau(tau, tau.charge(), tau.id(), tau.index());
		}
	}
	for (const auto& photon : photons) {
		if (smearPhotons) {
			float eff = m_upgrade->getPhotonEfficiency(photon.Pt()*1000./*, photon.Eta()*/);
			if (m_random.Uniform(1.0) < eff) {
				TLorentzVector pLV;
				pLV.SetPtEtaPhiM(photon.Pt()*1000., photon.Eta(), photon.Phi(), 0.);
				pLV = m_upgrade->getPhotonSmearedVector(&pLV);
				pLV.SetPtEtaPhiM(pLV.Pt() / 1000., pLV.Eta(), pLV.Phi(), 0.);
				smeared->addPhoton(pLV, photon.id(), photon.index());
			}
		}
		else {
			smeared->addPhoton(photon, photon.id(), photon.index());
		}
	}
	for (const auto& jet : jets) {
		if (smearJets) {
			TLorentzVector jet4V;
			float jetpt = jet.Pt();
			float jetptMeV = jetpt*1000.;
			if (smearJetPt){
							//std::cout << " Point 1 jetptMeV/ Eta /Phi /Mass= " <<
							//jetptMeV << " / " << jet.Eta() << " / " << jet.Phi() << " / " << jet.M() << std::endl;
				//if (jetpt < 1500) jetpt = m_upgrade->getJetSmearedEnergy(jetptMeV, jet.Eta(), true) / 1000.; // FIXME: can only smear jets below 1500 GeV
				if (jetpt < 1500) jet4V = m_upgrade->getSmearedJet(jetptMeV, jet.Eta(), jet.Phi(), jet.M()); // FIXME: can only smear jets below 1500 GeV
							//std::cout << " Point 2 After smearing Pt / Eta / Phi /Mass = " <<
							//jet4V.Pt() << " / " << jet4V.Eta() << " / " << jet4V.Phi() << " / " << jet4V.M() << std::endl;
							jetpt = jet4V.Pt()/1000.;
			}
			jetptMeV = jetpt*1000.;
			float jeteta = jet.Eta();
			float jetphi = jet.Phi();
			float jetE = jet.E()*jetpt / jet.Pt();

			char jetType = 'L';
			if (jet.pass(TrueBJet)) jetType = 'B';
			if (jet.pass(TrueCJet)) jetType = 'C';

			float tag = m_random.Uniform(1.0);
			int jetid = GoodJet;

			float tagEff60, tagEff70, tagEff77, tagEff85;
			if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) {
				tagEff70 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "mv1", 70, m_upgrade->getPileupTrackConfSetting());
				tagEff85 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "mv1", 85, m_upgrade->getPileupTrackConfSetting());

				if (tag < tagEff70) jetid |= (BTag70MV2c10 | BTag70MV2c20);
				if (tag < tagEff85) jetid |= (BTag85MV2c10 | BTag85MV2c20); //MT : extend to other WPs?

				if (tag < tagEff70) jetid = GoodBJet;

				if (jet.pass(TrueLightJet)) jetid |= TrueLightJet;
				if (jet.pass(TrueCJet))     jetid |= TrueCJet;
				if (jet.pass(TrueBJet))     jetid |= TrueBJet;
				if (jet.pass(TrueTau))      jetid |= TrueTau;

			}
			else { //Run2 settings
				tagEff60 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 60, false);
				tagEff70 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 70, false);
				tagEff77 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 77, false);
				tagEff85 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 85, false);

				if (tag < tagEff60) jetid |= BTag60MV2c10;
				if (tag < tagEff70) jetid |= BTag70MV2c10;
				if (tag < tagEff77) jetid |= BTag77MV2c10;
				if (tag < tagEff85) jetid |= BTag85MV2c10;

				//	if (tag < tagEff77) jetid=GoodBJet;

				if (jet.pass(TrueLightJet)) jetid |= TrueLightJet;
				if (jet.pass(TrueCJet))     jetid |= TrueCJet;
				if (jet.pass(TrueBJet))     jetid |= TrueBJet;
				if (jet.pass(TrueTau))      jetid |= TrueTau;

			}

			//if (addPileupJets) {
				if ((jetptMeV) < m_upgrade->getPileupJetPtThresholdMeV()) jetpt = 0;
				else {
					float trackEff = m_upgrade->getTrackJetConfirmEff(jetptMeV, jet.Eta(), "HS");
					float hsProb = m_random.Uniform(1.0);
					if (hsProb > trackEff) jetpt = 0; // FIXME: should couple this to JVT flag
				}
			//}
			if (jetpt) {
				TLorentzVector j;
				j.SetPtEtaPhiE(jetpt, jeteta, jetphi, jetE);
				smeared->addJet(j, jetid, jet.index());

				// Add jets faking electrons    //MT : need to implement run2 version yet
				if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) {

					if (m_random.Uniform(1.0) < m_upgrade->getElectronFakeRate(jet.Pt() * 1000, jet.Eta())) {
						float electron_e = m_upgrade->getElectronFakeRescaledEnergy(jet.E()*1000., jet.Eta()) / 1000.;
						TLorentzVector eLV;
						eLV.SetPtEtaPhiM(jet.Pt()*electron_e / jet.E(), jet.Eta(), jet.Phi(), 0.000510998910);
						int echarge = 1;
						if (m_random.Uniform(1.0) < 0.5) echarge = -1;
						smeared->addElectron(eLV, echarge, EIsoGood, -1);
					}
					// Add jets faking tau
					short tau_prong = 1; // FIXME: how to get prong distribution? Assuming 1-prong is likely overestimate
					short wp = 2; //tight tau
                                        if (jetpt * 1000 < 20000 || fabs(jet.Eta()) > 2.5) continue;
					if (m_random.Uniform(1.0) < m_upgrade->getTauFakeRate(jetpt * 1000, jet.Eta(), tau_prong, wp)) {
						float tau_et = jetpt; // FIXME: no tau smearing exists yet
						TLorentzVector tauLV;
						tauLV.SetPtEtaPhiM(tau_et, jet.Eta(), jet.Phi(), 1.777682);
						int taucharge = 1;
						if (m_random.Uniform(1.0) < 0.5) taucharge = -1;
						smeared->addTau(tauLV, taucharge, TauIsoGood, -1);
					}
					// Add jets faking photon
					if (m_random.Uniform(1.0) < m_upgrade->getPhotonFakeRate(jet.Pt() * 1000/*, jet.Eta()*/)) {
						float photon_et = m_upgrade->getPhotonFakeRescaledET(jet.Pt() * 1000/*., jet.Eta()*/) / 1000.;
						TLorentzVector pLV;
						pLV.SetPtEtaPhiM(photon_et, jet.Eta(), jet.Phi(), 0.0);
						smeared->addPhoton(pLV, PhotonIsoGood, -1);
					}
				}
			}

		}
		else {
			smeared->addJet(jet, jet.id(), jet.index());
		}
	}
	if (addPileupJets) {
		for (const auto& pujet : m_upgrade->getPileupJets()) {
			float trackEff = m_upgrade->getTrackJetConfirmEff(pujet.Pt(), pujet.Eta(), "PU");
			float puProb = m_random.Uniform(1.0);

			if (puProb > trackEff) continue; // FIXME: should couple this to JVT flag
			float tagEff70 = m_upgrade->getFlavourTagEfficiency(pujet.Pt(), pujet.Eta(), 'P', "mv1", 70, m_upgrade->getPileupTrackConfSetting());
			float tagEff85 = m_upgrade->getFlavourTagEfficiency(pujet.Pt(), pujet.Eta(), 'P', "mv1", 85, m_upgrade->getPileupTrackConfSetting());
			float tag = m_random.Uniform(1.0);
			int jetid = GoodJet;
			if (tag < tagEff85) jetid |= BTag85MV2c20; //FIXME: check if this should set other working points too
			if (tag < tagEff70) jetid = GoodBJet;
			smeared->addJet(pujet.Px() / 1000., pujet.Py() / 1000., pujet.Pz() / 1000., pujet.E() / 1000., jetid, -1);
			// Add jets faking photon
			if (m_random.Uniform(1.0) < m_upgrade->getPhotonPileupFakeRate(pujet.Pt()/*, pujet.Eta()*/)) {
				float photon_et = m_upgrade->getPhotonPileupFakeRescaledET(pujet.Pt()/*., pujet.Eta()*/) / 1000.;
				TLorentzVector pLV;
				pLV.SetPtEtaPhiM(photon_et, pujet.Eta(), pujet.Phi(), 0.0);
				smeared->addPhoton(pLV, PhotonIsoGood, -3);
			}
		}
	}
	for (const auto& jet : fatjets) {
		smeared->addFatJet(jet, jet.id(), jet.index()); //FIXME: for now there is no smearing for fat jets
	}

	if (smearMET) {
		if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) { //upgrade settings

			auto smearedMET = m_upgrade->getMETSmeared(sumet*1000., met_x*1000., met_y*1000.);
			met_x = smearedMET.first / 1000.;
			met_y = smearedMET.second / 1000.;

		}
		else { //Run2 settings

		  //truth level MET
		  //do some OR first  (simplified, no taus, no photons)
			auto METjets = AnalysisClass::overlapRemoval(jets, electrons, 0.2, NOT(BTag85MV2c10));
			auto METelectrons = AnalysisClass::overlapRemoval(electrons, METjets, 0.4);
			METjets = AnalysisClass::overlapRemoval(METjets, muons, 0.4, LessThan3Tracks);
			auto METmuons = AnalysisClass::overlapRemoval(muons, METjets, 0.4);

			//get vectorial sum of all objects
			AnalysisObject tMET(0, 0, 0, 0, 0, 0, MET, 0);
			for (int ii = 0; ii < (int)METjets.size(); ii++)      tMET += METjets[ii];
			for (int ii = 0; ii < (int)METelectrons.size(); ii++) tMET += METelectrons[ii];
			for (int ii = 0; ii < (int)METmuons.size(); ii++)     tMET += METmuons[ii];

			//auto sumet = event->getSumET(); // ??

			//smear TST 
			AnalysisObject ptHard = tMET + met;   // adding TruthNonInt (neutrinos, neutralinos, etc...)

			TVector3 TST = m_upgrade->getTSTsmearing(ptHard.Vect()*1000.) * 0.001; //back in GeV

			//compute smeared MET now
			auto SDelectrons = smeared->getElectrons(1., 4.2, EGood); // Filter on pT, eta and "ID"
			auto SDmuons = smeared->getMuons(1., 4.2, MuGood);
			auto SDjets = smeared->getJets(10., 5.2, GoodJet);

			auto SMETjets = AnalysisClass::overlapRemoval(SDjets, SDelectrons, 0.2, NOT(BTag85MV2c10));
			auto SMETelectrons = AnalysisClass::overlapRemoval(SDelectrons, SMETjets, 0.4);
			SMETjets = AnalysisClass::overlapRemoval(SMETjets, SDmuons, 0.4, LessThan3Tracks);
			auto SMETmuons = AnalysisClass::overlapRemoval(SDmuons, SMETjets, 0.4);

			//get vectorial sum of all objects
			AnalysisObject sMET(0, 0, 0, 0, 0, 0, MET, 0);
			for (int ii = 0; ii < (int)SMETjets.size(); ii++)      sMET += SMETjets[ii];
			for (int ii = 0; ii < (int)SMETelectrons.size(); ii++) sMET += SMETelectrons[ii];
			for (int ii = 0; ii < (int)SMETmuons.size(); ii++)     sMET += SMETmuons[ii];

			met_x = -sMET.Px() - TST.X();
			met_y = -sMET.Py() - TST.Y();

		}
		smeared->setMET(met_x, met_y);
	}

	return smeared;
#else
	return 0;
#endif

}

TruthEvent *TruthSmear::weighedSmearing(AnalysisEvent *
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
	event
#endif
) {
#ifdef ROOTCORE_PACKAGE_UpgradePerformanceFunctions
	auto electrons = event->getElectrons(1., 4.2); // Filter on pT, eta and "ID"
	auto muons = event->getMuons(1., 4.2);
	auto taus = event->getTaus(20., 2.5);
	auto photons = event->getPhotons(5., 4.2);
	auto jets = event->getJets(10., 5.2);
	auto fatjets = event->getFatJets(10., 5.2);
	auto puJets = m_upgrade->getPileupJets();
	auto met = event->getMET();
	auto sumet = event->getSumET();

	double met_x = met.Px();
	double met_y = met.Py();

	TruthEvent* smeared = new TruthEvent(sumet, met_x, met_y);
	smeared->setChannelInfo(event->getMCNumber(), event->getSUSYChannel());
	smeared->setGenMET(event->getGenMET());
	smeared->setGenHT(event->getGenHT());
	smeared->setMCWeights(event->getMCWeights());
	smeared->setTruth(event);

	for (const auto& electron : electrons) {
		if (smearElectrons) {
			float electron_e = m_upgrade->getElectronSmearedEnergy(electron.E()*1000., electron.Eta()) / 1000.;
			TLorentzVector eLV;
			eLV.SetPtEtaPhiM(electron.Pt()*electron_e / electron.E(), electron.Eta(), electron.Phi(), 0.000510998910);
			int echarge = electron.charge();
			if (fabs(electron.Eta()) < 2.47) //FIXME: should have for full eta range
				if (m_random.Uniform(1.0) < m_upgrade->getElectronChargeFlipProb(electron.Pt()*1000., electron.Eta())) echarge *= -1;
			float recoProb_e = getElectronWeight(electron.Pt()*1000., electron.Eta(), m_upgrade);
			smeared->addElectron(eLV, echarge, electron.id(), electron.index(), recoProb_e);
			float recoProb_y = getPhotonFakeFromElectronWeight(electron.Pt()*1000., electron.Eta(), m_upgrade);
			smeared->addPhoton(eLV, PhotonIsoGood, -2, recoProb_e*recoProb_y);
		}
		else {
			smeared->addElectron(electron, electron.charge(), electron.id(), electron.index());
		}
	}
	for (const auto& muon : muons) {
		if (smearMuons) {
			float muonUnsmearedPt = muon.Pt()*1000.;
			float qoverpt = muon.charge() / muonUnsmearedPt;
			float muonQOverPtResolution = m_upgrade->getMuonQOverPtResolution(muonUnsmearedPt, muon.Eta());
			qoverpt += m_random.Gaus(0., muonQOverPtResolution);
			float muonSmearedPt = fabs(1. / qoverpt) / 1000.;
			int muonCharge = 1;
			if (qoverpt < 0) muonCharge = -1;
			TLorentzVector mLV;
			mLV.SetPtEtaPhiM(muonSmearedPt, muon.Eta(), muon.Phi(), 0.1056583715);
			float recoProb = getMuonWeight(muon.Pt()*1000., muon.Eta(),  m_upgrade);
			smeared->addMuon(mLV, muonCharge, muon.id(), muon.index(), recoProb);
		}
		else {
			smeared->addMuon(muon, muon.charge(), muon.id(), muon.index());
		}
	}
	// std::cout << std::endl;
	// std::cout << "-------------------------- SMEARING --------------------------------------" << std::endl;
	for (const auto& tau : taus) {
		if (smearTaus) {
			short prong = 1;
                        short wp = 2; //tight tau
			if (tau.pass(TauThreeProng)) prong = 3;
			float tau_E = m_upgrade->getTauSmearedEnergy(tau.E()*1000., tau.Eta(), prong) / 1000.;
			TLorentzVector tauLV;
			tauLV.SetPtEtaPhiM(tau.Pt()*tau_E / tau.E(), tau.Eta(), tau.Phi(), 1.777682);
			float recoProb = getTauWeight(tau.Pt()*1000., tau.Eta(), prong, wp, m_upgrade);
			smeared->addTau(tauLV, tau.charge(), tau.id(), tau.index(), recoProb);
			// std::cout << "Added tau with index " << tau.index() << " and Pt " << tau.Pt() << std::endl;
		}
		else {
			smeared->addTau(tau, tau.charge(), tau.id(), tau.index());
		}
	}
	for (const auto& photon : photons) {
		if (smearPhotons) {
			TLorentzVector pLV;
			pLV.SetPtEtaPhiM(photon.Pt()*1000., photon.Eta(), photon.Phi(), 0.);
			pLV = m_upgrade->getPhotonSmearedVector(&pLV);
			pLV.SetPtEtaPhiM(pLV.Pt() / 1000., pLV.Eta(), pLV.Phi(), 0.);
			float recoProb = getPhotonWeight(photon.Pt()*1000./*, photon.Eta()*/, m_upgrade);
			smeared->addPhoton(pLV, photon.id(), photon.index(), recoProb);
		}
		else {
			smeared->addPhoton(photon, photon.id(), photon.index());
		}
	}
	for (const auto& jet : jets) {
		if (smearJets) {
                        TLorentzVector jet4V;
			float jetpt = jet.Pt();
			float jetptMeV = jetpt*1000.;
			if (smearJetPt){
							//std::cout << " Point 3 jetptMeV/ Eta /Phi /Mass= " <<
							//jetptMeV << " / " << jet.Eta() << " / " << jet.Phi() << " / " << jet.M() << std::endl;
				//if (jetpt < 1500) jetpt = m_upgrade->getJetSmearedEnergy(jetptMeV, jet.Eta(), true) / 1000.; // FIXME: can only smear jets below 1500 GeV
				if (jetpt < 1500) jet4V = m_upgrade->getSmearedJet(jetptMeV, jet.Eta(), jet.Phi(), jet.M()); // FIXME: can only smear jets below 1500 GeV
							//std::cout << " Point 4 After smearing Pt / Eta / Phi /Mass = " <<
							//jet4V.Pt() << " / " << jet4V.Eta() << " / " << jet4V.Phi() << " / " << jet4V.M() << std::endl;
	
							jetpt = jet4V.Pt()/1000.;
			}
			jetptMeV = jetpt*1000.;
			float jeteta = jet.Eta();
			float jetphi = jet.Phi();
			float jetE = jet.E()*jetpt / jet.Pt();

			char jetType = 'L';
			if (jet.pass(TrueBJet)) jetType = 'B';
			if (jet.pass(TrueCJet)) jetType = 'C';

			float tag = m_random.Uniform(1.0);
			int jetid = GoodJet;

			float tagEff60, tagEff70, tagEff77, tagEff85;
			if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) {
				tagEff70 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "mv1", 70, m_upgrade->getPileupTrackConfSetting());
				tagEff85 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "mv1", 85, m_upgrade->getPileupTrackConfSetting());

				if (tag < tagEff70) jetid |= (BTag70MV2c10 | BTag70MV2c20);
				if (tag < tagEff85) jetid |= (BTag85MV2c10 | BTag85MV2c20); //MT : extend to other WPs?

				if (tag < tagEff70) jetid = GoodBJet;

				if (jet.pass(TrueLightJet)) jetid |= TrueLightJet;
				if (jet.pass(TrueCJet))     jetid |= TrueCJet;
				if (jet.pass(TrueBJet))     jetid |= TrueBJet;
				if (jet.pass(TrueTau))      jetid |= TrueTau;

			}
			else { //Run2 settings
				tagEff60 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 60, false);
				tagEff70 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 70, false);
				tagEff77 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 77, false);
				tagEff85 = m_upgrade->getFlavourTagEfficiency(jetpt*1000., jeteta, jetType, "MV2c10", 85, false);

				if (tag < tagEff60) jetid |= BTag60MV2c10;
				if (tag < tagEff70) jetid |= BTag70MV2c10;
				if (tag < tagEff77) jetid |= BTag77MV2c10;
				if (tag < tagEff85) jetid |= BTag85MV2c10;

				//	if (tag < tagEff77) jetid=GoodBJet;

				if (jet.pass(TrueLightJet)) jetid |= TrueLightJet;
				if (jet.pass(TrueCJet))     jetid |= TrueCJet;
				if (jet.pass(TrueBJet))     jetid |= TrueBJet;
				if (jet.pass(TrueTau))      jetid |= TrueTau;

			}

			float jetConfirmEff = 1.;

			//if (addPileupJets) {
				if ((jetptMeV) < m_upgrade->getPileupJetPtThresholdMeV()) jetpt = 0;
				else {
					jetConfirmEff = getJetWeight(jetptMeV, jet.Eta(), "HS", m_upgrade);
					// float hsProb = m_random.Uniform(1.0);
					// if (hsProb > jetConfirmEff) jetpt = 0; // FIXME: should couple this to JVT flag
				}
			//}

			if (jetpt) {
				TLorentzVector j;
				j.SetPtEtaPhiE(jetpt, jeteta, jetphi, jetE);
				int ID = getID();
				smeared->addJet(j, jetid, jet.index(), jetConfirmEff, ID);
				// std::cout << "Added jet with index " << jet.index() << " and Pt " << j.Pt() << " and recoIndex " << ID << std::endl;

				//MT : need to implement run2 version yet
				if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) {
					// Add jets faking electrons
					float recoProb_e_fake = getElectronFakeWeight(jet.Pt() * 1000, jet.Eta(), m_upgrade);
					float electron_e = m_upgrade->getElectronFakeRescaledEnergy(jet.E()*1000., jet.Eta()) / 1000.;
					TLorentzVector eLV;
					eLV.SetPtEtaPhiM(jet.Pt()*electron_e / jet.E(), jet.Eta(), jet.Phi(), 0.000510998910);
					int echarge = 1;
					if (m_random.Uniform(1.0) < 0.5) echarge = -1;
					smeared->addElectron(eLV, echarge, EIsoGood, -1, recoProb_e_fake, ID);

					// Add jets faking tau
					short tau_prong = 1; // FIXME: how to get prong distribution? Assuming 1-prong is likely overestimate
					short wp = 2;
					if (jetpt*1000.<20000 || fabs(jet.Eta())>2.5) continue;

					float recoProb_tau_fake = getTauFakeWeight(jetpt * 1000, jet.Eta(), tau_prong, wp, m_upgrade);
					float tau_et = jetpt; // FIXME: no tau smearing exists yet
					TLorentzVector tauLV;
					tauLV.SetPtEtaPhiM(tau_et, jet.Eta(), jet.Phi(), 1.777682);
					int taucharge = 1;
					if (m_random.Uniform(1.0) < 0.5) taucharge = -1;
					smeared->addTau(tauLV, taucharge, TauIsoGood, -1, recoProb_tau_fake, ID);
					// std::cout << "Added tau with index -1 and Pt " << tauLV.Pt() << " and recoIndex " << ID << std::endl;
					

					// Add jets faking photon
					float recoProb_y_fake = getPhotonFakeWeight(jet.Pt() * 1000/*, jet.Eta()*/, m_upgrade);
					float photon_et = m_upgrade->getPhotonFakeRescaledET(jet.Pt() * 1000/*., jet.Eta()*/) / 1000.;
					TLorentzVector pLV;
					pLV.SetPtEtaPhiM(photon_et, jet.Eta(), jet.Phi(), 0.0);
					smeared->addPhoton(pLV, PhotonIsoGood, -1,  recoProb_y_fake, ID);
				}
			}
		}
		else {
			smeared->addJet(jet, jet.id(), jet.index());
		}
	}
	if (addPileupJets) {
		for (const auto& pujet : puJets) {
			float trackEff = getJetWeight(pujet.Pt(), pujet.Eta(), "PU", m_upgrade);
			float tagEff70 = m_upgrade->getFlavourTagEfficiency(pujet.Pt(), pujet.Eta(), 'P', "mv1", 70, m_upgrade->getPileupTrackConfSetting());
			float tagEff85 = m_upgrade->getFlavourTagEfficiency(pujet.Pt(), pujet.Eta(), 'P', "mv1", 85, m_upgrade->getPileupTrackConfSetting());
			float tag = m_random.Uniform(1.0);
			int jetid = GoodJet;
			if (tag < tagEff85) jetid |= BTag85MV2c20; //FIXME: check if this should set other working points too
			if (tag < tagEff70) jetid = GoodBJet;
			smeared->addJet(pujet.Px() / 1000., pujet.Py() / 1000., pujet.Pz() / 1000., pujet.E() / 1000., jetid, -1, trackEff);
			// Add jets faking photon
			float recoProb_y_fake = getPileupFakePhotonWeight(pujet.Pt()/*, pujet.Eta()*/, m_upgrade);
			float photon_et = m_upgrade->getPhotonPileupFakeRescaledET(pujet.Pt()/*., pujet.Eta()*/) / 1000.;
			TLorentzVector pLV;
			pLV.SetPtEtaPhiM(photon_et, pujet.Eta(), pujet.Phi(), 0.0);
			smeared->addPhoton(pLV, PhotonIsoGood, -3, recoProb_y_fake*trackEff);
		}
	}
	for (const auto& jet : fatjets) {
		smeared->addFatJet(jet, jet.id(), jet.index()); //FIXME: for now there is no smearing for fat jets
	}

	if (smearMET) {
		if (m_upgrade->getLayout() != UpgradePerformanceFunctions::UpgradeLayout::run2) { //upgrade settings

			auto smearedMET = m_upgrade->getMETSmeared(sumet*1000., met_x*1000., met_y*1000.);
			met_x = smearedMET.first / 1000.;
			met_y = smearedMET.second / 1000.;

		}
		else { //Run2 settings

		  //truth level MET
		  //do some OR first  (simplified, no taus, no photons)
			auto METjets = AnalysisClass::overlapRemoval(jets, electrons, 0.2, NOT(BTag85MV2c10));
			auto METelectrons = AnalysisClass::overlapRemoval(electrons, METjets, 0.4);
			METjets = AnalysisClass::overlapRemoval(METjets, muons, 0.4, LessThan3Tracks);
			auto METmuons = AnalysisClass::overlapRemoval(muons, METjets, 0.4);

			//get vectorial sum of all objects
			AnalysisObject tMET(0, 0, 0, 0, 0, 0, MET, 0);
			for (int ii = 0; ii < (int)METjets.size(); ii++)      tMET += METjets[ii];
			for (int ii = 0; ii < (int)METelectrons.size(); ii++) tMET += METelectrons[ii];
			for (int ii = 0; ii < (int)METmuons.size(); ii++)     tMET += METmuons[ii];

			//auto sumet = event->getSumET(); // ??

			//smear TST 
			AnalysisObject ptHard = tMET + met;   // adding TruthNonInt (neutrinos, neutralinos, etc...)

			TVector3 TST = m_upgrade->getTSTsmearing(ptHard.Vect()*1000.) * 0.001; //back in GeV

			//compute smeared MET now
			auto SDelectrons = smeared->getElectrons(1., 4.2, EGood); // Filter on pT, eta and "ID"
			auto SDmuons = smeared->getMuons(1., 4.2, MuGood);
			auto SDjets = smeared->getJets(10., 5.2, GoodJet);

			auto SMETjets = AnalysisClass::overlapRemoval(SDjets, SDelectrons, 0.2, NOT(BTag85MV2c10));
			auto SMETelectrons = AnalysisClass::overlapRemoval(SDelectrons, SMETjets, 0.4);
			SMETjets = AnalysisClass::overlapRemoval(SMETjets, SDmuons, 0.4, LessThan3Tracks);
			auto SMETmuons = AnalysisClass::overlapRemoval(SDmuons, SMETjets, 0.4);

			//get vectorial sum of all objects
			AnalysisObject sMET(0, 0, 0, 0, 0, 0, MET, 0);
			for (int ii = 0; ii < (int)SMETjets.size(); ii++)      sMET += SMETjets[ii];
			for (int ii = 0; ii < (int)SMETelectrons.size(); ii++) sMET += SMETelectrons[ii];
			for (int ii = 0; ii < (int)SMETmuons.size(); ii++)     sMET += SMETmuons[ii];

			met_x = -sMET.Px() - TST.X();
			met_y = -sMET.Py() - TST.Y();

		}
		smeared->setMET(met_x, met_y);
	}
	
	return smeared;

#else
	return 0;
#endif
}

